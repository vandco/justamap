//
//  Message.swift
//  
//
//  Created by Artur on 29/08/16.
//
//

import Foundation
import CoreData

class Message: NSManagedObject {
    
    @NSManaged var sentAt: Date?
    @NSManaged var text: String
    @NSManaged var isShort: NSNumber
    @NSManaged var photo: Data
    @NSManaged var direction_: NSNumber
    @NSManaged var correspondentUDID: String
    @NSManaged var isUnread: NSNumber

}
