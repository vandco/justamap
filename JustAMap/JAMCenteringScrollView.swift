//
//  JAMCenteringScrollView.swift
//  JustAMap
//
//  Created by Artur on 26/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//
//  http://stackoverflow.com/a/3479076/2175025
//

import UIKit

class JAMCenteringScrollView: UIScrollView {
    override func layoutSubviews() {
        super.layoutSubviews()
    
        // center the image as it becomes smaller than the size of the screen
        let boundsSize = self.bounds.size
        let subview = self.subviews[0] 
        var frameToCenter = subview.frame
    
        // center horizontally
        if (frameToCenter.size.width < boundsSize.width) {
            frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2
        } else {
            frameToCenter.origin.x = 0
        }
    
        // center vertically
        if (frameToCenter.size.height < boundsSize.height) {
            frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2
        } else {
            frameToCenter.origin.y = 0
        }
    
        subview.frame = frameToCenter
    }
}
