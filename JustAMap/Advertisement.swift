//
//  Advertisement.swift
//  
//
//  Created by Artur on 29/07/16.
//
//

import Foundation
import CoreData

@objc(Advertisement)
class Advertisement: NSManagedObject {

    @NSManaged var udid: String
    @NSManaged var desc: String
    @NSManaged var photo: NSData
    @NSManaged var submittedAt: NSDate
    @NSManaged var isDraft: NSNumber

}
