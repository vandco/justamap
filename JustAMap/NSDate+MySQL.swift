//
//  NSDate+MySQL.swift
//  JustAMap
//
//  Created by Artur on 30/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import Foundation

extension Date {
    init?(mySqlTimestamp: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let timestamp = dateFormatter.date(from: mySqlTimestamp) {
            self.init(timeInterval: 0, since: timestamp)
        } else {
            self.init()
            return nil
        }
    }
}
