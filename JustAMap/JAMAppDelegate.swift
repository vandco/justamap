//
//  JAMAppDelegate.swift
//  JustAMap
//
//  Created by Artur on 25/07/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import Alamofire

enum NotificationKeys: String {
    case LAUNCH_STATUS_UPDATED_NOTIFICATION_KEY = ".launchStatusUpdate"
    case LAUNCH_COMPLETED_NOTIFICATION_KEY = ".launchComplete"
    case LOCATION_UPDATED_NOTIFICATION_KEY = ".locationUpdate"
    case MESSAGES_UPDATED_NOTIFICATION_KEY = ".newMessage"
    case MESSAGES_SENT_NOTIFICATION_KEY = ".messageSent"
    case ADVERTISEMENTS_REFRESHED_NOTIFICATION_KEY = ".advertisementsRefreshed"
    
    var notification: Notification.Name {
        return Notification.Name(rawValue: Bundle.main.bundleIdentifier! + self.rawValue)
    }
}

let AVATAR_SIZE: CGFloat = 64
let MAX_PHOTO_FULL_SIZE = CGSize(width: 1920, height: 1080)
let FOREGROUND_BUTTON_COLOR: UIColor = UIColor(red: 0, green: 122/255.0, blue: 1, alpha: 1)
let FOREGROUND_DISABLED_BUTTON_COLOR: UIColor = UIColor(red: 187/255.0, green: 217/255.0, blue: 251/255.0, alpha: 1)

let EARTH_EQUATORIAL_CIRCUMFERENCE_IN_METERS = 40075696.0

let MAX_CHARACTERS_IN_MESSAGE = 128

@UIApplicationMain
class JAMAppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    var locationManager: CLLocationManager?
    fileprivate var _location: CLLocationCoordinate2D?
    var location: CLLocationCoordinate2D? {
        get {
            if ((self.locationManager === nil) || (!CLLocationManager.locationServicesEnabled())) {
                _location = nil
            }
            return _location
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.locationManager = CLLocationManager()
        self.locationManager!.delegate = self
        self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest
        
        self.locationManager!.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager!.startUpdatingLocation()
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    lazy var defaults: UserDefaults = {
        let defaults = UserDefaults.standard
        defaults.register(defaults: [
            "MGLMapboxMetricsEnabled": false
        ])
        print("Mapbox Metrics Is Enabled: \(defaults.isMapboxMetricsEnabled)")
        return defaults
    }()
    
    lazy var scale: String = {
        return String(format: "%d", Int(UIScreen.main.scale))
    }()
    
    lazy var coreDataDispatcher: CoreDataDispatcher = {
        let dispatcher = CoreDataDispatcher.sharedDispatcher
        dispatcher.managedObjectContext = self.managedObjectContext
        return dispatcher
    }()

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "ru.vandco.JustAMap" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] 
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "JustAMap", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("JustAMap.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        if ((try? coordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)) == nil) {
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if let moc = self.managedObjectContext {
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch let error as NSError {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog("Unresolved error \(error), \(error.userInfo)")
                    abort()
                }
            }
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager!.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        _location = (locations.last!).coordinate
        NotificationCenter.default.post(name: NotificationKeys.LOCATION_UPDATED_NOTIFICATION_KEY.notification, object: self)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let clError = error as? CLError {
            switch (clError) {
            case CLError.locationUnknown:
                //println("GPS Unknown")
                _location = nil
            case CLError.denied:
                //println("GPS Denied")
                _location = nil
                self.locationManager?.stopUpdatingLocation()
            default:
                print("GPS ERROR")
                break
            }

            if CLLocationManager.locationServicesEnabled() {
                self.locationManager!.stopUpdatingLocation()
                self.locationManager!.startUpdatingLocation()
            }
        }
    }
    
    // MARK: - Alamofire SessionManager
    
    static public var sharedSessionManager: Alamofire.SessionManager = Alamofire.SessionManager()

}

