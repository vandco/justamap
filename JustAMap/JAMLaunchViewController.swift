//
//  JAMLaunchViewController.swift
//  JustAMap
//
//  Created by Artur on 24/09/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

class JAMLaunchViewController: UIViewController {
    
    fileprivate var _storyboard: UIStoryboard!
    fileprivate var _navVC: UINavigationController!
    fileprivate var _dummyNavView: UIView!
    fileprivate var _mapVC: JAMMapViewController!
    fileprivate var _dummyMapView: UIView!

    @IBOutlet weak var lblStatus: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NotificationCenter.default.addObserver(self, selector: #selector(updateLaunchStatus), name: NotificationKeys.LAUNCH_STATUS_UPDATED_NOTIFICATION_KEY.notification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(completeLaunching), name: NotificationKeys.LAUNCH_COMPLETED_NOTIFICATION_KEY.notification, object: nil)

        self._storyboard = UIStoryboard(name: "Main", bundle: nil)
        self._navVC = self._storyboard.instantiateViewController(withIdentifier: "navVC") as! UINavigationController
        self._dummyNavView = self._navVC.view // to ensure the VC is actually loaded and viewDidLoad method is called
        self._mapVC = self._navVC.viewControllers[0] as! JAMMapViewController
        self._mapVC.launchVC = self
        self._dummyMapView = self._mapVC.view // to ensure the VC is actually loaded and viewDidLoad method is called
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateLaunchStatus(_ notification: Notification) {
        let userInfo = notification.userInfo! as! [String: String]
        self.lblStatus.text = userInfo["status"]
    }
    
    @objc func completeLaunching() {
        self.present(self._navVC, animated: true, completion: nil)
    }
    
}
