//
//  JAMProcessingViewController.swift
//  JustAMap
//
//  Created by Artur on 12/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

class JAMProcessingViewController: UIViewController {
    
    var status: String {
        get { return (self.view as! JAMProcessingView).status }
        set { (self.view as! JAMProcessingView).status = newValue }
    }

    init(status: String) {
        super.init(nibName: nil, bundle: nil)
        self.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        
        self.view = JAMProcessingView.instanceFromNib(status: status)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
