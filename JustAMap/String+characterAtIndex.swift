//
//  String+characterAtIndex.swift
//  JustAMap
//
//  Created by Artur on 26/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//
//  http://stackoverflow.com/a/31265316/2175025
//

import Foundation

extension String {
    subscript(integerIndex: Int) -> Character {
        let index = self.characters.index(self.startIndex, offsetBy: integerIndex)
        return self[index]
    }
    
    subscript(integerRange: Range<Int>) -> String {
        let start = index(self.startIndex, offsetBy: integerRange.lowerBound)
        let end   = index(self.startIndex, offsetBy: integerRange.upperBound)
        let range = start..<end
        return self[range]
    }
    
    subscript(integerRange: ClosedRange<Int>) -> String {
        let start = index(self.startIndex, offsetBy: integerRange.lowerBound)
        let end   = index(self.startIndex, offsetBy: integerRange.upperBound)
        let range = start...end
        return self[range]
    }
}
