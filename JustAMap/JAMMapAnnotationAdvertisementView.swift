//
//  JAMMapAnnotationAdvertisementView.swift
//  JustAMap
//
//  Created by Artur on 26/07/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit
import Mapbox

class JAMMapAnnotationAdvertisementView: UIView, MGLCalloutView {
    
    @IBOutlet weak var imgAvatar: UIImageView?
    @IBOutlet weak var lblShortDescription: UILabel?
    
    class func instanceFromNib() -> JAMMapAnnotationAdvertisementView {
        let view = UINib(nibName: "JAMMapAnnotationAdvertisement", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! JAMMapAnnotationAdvertisementView
        
        //view.layer.cornerRadius = 7
        view.layer.borderWidth = 1
        view.layer.borderColor = FOREGROUND_BUTTON_COLOR.cgColor
        
        let tapRecognizer = UITapGestureRecognizer(target: view, action: #selector(JAMMapAnnotationAdvertisementView.calloutTapped))
        view.addGestureRecognizer(tapRecognizer)
        
        //view.setTranslatesAutoresizingMaskIntoConstraints(false)
        //view.layoutIfNeeded()
        //view.invalidateIntrinsicContentSize()
        
        return view
    }
        
    // MARK: - MGLCalloutView
    
    var representedObject: MGLAnnotation
    public var leftAccessoryView = UIView()/* unused */
    public var rightAccessoryView = UIView()/* unused */
    weak var delegate: MGLCalloutViewDelegate?
    
    var controller: JAMMapViewController!
    
    let tipHeight: CGFloat = 10.0
    let tipWidth: CGFloat = 20.0
    
    required init(representedObject: MGLAnnotation) {
        self.representedObject = representedObject
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        backgroundColor = UIColor.clear
    }

    required init?(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        self.representedObject = aDecoder.decodeObject(forKey: "representedObject") as? JAMPointAnnotation ?? JAMPointAnnotation()
        super.init(coder: aDecoder)
    }
    
    override func encode(with aCoder: NSCoder) {
        aCoder.encode(self.representedObject, forKey: "representedObject")
        super.encode(with: aCoder)
    }
    
    @objc func calloutTapped() {
        self.controller.calloutViewTapped(self)
    }

    // MARK: - MGLCalloutView API
    
    func presentCallout(from rect: CGRect, in view: UIView, constrainedTo constrainedView: UIView, animated: Bool) {
        view.addSubview(self)
        
        // Prepare our frame, adding extra space at the bottom for the tip
        let frameWidth = self.bounds.size.width
        let frameHeight = self.bounds.size.height /*+ tipHeight*/
        let frameOriginX = rect.origin.x + (rect.size.width/2.0) - (frameWidth/2.0)
        let frameOriginY = rect.origin.y - frameHeight
        frame = CGRect(x: frameOriginX, y: frameOriginY, width: frameWidth, height: frameHeight)
        
        if animated {
            alpha = 0
            
            UIView.animate(withDuration: 0.2, animations: { [weak self] in
                self?.alpha = 1
            }) 
        }
    }
    
    func dismissCallout(animated: Bool) {
        if (superview != nil) {
            if animated {
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    self?.alpha = 0
                    }, completion: { [weak self] _ in
                        self?.removeFromSuperview()
                    })
            } else {
                removeFromSuperview()
            }
        }
    }
}
