//
//  async_ui_threads.swift
//  JustAMap
//
//  Created by Artur on 02/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import Foundation

func __ASYNC_BLOCK(_ code: @escaping () -> Void) -> Void {
    DispatchQueue.global(qos: .default).async(execute: code)
    //DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: code)
}

func __UI_THREAD_TASK(_ code: @escaping () -> Void) -> Void {
    DispatchQueue.main.async(execute: code)
}
