//
//  Advertisement+Downloader.swift
//  JustAMap
//
//  Created by Artur on 31/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import CoreData
import CoreLocation
import UIKit
import Alamofire

extension Advertisement {
    
    class func download(
        _ advertisementUDID: String,
        scale: String,
        circleImage: @escaping (UIImage) -> UIImage,
        coreDataDispatcher: CoreDataDispatcher,
        //networker: JAMNetworking,
        baseUri: String,
        selfUDID: String,
        completionHandler: ((_ advertisement: Advertisement) -> Void)?,
        errorHandler: DataCompletionHandler?
    ) -> Void
    {
        var url = "/advertisements/" + advertisementUDID
        url += "?access-token=" + selfUDID
        
        print("URL: \(url)")
        
        JAMAppDelegate.sharedSessionManager.request(baseUri + url).validate().responseJSON { (response: DataResponse<Any>) in
            switch response.result {
            case .success(let advertisement as [String: Any]):
                let udid = advertisement["udid"] as! String
                let coordinate = CLLocationCoordinate2D(
                    latitude: CLLocationDegrees((advertisement["latitude"] as! NSNumber).doubleValue),
                    longitude: CLLocationDegrees((advertisement["longitude"] as! NSNumber).doubleValue)
                )
                let shortDescription = advertisement["shortDescription"] as! String
                //let description = advertisement["description"] as! String
                
                let submittedAt = Date(mySqlTimestamp: advertisement["submitted_at"] as! String)!
                
                let advertisement: Advertisement
                if let storedAdvertisement = (coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [udid])) as [Advertisement]).first
                {
                    advertisement = storedAdvertisement
                } else {
                    advertisement = coreDataDispatcher.newObject() as Advertisement
                    advertisement.udid = udid
                }
                
                advertisement.descShort = shortDescription
                advertisement.submittedAt = submittedAt
                advertisement.latitude = coordinate.latitude
                advertisement.longitude = coordinate.longitude
                
                advertisement.loadAvatar(
                    scale: scale,
                    circleImage: circleImage,
                    coreDataDispatcher: coreDataDispatcher,
                    baseUri: baseUri,
                    selfUDID: selfUDID,
                    completionHandler: completionHandler
                )
            case .success:
                if (errorHandler == nil) {
                    __ASYNC_BLOCK {
                        self.download(
                            advertisementUDID,
                            scale: scale,
                            circleImage: circleImage,
                            coreDataDispatcher: coreDataDispatcher,
                            baseUri: baseUri,
                            selfUDID: selfUDID,
                            completionHandler: completionHandler,
                            errorHandler: errorHandler
                        )
                    }
                } else {
                    __ASYNC_BLOCK {
                        errorHandler!(response.data, response.response, nil)
                    }
                }
            case .failure(let error):
                if (errorHandler == nil) {
                    __ASYNC_BLOCK {
                        self.download(
                            advertisementUDID,
                            scale: scale,
                            circleImage: circleImage,
                            coreDataDispatcher: coreDataDispatcher,
                            baseUri: baseUri,
                            selfUDID: selfUDID,
                            completionHandler: completionHandler,
                            errorHandler: errorHandler
                        )
                    }
                } else {
                    __ASYNC_BLOCK {
                        errorHandler!(response.data, response.response, error as NSError?)
                    }
                }
            }
        }
        
        /*networker.request(
            url,
            httpMethod: "GET",
            data: nil,
            onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                let bodyStr = String(data: body!, encoding: String.Encoding.utf8)
                print(bodyStr!)
                
                if let advertisement = try! JSONSerialization.jsonObject(with: body!, options: []) as? [String: AnyObject]
                {
                    let udid = advertisement["udid"] as! String
                    let coordinate = CLLocationCoordinate2D(
                        latitude: CLLocationDegrees((advertisement["latitude"] as! NSNumber).doubleValue),
                        longitude: CLLocationDegrees((advertisement["longitude"] as! NSNumber).doubleValue)
                    )
                    let shortDescription = advertisement["shortDescription"] as! String
                    //let description = advertisement["description"] as! String
                    
                    let submittedAt = Date(mySqlTimestamp: advertisement["submitted_at"] as! String)!
                    
                    let advertisement: Advertisement
                    if let storedAdvertisement = (coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [udid])) as [Advertisement]).first
                    {
                        advertisement = storedAdvertisement
                    } else {
                        advertisement = coreDataDispatcher.newObject() as Advertisement
                        advertisement.udid = udid
                    }
                    
                    advertisement.descShort = shortDescription
                    advertisement.submittedAt = submittedAt
                    advertisement.latitude = coordinate.latitude
                    advertisement.longitude = coordinate.longitude
                    
                    advertisement.loadAvatar(
                        scale: scale,
                        circleImage: circleImage,
                        coreDataDispatcher: coreDataDispatcher,
                        networker: networker,
                        selfUDID: selfUDID,
                        completionHandler: completionHandler
                    )
                } else {
                    if (errorHandler == nil) {
                        __ASYNC_BLOCK {
                            self.download(
                                advertisementUDID,
                                scale: scale,
                                circleImage: circleImage,
                                coreDataDispatcher: coreDataDispatcher,
                                networker: networker,
                                selfUDID: selfUDID,
                                completionHandler: completionHandler,
                                errorHandler: errorHandler
                            )
                        }
                    } else {
                        __ASYNC_BLOCK {
                            errorHandler!(body, response, error)
                        }
                    }
                }
            },
            onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                if (response != nil) {
                    print(response!)
                }
                
                if (body != nil) {
                    print(String(data: body!, encoding: String.Encoding.utf8)!)
                }
                
                if (errorHandler == nil) {
                    __ASYNC_BLOCK {
                        self.download(
                            advertisementUDID,
                            scale: scale,
                            circleImage: circleImage,
                            coreDataDispatcher: coreDataDispatcher,
                            networker: networker,
                            selfUDID: selfUDID,
                            completionHandler: completionHandler,
                            errorHandler: errorHandler
                        )
                    }
                } else {
                    __ASYNC_BLOCK {
                        errorHandler!(body, response, error)
                    }
                }
            }
        )
        networker.start()*/
    }
    
    func loadAvatar(
        scale: String,
        circleImage: @escaping (UIImage) -> UIImage,
        coreDataDispatcher: CoreDataDispatcher,
        //networker: JAMNetworking,
        baseUri: String,
        selfUDID: String,
        completionHandler: ((_ advertisement: Advertisement) -> Void)?
    ) -> Void
    {
        JAMAppDelegate.sharedSessionManager.request(
            baseUri + "/advertisements/" + self.udid + "/photo/micro/x" + scale + "?access-token=" + selfUDID
        ).validate().responseData { (response: DataResponse<Data>) in
            switch response.result {
            case .success(let data):
                if let downloadedPhoto = UIImage(data: data, scale: UIScreen.main.scale) {
                    let requiredSize = CGSize(width: AVATAR_SIZE, height: AVATAR_SIZE)
                    let photo: UIImage
                    if !downloadedPhoto.size.equalTo(requiredSize) {
                        photo = downloadedPhoto.scaleImage(scaledToSize: requiredSize)
                    } else {
                        photo = downloadedPhoto
                    }
                    
                    let circledPhoto: UIImage = photo.circleImage()
                    
                    self.photoMicro = UIImagePNGRepresentation(circledPhoto)
                }
                
                __ASYNC_BLOCK { () -> Void in
                    completionHandler?(self)
                }
            case .failure(let error):
                if error is Alamofire.AFError {
                    if response.response?.statusCode == 404 {
                        __ASYNC_BLOCK { () -> Void in
                            completionHandler?(self)
                        }
                    }
                }
            }
        }

        /*networker.download(
            "/advertisements/" + self.udid + "/photo/micro/x" + scale + "?access-token=" + selfUDID,
            onProgress: nil,
            onSuccess: { (location: URL?, response: URLResponse?, error: NSError?) -> Void in
                if let body = networker.responseBody {
                    if let downloadedPhoto = UIImage(data: body, scale: UIScreen.main.scale) {
                        let requiredSize = CGSize(width: AVATAR_SIZE, height: AVATAR_SIZE)
                        let photo: UIImage = downloadedPhoto
                        
                        let circledPhoto: UIImage = circleImage(photo)
                        
                        self.photoMicro = UIImagePNGRepresentation(circledPhoto)
                    }
                }
                
                __ASYNC_BLOCK {
                    completionHandler?(self)
                }
            },
            onError: { (_: URL?, response: URLResponse?, error: NSError?) -> Void in
                if let response = response {
                    print(response)
                    if let response = response as? HTTPURLResponse {
                        if response.statusCode == 404 {
                        }
                    }
                }
                
                __ASYNC_BLOCK {
                    completionHandler?(self)
                }
            },
            downloadTo: nil,
            keepInMemoryOnSuccess: true,
            keepInMemoryOnError: false
        )
        networker.start()*/
    }
    
}
