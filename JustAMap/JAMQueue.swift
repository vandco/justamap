//
//  JAMQueue.swift
//  JustAMap
//
//  Created by Artur on 24/01/2017.
//  Copyright © 2017 V&Co Ltd. All rights reserved.
//

import Foundation

typealias TaskHandler = () -> Void

class JAMTask {
    fileprivate var _taskHandler: TaskHandler
    
    init(task: @escaping TaskHandler) {
        self._taskHandler = task
    }
    
    public func run() -> Void {
        self._taskHandler()
    }
}

class JAMAsyncTask: JAMTask {}

class JAMQueue {
    private var _queue: [JAMTask] = []
    public var completionHandler: CompletionHandler
    
    init(onComplete: @escaping CompletionHandler) {
        self.completionHandler = onComplete
    }
    
    public func enqueue(task: JAMTask) -> Void {
        self._queue.append(task)
    }
    
    public func enqueue(task: @escaping TaskHandler) -> Void {
        self._queue.append(JAMTask(task: task))
    }
    
    public func enqueueAsync(task: @escaping TaskHandler) -> Void {
        self._queue.append(JAMAsyncTask(task: task))
    }
    
    @discardableResult public func nextTask() -> Bool {
        objc_sync_enter(self._queue)
        defer { objc_sync_exit(self._queue) }
        
        if (self._queue.count == 0) {
            __ASYNC_BLOCK {
                self.completionHandler()
            }
            return false
        }
        
        let task = self._queue[0]
        self._queue.remove(at: 0)
        
        __ASYNC_BLOCK {
            task.run()
            if !(task is JAMAsyncTask) {
                // the synchronous task has just finished
                self.nextTask()
            }
            // otherwise it's obligation of the async task to call self.nextTask()
        }
        
        // anyway we should return true because we've succeeded with starting a next task
        return true
    }
    
    public func reset() -> Void {
        objc_sync_enter(self._queue)
        defer { objc_sync_exit(self._queue) }

        self._queue.removeAll()
    }
}
