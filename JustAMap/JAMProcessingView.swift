//
//  JAMProcessingView.swift
//  JustAMap
//
//  Created by Artur on 12/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

class JAMProcessingView: UIView {

    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    
    var status: String {
        get { return self.lblStatus.text ?? "" }
        set { self.lblStatus.text = newValue }
    }
    
    class func instanceFromNib(status: String) -> JAMProcessingView {
        let view = UINib(nibName: "JAMProcessing", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! JAMProcessingView
        
        view.boxView.layer.cornerRadius = 7
        view.lblStatus.text = status
        
        view.alpha = 0
        
        return view
    }

}
