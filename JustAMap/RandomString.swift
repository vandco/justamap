//
//  RandomString.swift
//  JustAMap
//
//  Created by Artur on 26/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import Foundation

func randomAlphaNumericString(
    _ length: Int,
    useLowercase: Bool = true,
    useDigits: Bool = true,
    useUppercase: Bool = true
) -> String {
    
    let lowercaseChars = "abcdefghijklmnopqrstuvwxyz"
    let uppercaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    let digitChars     = "0123456789"
    
    var allowedChars = ""
    if useLowercase { allowedChars += lowercaseChars }
    if useUppercase { allowedChars += uppercaseChars }
    if useDigits    { allowedChars += digitChars }

    let allowedCharsCount = UInt32(allowedChars.characters.count)
    var randomString = ""
    
    for _ in (0..<length) {
        let randomNum = Int(arc4random_uniform(allowedCharsCount))
        let newCharacter = allowedChars[randomNum]
        randomString += String(newCharacter)
    }
    
    return randomString
}
