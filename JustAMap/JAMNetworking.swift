//
//  JAMNetworking.swift
//  JustAMap
//
//  Created by Artur on 26/07/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

typealias ProgressHandler = (_ bytesWrittenSinceLastCall: Int64, _ totalBytesWritten: Int64, _ totalBytesExpectedToWrite: Int64) -> Void
typealias DataCompletionHandler = (_ body: Data?, _ response: URLResponse?, _ error: NSError?) -> Void
typealias DownloadCompletionHandler = (_ location: URL?, _ response: URLResponse?, _ error: NSError?) -> Void
typealias CompletionHandler = () -> Void

class JAMNetworkingTask {
    fileprivate var _numberOfAttempts: Int
    fileprivate var _attempts: Int = 0
    
    fileprivate var _networker: JAMNetworking
    fileprivate var _session: URLSession
    fileprivate var _task: URLSessionTask
    fileprivate var _uploadData: Data? = nil
    fileprivate var _responseBody: Data?
    
    fileprivate var _totalDataAmountInBytes: Int64 = 0
    fileprivate var _completedDataAmountInBytes: Int64 = 0
    
    fileprivate var _progressHandler: ProgressHandler?
    fileprivate var _dataSuccessHandler: DataCompletionHandler?
    fileprivate var _dataErrorHandler: DataCompletionHandler?
    fileprivate var _downloadSuccessHandler: DownloadCompletionHandler?
    fileprivate var _downloadErrorHandler: DownloadCompletionHandler?
    
    fileprivate var _keepDownloadedDataInMemoryOnSuccess: Bool = false
    fileprivate var _keepDownloadedDataInMemoryOnError: Bool = false
    fileprivate var _downloadTo: URL? = nil
    
    fileprivate var _failed: Bool = false
    var abortOnError: Bool = true

    init(session: URLSession, task: URLSessionTask, networker: JAMNetworking) {
        self._session = session
        self._task = task
        self._networker = networker
        self._numberOfAttempts = networker.numberOfAttempts
    }
    
    convenience init(
        session: URLSession,
        task: URLSessionUploadTask,
        networker: JAMNetworking,
        uploadData: Data
    ) {
        self.init(session: session, task: task, networker: networker)
        self._uploadData = uploadData
    }
    
    fileprivate func _resume() {
        _task.resume();
    }
    
    fileprivate func _cancel() {
        _task.cancel();
    }
    
    fileprivate func _complete(_ response: URLResponse?, error: NSError?) -> Bool {
        if let response = response as? HTTPURLResponse {
            if ((response.statusCode >= 200) && (response.statusCode < 300)) {
                if self._task is URLSessionDataTask {
                    _dataSuccessHandler?(_responseBody, response, error)
                } else if self._task is URLSessionDownloadTask {
                    _downloadSuccessHandler?(_downloadTo, response, error)
                }
                
                return true
            } else {
                _failed = true
                if self._task is URLSessionDataTask {
                    _dataErrorHandler?(_responseBody, response, error)
                } else if self._task is URLSessionDownloadTask {
                    _downloadErrorHandler?(nil, response, error)
                }
                
                return true
            }
        } else {
            _attempts += 1
            if ((_numberOfAttempts > 0) && (_attempts >= _numberOfAttempts)) {
                _failed = true
                if self._task is URLSessionDataTask {
                    _dataErrorHandler?(nil, response, error)
                } else if self._task is URLSessionDownloadTask {
                    _downloadErrorHandler?(nil, response, error)
                }
                
                return true
            } else {
                print("Networking: attempt \(self._attempts) of \(self._numberOfAttempts)")
                
                self._completedDataAmountInBytes = 0
                
                print("Length of upload: \(self._uploadData?.count)")
                
                if let task = self._task as? URLSessionUploadTask {
                    self._task = self._session.uploadTask(with: task.originalRequest!, from: self._uploadData!)
                } else if let task = self._task as? URLSessionDataTask {
                    self._task = self._session.dataTask(with: task.originalRequest!)
                } else if let task = self._task as? URLSessionDownloadTask {
                    self._task = self._session.downloadTask(with: task.originalRequest!)
                }
                
                self._task.resume()
                
                return false
            }
        }
    }
    
    fileprivate func _downloaded(_ response: URLResponse?, location: URL) {
        if let response = response as? HTTPURLResponse {
            if ((response.statusCode >= 200) && (response.statusCode < 300)) {
                if let downloadTo = _downloadTo {
                    let fileManager = FileManager.default
                    try! fileManager.moveItem(at: location, to: downloadTo)
                }
                
                if (_keepDownloadedDataInMemoryOnSuccess) {
                    _responseBody = try? Data(contentsOf: location)
                }
            } else {
                if (_keepDownloadedDataInMemoryOnError) {
                    _responseBody = try? Data(contentsOf: location)
                }
            }
        }
    }
}

class JAMNetworking: NSObject, URLSessionTaskDelegate, URLSessionDataDelegate, URLSessionDownloadDelegate {
    // MARK: - Public parameters
    
    var baseUri = ""
    var fullTimeoutInSeconds: Double = 30
    var noDataTimeoutInSeconds: Double = 5
    var numberOfAttempts: Int = 5
    
    // MARK: - Private & read-only vars

    //private var _dataTask: NSURLSessionDataTask?
    //private var _downloadTask: NSURLSessionDownloadTask?
    //private var _uploadTask: NSURLSessionUploadTask?
    fileprivate var _queue: Array<JAMNetworkingTask> = []
    fileprivate var _started: Bool = false
    
    fileprivate var _totalDataAmountOfCompletedTasksInBytes: Int64 = 0
    var totalDataAmountInBytes: Int64 {
        get {
            var result: Int64 = 0
            for task in self._queue {
                result += task._totalDataAmountInBytes
            }
            return self._totalDataAmountOfCompletedTasksInBytes + result
        }
    }
    
    fileprivate var _completedDataAmountOfCompletedTasksInBytes: Int64 = 0
    var completedDataAmountInBytes: Int64 {
        get {
            var result: Int64 = 0
            for task in self._queue {
                result += task._completedDataAmountInBytes
            }
            return self._completedDataAmountOfCompletedTasksInBytes + result
        }
    }
    
    fileprivate var _responseBody: Data?
    var responseBody: Data? {
        get { return _responseBody }
    }
    
    var progressHandler: ProgressHandler?
    var successHandler: CompletionHandler?
    var errorHandler: CompletionHandler?
    
    // MARK: - Public methods
    
    init(baseUri: String) {
        self.baseUri = baseUri
        let delegate: JAMAppDelegate = UIApplication.shared.delegate as! JAMAppDelegate
        let defaults: UserDefaults = delegate.defaults
        
        self.noDataTimeoutInSeconds = Double(defaults.networkNoDataTimeoutInSeconds)
        self.fullTimeoutInSeconds   = Double(defaults.networkFullTimeoutInSeconds)
        self.numberOfAttempts       = defaults.networkNumberOfAttempts
    }
    
    func request(_ uriComponent: String!, httpMethod: String!, rawData: Data!, onSuccess: DataCompletionHandler?, onError: DataCompletionHandler?) -> JAMNetworkingTask {
        let url: URL! = URL(string: uriComponent, relativeTo: URL(string: baseUri))
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = self.noDataTimeoutInSeconds
        sessionConfiguration.timeoutIntervalForResource = self.fullTimeoutInSeconds
        
        let session = Foundation.URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod
        urlRequest.httpBody = rawData
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        _responseBody = nil

        let dataTask = session.dataTask(with: urlRequest)
        let task = JAMNetworkingTask(session: session, task: dataTask, networker: self)
        task._totalDataAmountInBytes = Int64(rawData.count)
        task._dataSuccessHandler = onSuccess
        task._dataErrorHandler = onError
        
        _queue.append(task)
        
        return task
    }
    
    func request(_ uriComponent: String!, httpMethod: String!, data: AnyObject?, onSuccess: DataCompletionHandler?, onError: DataCompletionHandler?) -> JAMNetworkingTask? {
        if (data === nil) {
            return self.request(uriComponent, httpMethod: httpMethod, rawData: Data(), onSuccess: onSuccess, onError: onError)
        } else if let json = try? JSONSerialization.data(withJSONObject: data!, options: []) {
            return self.request(uriComponent, httpMethod: httpMethod, rawData: json, onSuccess: onSuccess, onError: onError)
        } else {
            return nil
        }
    }
    
    func download(
        _ uriComponent: String!,
        onProgress: ProgressHandler?,
        onSuccess: DownloadCompletionHandler?,
        onError: DownloadCompletionHandler?,
        downloadTo: URL?,
        keepInMemoryOnSuccess: Bool = false,
        keepInMemoryOnError: Bool = false
    ) -> JAMNetworkingTask
    {
        let url: URL! = URL(string: uriComponent, relativeTo: URL(string: baseUri))
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.requestCachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        sessionConfiguration.timeoutIntervalForRequest = self.noDataTimeoutInSeconds
        sessionConfiguration.timeoutIntervalForResource = self.fullTimeoutInSeconds
        
        let session = Foundation.URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        _responseBody = nil

        let downloadTask = session.downloadTask(with: urlRequest)
        let task = JAMNetworkingTask(session: session, task: downloadTask, networker: self)
        task._progressHandler = onProgress
        task._downloadSuccessHandler = onSuccess
        task._downloadErrorHandler = onError
        task._keepDownloadedDataInMemoryOnSuccess = keepInMemoryOnSuccess
        task._keepDownloadedDataInMemoryOnError   = keepInMemoryOnError
        task._downloadTo = downloadTo
        
        _queue.append(task)
        
        return task
    }
    
    func upload(
        _ uriComponent: String!,
        httpMethod: String!,
        rawData: Data!,
        onProgress: ProgressHandler?,
        onSuccess: DataCompletionHandler?,
        onError: DataCompletionHandler?
    ) -> JAMNetworkingTask
    {
        let url: URL! = URL(string: uriComponent, relativeTo: URL(string: baseUri))
        
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = self.noDataTimeoutInSeconds
        sessionConfiguration.timeoutIntervalForResource = self.fullTimeoutInSeconds
        
        let session = Foundation.URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        let urlRequest = NSMutableURLRequest(url: url)
        urlRequest.httpMethod = httpMethod
        
        _responseBody = nil

        let uploadTask = session.uploadTask(with: urlRequest as URLRequest, from: rawData)
        let task = JAMNetworkingTask(session: session, task: uploadTask, networker: self, uploadData: rawData)
        task._totalDataAmountInBytes = Int64(rawData.count)
        task._progressHandler = onProgress
        task._dataSuccessHandler = onSuccess
        task._dataErrorHandler = onError

        _queue.append(task)
        
        return task
    }
    
    func start() {
        if _queue.count > 0 {
            _started = true
            _queue.first?._resume()
        } else {
            self.successHandler?()
        }
    }
    
    func abort() {
        _queue.first?._cancel()
        _queue.removeAll(keepingCapacity: false)
        _started = false
        
        _totalDataAmountOfCompletedTasksInBytes     = 0
        _completedDataAmountOfCompletedTasksInBytes = 0
    }
    
    fileprivate func _nextTask() {
        if (self._queue.count == 0) {
            // the current task was cancelled
            _started = false
            
            _totalDataAmountOfCompletedTasksInBytes     = 0
            _completedDataAmountOfCompletedTasksInBytes = 0
            
            return
        }
        
        let completedTask = _queue.first!
        _queue.remove(at: 0)
        
        self._totalDataAmountOfCompletedTasksInBytes     += completedTask._totalDataAmountInBytes
        self._completedDataAmountOfCompletedTasksInBytes += completedTask._completedDataAmountInBytes
        
        if (completedTask._failed && completedTask.abortOnError) {
            self.abort()
        }
        
        if (_queue.count > 0) {
            _queue.first!._resume()
        } else {
            if (completedTask._failed) {
                self.errorHandler?()
            } else {
                self.successHandler?()
            }
            
            _started = false
            
            _totalDataAmountOfCompletedTasksInBytes     = 0
            _completedDataAmountOfCompletedTasksInBytes = 0
        }
    }
    
    // MARK: - URLSessionDataDelegate
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        //_responseBody = data
        if let curTask = _queue.first {
            var tempData: Data
            if let body = curTask._responseBody {
                tempData = body
            } else {
                tempData = Data()
            }
            tempData.append(data)
            curTask._responseBody = tempData
        }
    }
    
    // MARK: - URLSessionTaskDelegate
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let curTask = _queue.first {
            if (curTask._responseBody == nil) {
                curTask._responseBody = Data()
            }
            self._responseBody = curTask._responseBody
            if curTask._complete(task.response, error: error as NSError?) {
                self._nextTask()
            }
        }
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        _queue.first!._progressHandler?(bytesSent, totalBytesSent, totalBytesExpectedToSend)

        _queue.first!._completedDataAmountInBytes = totalBytesSent

        self.progressHandler?(bytesSent, self.completedDataAmountInBytes, self.totalDataAmountInBytes)
    }
    
    // MARK: - URLSessionDownloadDelegate
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        _queue.first!._downloaded(downloadTask.response, location: location)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        _queue.first!._progressHandler?(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite)
        
        _queue.first!._completedDataAmountInBytes = bytesWritten
        _queue.first!._totalDataAmountInBytes     = totalBytesExpectedToWrite
        
        self.progressHandler?(bytesWritten, self.completedDataAmountInBytes, self.totalDataAmountInBytes)
    }
}
