//
//  CoreDataDispatcher.swift
//  JustAMap
//
//  Created by Artur on 29/07/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import Foundation
import CoreData

class CoreDataDispatcher {
    static let sharedDispatcher = CoreDataDispatcher();
    
    fileprivate var _errors: [AnyObject] = []
    var errors: [AnyObject] {
        get { return _errors }
    }
    
    var managedObjectContext: NSManagedObjectContext?
    
    init() {}
    
    func newObject<T: NSManagedObject>() -> T {
        let namespacedClassName = NSStringFromClass(T.self)
        let className = namespacedClassName.components(separatedBy: ".").last! as String
        
        let result = NSEntityDescription.insertNewObject(
            forEntityName: className,
            into: managedObjectContext!
        ) as! T
        return result
    }
    
    func fetchObjectsMeetPredicate<T: NSManagedObject>(
        _ predicate: NSPredicate?,
        sortedByDescriptors: [NSSortDescriptor]
    ) -> [T]
    {
        let namespacedClassName = NSStringFromClass(T.self)
        let className = namespacedClassName.components(separatedBy: ".").last! as String
        
        /*let entity = NSEntityDescription.entityForName(
            className,
            inManagedObjectContext: managedObjectContext!
        )
        var fetchRequest = NSFetchRequest()
        fetchRequest.entity = entity*/
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: className)
        fetchRequest.predicate = predicate;
        fetchRequest.sortDescriptors = sortedByDescriptors;
        
        do {
            let fetchedObjects = try managedObjectContext!.fetch(fetchRequest)
            return fetchedObjects as! [T]
        } catch let error as NSError {
            _errors.append(error)
            return []
        }
    }
    
    func fetchObjectsMeetPredicate<T: NSManagedObject>(
        _ predicate: NSPredicate?,
        sortedBy: String,
        inAscendingOrder: Bool
    ) -> [T]
    {
        let sortDescriptor = NSSortDescriptor(key: sortedBy, ascending: inAscendingOrder)
        return self.fetchObjectsMeetPredicate(predicate, sortedByDescriptors: [sortDescriptor])
    }

    func fetchObjectsMeetPredicate<T: NSManagedObject>(_ predicate: NSPredicate?) -> [T]
    {
        return self.fetchObjectsMeetPredicate(predicate, sortedByDescriptors: [])
    }
    
    func countObjects(
        _ entityName: String,
        predicate: NSPredicate?
    ) -> Int
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = predicate;
        
        do {
            let count = try managedObjectContext!.count(for: fetchRequest)
            return count
        } catch let error as NSError {
            _errors.append(error)
            return 0
        }
    }
    
    @discardableResult func deleteObject(_ object: NSManagedObject) {
        managedObjectContext?.delete(object)
    }
    
    @discardableResult func deleteObjectsMeetPredicate<T: NSManagedObject>(_ predicate: NSPredicate?) -> [T] {
        let results = self.fetchObjectsMeetPredicate(predicate) as [T]
        
        for object in results {
            self.deleteObject(object)
        }
        
        return results
    }
    
    @discardableResult func commit() -> Bool {
        if (!managedObjectContext!.hasChanges) {
            print("Tried to save with NO CHANGES")
            return false
        }
        
        do {
            try managedObjectContext!.save()
            return true
        } catch let error as NSError {
            _errors.append(error)
            return false
        }
    }
}
