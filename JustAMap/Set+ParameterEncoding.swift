//
//  Set+ParameterEncoding.swift
//  JustAMap
//
//  Created by Artur on 23/01/2017.
//  Copyright © 2017 V&Co Ltd. All rights reserved.
//

import Foundation
import Alamofire

extension Set: ParameterEncoding {
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        let json = try JSONSerialization.data(withJSONObject: Array(self), options: [])
        request.httpBody = json
        return request
    }
}
