//
//  Message.swift
//  
//
//  Created by Artur on 29/08/16.
//
//

import Foundation
import CoreData

class Message: NSManagedObject {

    @NSManaged var sentAt: NSDate
    @NSManaged var text: String
    @NSManaged var photo: NSData
    @NSManaged var direction: NSNumber
    @NSManaged var correspondent: Advertisement

}
