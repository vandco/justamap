//
//  NSUserDefaults+Simplified.swift
//  JustAMap
//
//  Created by Artur on 26/07/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

extension UserDefaults {
    var isMapboxMetricsEnabled: Bool {
        get {
            if let isMapboxMetricsEnabled = self.value(forKey: "MGLMapboxMetricsEnabled") as? Bool {
                return isMapboxMetricsEnabled
            } else {
                self.isMapboxMetricsEnabled = false
                return self.isMapboxMetricsEnabled
            }
        }
        set {
            self.setValue(newValue, forKey: "MGLMapboxMetricsEnabled")
        }
    }

    var udid: String? {
        get { return self.value(forKey: "udid") as? String }
        set { self.set(newValue, forKey: "udid") }
    }

    var networkFullTimeoutInSeconds: Int {
        get {
            if let fullTimeoutString = self.value(forKey: "JAMNetworkFullTimeout") as? String {
                if let result = Int(fullTimeoutString) {
                    return result
                } else {
                    self.networkFullTimeoutInSeconds = 60 //86400
                    return self.networkFullTimeoutInSeconds
                }
            } else {
                self.networkFullTimeoutInSeconds = 60 //86400
                return self.networkFullTimeoutInSeconds
            }
        }
        set {
            let fullTimeoutString = "\(newValue)"
            self.set(fullTimeoutString, forKey: "JAMNetworkFullTimeout")
        }
    }
    
    var networkNoDataTimeoutInSeconds: Int {
        get {
            if let noDataTimeoutString = self.object(forKey: "JAMNetworkNoDataTimeout") as? String {
                if let result = Int(noDataTimeoutString) {
                    return result
                } else {
                    self.networkNoDataTimeoutInSeconds = 1
                    return self.networkNoDataTimeoutInSeconds
                }
            } else {
                self.networkNoDataTimeoutInSeconds = 1
                return self.networkNoDataTimeoutInSeconds
            }
        }
        set {
            let noDataTimeoutString = "\(newValue)"
            self.set(noDataTimeoutString, forKey: "JAMNetworkNoDataTimeout")
        }
    }
    
    var networkNumberOfAttempts: Int {
        get {
            if let numberOfAttemptsString = self.object(forKey: "JAMNetworkNumberOfAttempts") as? String {
                if let result = Int(numberOfAttemptsString) {
                    return result
                } else {
                    self.networkNumberOfAttempts = 3
                    return self.networkNumberOfAttempts
                }
            } else {
                self.networkNumberOfAttempts = 3
                return self.networkNumberOfAttempts
            }
        }
        set {
            let numberOfAttemptsString = "\(newValue)"
            self.set(numberOfAttemptsString, forKey: "JAMNetworkNumberOfAttempts")
        }
    }
    
    /*var numOfAdvertisementsToShow: Int {
        get {
            if let numOfAdvertisementsToShow = self.valueForKey("JAMNumberOfAdvertisementsToShow") as? Int {
                return numOfAdvertisementsToShow
            } else {
                self.numOfAdvertisementsToShow = 100
                return self.numOfAdvertisementsToShow
            }
        }
        set {
            //let numOfAdvertisementsToShowString = "\(newValue)"
            self.setValue(newValue, forKey: "JAMNumberOfAdvertisementsToShow")
        }
    }*/
    
    /*var lastAdvertisementsRefreshTimestamp: Int {
        get {
            if let lastAdvertisementsRefreshTimestamp = self.valueForKey("lastAdvertisementsRefreshTimestamp") as? Int {
                return lastAdvertisementsRefreshTimestamp
            } else {
                self.lastAdvertisementsRefreshTimestamp = 0
                return self.lastAdvertisementsRefreshTimestamp
            }
        }
        set {
            self.setValue(newValue, forKey: "lastAdvertisementsRefreshTimestamp")
        }
    }*/
    
    var lastAdvertisementsRefreshRegion: CGRect {
        get {
            if let lastAdvertisementsRefreshRegionString = self.value(forKey: "lastAdvertisementsRefreshRegionString") as? String
            {
                return CGRectFromString(lastAdvertisementsRefreshRegionString)
            } else {
                self.lastAdvertisementsRefreshRegion = CGRect.zero
                return self.lastAdvertisementsRefreshRegion
            }
        }
        set {
            self.setValue(NSStringFromCGRect(newValue), forKey: "lastAdvertisementsRefreshRegionString")
        }
    }
    
    var lastTimeAppLaunched: Date? {
        get {
            return self.value(forKey: "lastTimeAppLaunched") as? Date
        }
        set {
            if let newTime = newValue {
                self.setValue(newTime, forKey: "lastTimeAppLaunched")
            } else {
                self.removeObject(forKey: "lastTimeAppLaunched")
            }
        }
    }
    
    var baseURI: String? {
        get {
            return self.value(forKey: "baseURI") as? String
        }
        set {
            if let newTime = newValue {
                self.setValue(newTime, forKey: "baseURI")
            } else {
                self.removeObject(forKey: "baseURI")
            }
        }
    }
    
    func reset() {
        self.removeObject(forKey: "MGLMapboxMetricsEnabled")
        self.removeObject(forKey: "udid")
        self.removeObject(forKey: "JAMNetworkFullTimeout")
        self.removeObject(forKey: "JAMNetworkNoDataTimeout")
        self.removeObject(forKey: "JAMNetworkNumberOfAttempts")
        self.removeObject(forKey: "lastAdvertisementsRefreshRegionString")
        self.removeObject(forKey: "lastTimeAppLaunched")
        self.removeObject(forKey: "baseURI")
    }
}
