//
//  JAMPointAnnotation.swift
//  JustAMap
//
//  Created by Artur on 26/07/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit
import Mapbox

class JAMPointAnnotation: MGLPointAnnotation {
    var udid: String!
    var avatar: UIImage?
    var shortDescription: String!
    var submittedAt: Date?
    weak var calloutView: JAMMapAnnotationAdvertisementView?
    weak var tapGestureRecognizer: UITapGestureRecognizer?
}
