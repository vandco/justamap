//
//  JAMPhotoViewController.swift
//  JustAMap
//
//  Created by Artur on 02/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit
import Alamofire

class JAMPhotoViewController: UIViewController {

    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var lblLoadingStatus: UILabel!
    @IBOutlet weak var pbLoading: UIProgressView!

    lazy var delegate: JAMAppDelegate = { return (UIApplication.shared.delegate as! JAMAppDelegate) }()
    lazy var defaults: UserDefaults = { return self.delegate.defaults }()

    var advertisementVC: JAMAdverisementViewController!
    
    //var networker: JAMNetworking!
    var baseUri: String!
    var photoFull: UIImage?
    var udid: String!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewDidLoad() {
        //self.networker = JAMNetworking(baseUri: self.advertisementVC.networker.baseUri)
        self.baseUri = self.advertisementVC.baseUri
        
        if (self.photoFull === nil) {
            self.lblLoadingStatus.text = "Загрузка фото..."
            self.showLoading(animated: false)
            self.loadPhoto()
        } else {
            self.imgPhoto.image = self.photoFull!
            self.hideLoading(animated: false)
        }
    }
    
    func loadPhoto() {
        let alert = { (message: String) -> Void in
            let alertController = UIAlertController(
                title: "Загрузка фотографии",
                message: message,
                preferredStyle: UIAlertControllerStyle.alert
            )
            let retry = UIAlertAction(
                title: "Повтор",
                style: UIAlertActionStyle.default,
                handler: { (action: UIAlertAction!) -> Void in
                    self.loadPhoto()
                }
            )
            let cancel = UIAlertAction(
                title: "Отмена",
                style: UIAlertActionStyle.cancel,
                handler:  { (action: UIAlertAction!) -> Void in
                    _ = self.navigationController?.popViewController(animated: true)
                }
                
            )
            alertController.addAction(retry)
            alertController.addAction(cancel)
            self.present(alertController, animated: true, completion: nil)
        }

        let advertisement = (self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [self.udid])) as [Advertisement]).first!
        
        let downloadPhotoFullSize = { () -> Void in
            JAMAppDelegate.sharedSessionManager.request(
                self.baseUri + "/advertisements/" + self.udid + "/photo/full?access-token=" + self.defaults.udid!
            ).downloadProgress { (progress: Progress) in
                __UI_THREAD_TASK { () -> Void in
                    self.pbLoading.progress = Float(progress.fractionCompleted)
                }
            }.validate().responseData { (response: DataResponse<Data>) in
                switch response.result {
                case .success(let body):
                    self.photoFull = UIImage(data: body)
                    if (self.photoFull === nil) {
                        alert("Не удалось загрузить фотографию")
                    } else {
                        self.imgPhoto.image = self.photoFull
                        self.advertisementVC.photoFull = self.photoFull
                        
                        advertisement.photoFullSize = body
                        self.delegate.coreDataDispatcher.commit()
                        
                        self.hideLoading(animated: true)
                    }
                case .failure(let error):
                    if error is Alamofire.AFError {
                        let statusCode = response.response!.statusCode
                        let statusText = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                        alert("Неполадки на сервере\n\n\(statusCode) \(statusText)")
                    } else {
                        alert("Нет подключения к интернету")
                    }
                }
            }

            /*self.networker.download(
                "/advertisements/" + self.udid + "/photo/full?access-token=" + self.defaults.udid!,
                onProgress: { (bytesWrittenSinceLastCall: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) -> Void in
                    __UI_THREAD_TASK { () -> Void in
                        self.pbLoading.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
                    }
                },
                onSuccess: { (location: URL?, response: URLResponse?, error: NSError?) -> Void in
                    if let body = self.networker.responseBody {
                        self.photoFull = UIImage(data: body)
                        if (self.photoFull === nil) {
                            alert("Не удалось загрузить фотографию")
                        } else {
                            self.imgPhoto.image = self.photoFull
                            self.advertisementVC.photoFull = self.photoFull
                            
                            advertisement.photoFullSize = body
                            self.delegate.coreDataDispatcher.commit()
                            
                            self.hideLoading(animated: true)
                        }
                    } else {
                        alert("Ошибка сервера")
                    }
                },
                onError: { (_: URL?, response: URLResponse?, error: NSError?) -> Void in
                    if (error !== nil) {
                        alert("Проверьте подключение к интернету")
                    } else {
                        print(response!)
                        alert("Ошибка сервера")
                    }
                },
                downloadTo: nil,
                keepInMemoryOnSuccess: true,
                keepInMemoryOnError: false
            )
            self.networker.start()*/
        }
        
        if (advertisement.photoFullSize == nil) {
            self.showLoading(animated: false)
            downloadPhotoFullSize()
        } else {
            if let photoFullSize = UIImage(data: advertisement.photoFullSize!, scale: UIScreen.main.scale)
            {
                self.hideLoading(animated: false)
                self.imgPhoto.image = photoFullSize
                self.advertisementVC.photoFull = photoFullSize
            } else {
                self.showLoading(animated: false)
                downloadPhotoFullSize()
            }
        }
    }

    func showLoading(animated: Bool, completionHandler: @escaping () -> Void) -> Void {
        __UI_THREAD_TASK { () -> Void in
            let _showLoading = { () -> Void in
                self.loadingView.alpha = 1
                self.imgPhoto.alpha = 0
            }
            
            if (animated) {
                UIView.animate(withDuration: 0.5, animations: _showLoading, completion: { (_) -> Void in
                    completionHandler()
                })
            } else {
                _showLoading()
            }
        }
    }
    
    func showLoading(animated: Bool) -> Void {
        self.showLoading(animated: animated) {}
    }
    
    func hideLoading(animated: Bool, completionHandler: @escaping () -> Void) -> Void {
        __UI_THREAD_TASK { () -> Void in
            let _hideLoading = { () -> Void in
                self.loadingView.alpha = 0
                self.imgPhoto.alpha = 1
            }
            
            if (animated) {
                UIView.animate(withDuration: 0.5, animations: _hideLoading, completion: { (_) -> Void in
                    completionHandler()
                })
            } else {
                _hideLoading()
            }
        }
    }
    
    func hideLoading(animated: Bool) -> Void {
        self.hideLoading(animated: animated) {}
    }
    
}
