//
//  UIRefreshControl+beginRefreshing.swift
//  JustAMap
//
//  Created by Artur on 09/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//
//  http://stackoverflow.com/a/35468501/2175025
//

import UIKit

extension UIRefreshControl {
    func beginRefreshingManually() {
        if let scrollView = self.superview as? UIScrollView {
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - self.frame.height), animated: true)
        }
        self.beginRefreshing()
        self.sendActions(for: UIControlEvents.valueChanged)
    }
}
