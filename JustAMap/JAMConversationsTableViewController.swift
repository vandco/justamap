//
//  JAMConversationsTableViewController.swift
//  
//
//  Created by Artur on 09/08/16.
//
//

import UIKit
import Alamofire

class JAMConversationsTableViewController: UITableViewController {

    var mapVC: JAMMapViewController!

    lazy var delegate: JAMAppDelegate = { return (UIApplication.shared.delegate as! JAMAppDelegate) }()
    lazy var defaults: UserDefaults = { return self.delegate.defaults }()
    
    //var networker: JAMNetworking!
    var baseUri: String!
    
    var conversations = [String: JAMMessage]()
    
    var placeholder: UILabel!
    
    var processingVC: JAMProcessingViewController!
    
    // MARK: - Navigation Bar Enabled State
    
    var navBarEnabled: Bool {
        get { return self.navigationController!.navigationBar.isUserInteractionEnabled }
        set {
            self.navigationController!.navigationBar.isUserInteractionEnabled = newValue
            self.navigationController!.navigationBar.tintColor
                = (newValue) ? FOREGROUND_BUTTON_COLOR
                : FOREGROUND_DISABLED_BUTTON_COLOR
        }
    }
    
    // MARK: - Initialize View Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refreshButton = UIBarButtonItem(
            title: "Обновить",
            style: UIBarButtonItemStyle.plain,
            target: self,
            action: #selector(JAMConversationsTableViewController.refresh))
        self.navigationItem.rightBarButtonItem = refreshButton
        
        self.navBarEnabled = false
        
        //self.networker = JAMNetworking(baseUri: self.mapVC.networker.baseUri)
        self.baseUri = self.mapVC.baseUri
        
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: CGFloat.leastNormalMagnitude)) // because zero-height is just ignored
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 48
        
        self.placeholder = UILabel()
        self.placeholder.font = self.placeholder.font.withSize(20)
        self.placeholder.numberOfLines = 0
        self.placeholder.text = "Пока сообщений нет"
        self.placeholder.textAlignment = NSTextAlignment.center;
        self.placeholder.textColor = UIColor.lightGray
        self.placeholder.isHidden = true; // Initially hidden.
        self.tableView.addSubview(self.placeholder)

        //self.refreshControl?.addTarget(self, action: "refreshMessages:", forControlEvents: UIControlEvents.ValueChanged)
        
        self.firstLoadConversations()
        
        //self.refreshControl?.beginRefreshingManually()
        self.processingVC = JAMProcessingViewController(status: "Загрузка сообщений...")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.default
        self.navigationItem.titleView = UIView(frame: CGRect.zero)
        self.beginRefresh(animated: animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.placeholder.frame = self.tableView.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Refresh Messages

    func firstLoadConversations() {
        self.conversations.removeAll(keepingCapacity: true)
        
        let storedMessages = self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(nil, sortedBy: "sentAt", inAscendingOrder: false) as [Message]
        //println("Count of Stored Messages: \(count(storedMessages))")
        
        for storedMessage in storedMessages {
            //println("Stored Message: \(storedMessage)")

            if let draftMessage = (self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(
                NSPredicate(format: "(correspondentUDID = %@) AND (isShort = NO) AND (sentAt = nil)", argumentArray: [storedMessage.correspondentUDID])
                ) as [Message]).first
            {
                let message = JAMMessage(
                    text: draftMessage.text,
                    toUserUDID: draftMessage.correspondentUDID,
                    fromUserUDID: self.defaults.udid!,
                    sentAt: nil
                )
                message.isUnread = false
                message.storedMessage = draftMessage
                
                self.conversations[draftMessage.correspondentUDID] = message
            } else {
                let toUserUDID: String
                let fromUserUDID: String
                if (storedMessage.direction == Message.Direction.incoming) {
                    toUserUDID = self.defaults.udid!
                    fromUserUDID = storedMessage.correspondentUDID
                } else {
                    toUserUDID = storedMessage.correspondentUDID
                    fromUserUDID = self.defaults.udid!
                }
                
                let message = JAMSentMessage(
                    text: storedMessage.text,
                    toUserUDID: toUserUDID,
                    fromUserUDID: fromUserUDID,
                    sentAt: storedMessage.sentAt!
                )
                message.isUnread = storedMessage.isUnread.boolValue
                message.storedMessage = storedMessage
                
                if (self.conversations[storedMessage.correspondentUDID] == nil) {
                    self.conversations[storedMessage.correspondentUDID] = message
                }
            }
        }
        
        /*let sortedKeys = (self.conversations as NSDictionary).keysSortedByValueUsingSelector("compare:") as! [String]
        var newConversations = [String: JAMMessage]()
        for udid in sortedKeys {
            newConversations[udid] = self.conversations[udid]
        }
        self.conversations = newConversations*/
    }
    
    @objc func reloadMessagesFromLocalStorage() {
        self.firstLoadConversations()
    }
    
    @objc func refresh() {
        self.beginRefresh(animated: true)
    }
    
    func beginRefresh(animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NotificationKeys.MESSAGES_UPDATED_NOTIFICATION_KEY.notification, object: nil)
        
        self.navBarEnabled = false
        self.present(self.processingVC, animated: animated) { () -> Void in
            self.refreshMessages(self)
        }
    }
    
    func endRefresh() {
        self.dismiss(animated: true) { () -> Void in
            NotificationCenter.default.addObserver(self, selector: #selector(self.reloadMessagesFromLocalStorage), name: NotificationKeys.MESSAGES_UPDATED_NOTIFICATION_KEY.notification, object: nil)
            self.navBarEnabled = true
        }
    }
    
    @objc func refreshMessages(_ sender: AnyObject) {
        let alert = { (message: String) -> Void in
            let alertController = UIAlertController(
                title: "Загрузка сообщений",
                message: message,
                preferredStyle: UIAlertControllerStyle.alert
            )
            let retry = UIAlertAction(
                title: "Повтор",
                style: UIAlertActionStyle.default,
                handler: { (action: UIAlertAction!) -> Void in
                    self.refreshMessages(alertController)
                }
            )
            let cancel = UIAlertAction(
                title: "Отмена",
                style: UIAlertActionStyle.cancel,
                handler:  { (action: UIAlertAction!) -> Void in
                    self.endRefresh()
                }
                
            )
            alertController.addAction(retry)
            alertController.addAction(cancel)
            self.processingVC.present(alertController, animated: true, completion: nil)
        }
        
        let lastTimestampOfMessages: Date
        let storedMessages = self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(nil, sortedBy: "sentAt", inAscendingOrder: false) as [Message]
        if let newestStoredMessage = storedMessages.first {
            lastTimestampOfMessages = newestStoredMessage.sentAt!
        } else {
            lastTimestampOfMessages = Date(timeIntervalSince1970: 0)
        }
        
        var url: String = "/messages/afterTimestamp/"
        url += String(format: "%d", arguments: [Int(lastTimestampOfMessages.timeIntervalSince1970)])
        url += "?access-token=" + defaults.udid!
        
        JAMAppDelegate.sharedSessionManager.request(self.baseUri + url).validate().responseJSON { (response: DataResponse<Any>) in
            switch response.result {
            case .success(let conversations as [String: [String: AnyObject]]):
                let afterProcessingMessages = { () -> Void in
                    self.tableView.reloadData()
                    
                    self.placeholder.isHidden = !self.conversations.isEmpty
                    
                    self.endRefresh()
                }
                
                if (conversations.count > 0) {
                    var numProcessedMessages: Int = 0
                    
                    for (udid, messageDict) in conversations {
                        let shortText = messageDict["shortText"]! as! String
                        let sentAt = Date(mySqlTimestamp: messageDict["sent_at"]! as! String)!
                        let incoming = messageDict["incoming"] as! Bool
                        var toUserUDID: String!
                        var fromUserUDID: String!
                        if incoming {
                            toUserUDID = self.defaults.udid!
                            fromUserUDID = udid
                        } else {
                            toUserUDID = udid
                            fromUserUDID = self.defaults.udid!
                        }
                        
                        let message = JAMMessage(
                            text: shortText,
                            toUserUDID: toUserUDID,
                            fromUserUDID: fromUserUDID,
                            sentAt: sentAt
                        )
                        self.conversations[udid] = message
                        
                        // Update local storage
                        let messageToStore = self.delegate.coreDataDispatcher.newObject() as Message
                        if incoming {
                            messageToStore.direction = Message.Direction.incoming
                        } else {
                            messageToStore.direction = Message.Direction.outgoing
                        }
                        messageToStore.text = shortText
                        messageToStore.isShort = true
                        messageToStore.sentAt = sentAt
                        
                        message.storedMessage = messageToStore
                        
                        messageToStore.linkToAdvertisementUDID(
                            udid,
                            scale: self.delegate.scale,
                            circleImage: { (image: UIImage) -> UIImage in
                                return image.circleImage()
                        },
                            coreDataDispatcher: self.delegate.coreDataDispatcher,
                            baseUri: self.baseUri,
                            selfUDID: self.defaults.udid!,
                            completionHandler: { (advertisement, message) -> Void in
                                numProcessedMessages += 1
                                if (numProcessedMessages >= conversations.count) {
                                    self.delegate.coreDataDispatcher.commit()
                                    
                                    afterProcessingMessages()
                                }
                        },
                            errorHandler: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                                numProcessedMessages += 1
                                if (numProcessedMessages >= conversations.count) {
                                    self.delegate.coreDataDispatcher.commit()
                                    
                                    afterProcessingMessages()
                                }
                        }
                        )
                    }
                } else {
                    afterProcessingMessages()
                }
            case .success:
                alert("Неожиданный ответ сервера\n\n\(response.data!)")
                self.endRefresh()
            case .failure(let error):
                if error is Alamofire.AFError {
                    alert("Неожиданный ответ сервера\n\n\(response.data!)")
                } else {
                    alert("Проверьте подключение к интернету")
                }
                self.endRefresh()
            }
        }

        /*networker.request(
            url,
            httpMethod: "GET",
            data: nil,
            onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                if let conversations = try! JSONSerialization.jsonObject(with: body!, options: []) as? [String: [String: AnyObject]]
                {
                    //var conversationsDict = [String: JAMMessage]()
                    print(conversations)
                    
                    let afterProcessingMessages = { () -> Void in
                        self.tableView.reloadData()
                        
                        self.placeholder.isHidden = !self.conversations.isEmpty
                        
                        self.endRefresh()
                    }
                    
                    if (conversations.count > 0) {
                        var numProcessedMessages: Int = 0

                        for (udid, messageDict) in conversations {
                            var shortText = messageDict["shortText"]! as! String
                            var sentAt = Date(mySqlTimestamp: messageDict["sent_at"]! as! String)!
                            var incoming = messageDict["incoming"] as! Bool
                            var toUserUDID: String!
                            var fromUserUDID: String!
                            if incoming {
                                toUserUDID = self.defaults.udid!
                                fromUserUDID = udid
                            } else {
                                toUserUDID = udid
                                fromUserUDID = self.defaults.udid!
                            }
                            
                            var message = JAMMessage(
                                text: shortText,
                                toUserUDID: toUserUDID,
                                fromUserUDID: fromUserUDID,
                                sentAt: sentAt
                            )
                            self.conversations[udid] = message
                            
                            // Update local storage
                            var messageToStore = self.delegate.coreDataDispatcher.newObject() as Message
                            if incoming {
                                messageToStore.direction = Message.Direction.incoming
                            } else {
                                messageToStore.direction = Message.Direction.outgoing
                            }
                            messageToStore.text = shortText
                            messageToStore.isShort = true
                            messageToStore.sentAt = sentAt
                            
                            message.storedMessage = messageToStore
                            
                            messageToStore.linkToAdvertisementUDID(
                                udid,
                                scale: self.delegate.scale,
                                circleImage: { (image: UIImage) -> UIImage in
                                    return image.circleImage()
                                },
                                coreDataDispatcher: self.delegate.coreDataDispatcher,
                                networker: self.networker,
                                selfUDID: self.defaults.udid!,
                                completionHandler: { (advertisement, message) -> Void in
                                    numProcessedMessages += 1
                                    if (numProcessedMessages >= conversations.count) {
                                        self.delegate.coreDataDispatcher.commit()
                                        
                                        afterProcessingMessages()
                                    }
                                },
                                errorHandler: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                                    numProcessedMessages += 1
                                    if (numProcessedMessages >= conversations.count) {
                                        self.delegate.coreDataDispatcher.commit()
                                        
                                        afterProcessingMessages()
                                    }
                                }
                            )
                        }
                    } else {
                        afterProcessingMessages()
                    }
                } else {
                    if let body = body {
                        if let body = String(data: body, encoding: String.Encoding.utf8) {
                            print(body)
                        }
                    }

                    alert("Неожиданный ответ сервера")
                }
            },
            onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                if (response === nil) {
                    alert("Проверьте подключение к интернету")
                } else {
                    print(response)
                    
                    if let body = body {
                        if let body = String(data: body, encoding: String.Encoding.utf8) {
                            print(body)
                        }
                    }
                    
                    alert("Неожиданный ответ сервера")
                }
                
                self.endRefresh()
            }
        )
        networker.start()*/
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "conversation", for: indexPath) as! JAMConversationsTableViewCell
        
        let udid = Array(self.conversations.keys)[indexPath.row]
        let message = self.conversations[udid]!
        let advertisement = message.storedMessage!.correspondent
        cell.lblShortMessage.text = message.shortText
        cell.hasUnread = message.isUnread
        cell.hasDraft = (message.sentAt == nil)
        if let photoMicro = advertisement?.photoMicro {
            cell.imgAvatar.image = UIImage(data: photoMicro as Data, scale: UIScreen.main.scale)
        }
        cell.conversationsVC = self
        cell.udid = udid

        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let udid = Array(self.conversations.keys)[indexPath.row]
        let message = self.conversations[udid]!
        
        message.isUnread = false
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        
        self.performSegue(withIdentifier: "messages", sender: udid)
    }
    
    // MARK: - Tap on avatar
    
    func avatarTapped(_ udid: String) {
        self.performSegue(withIdentifier: "advertisement", sender: udid)
    }
    
    // MARK: - Long Tap to delete conversation
    
    func longTap(_ udid: String) {
        let menu = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: UIAlertControllerStyle.actionSheet
        )
        
        let delete = UIAlertAction(
            title: "Удалить",
            style: UIAlertActionStyle.destructive,
            handler: { (action: UIAlertAction!) -> Void in
                let alert = UIAlertController(
                    title: "Подтвердите удаление",
                    message: "Диалог с данным пользователем будет удалён",
                    preferredStyle: UIAlertControllerStyle.alert)
                let cancel = UIAlertAction(
                    title: "Отмена",
                    style: UIAlertActionStyle.cancel,
                    handler: nil
                )
                let confirm = UIAlertAction(
                    title: "Удалить",
                    style: UIAlertActionStyle.destructive,
                    handler: { (action: UIAlertAction!) -> Void in
                        let processingVC = JAMProcessingViewController(status: "Удаление...")
                        
                        var alert: ((String) -> Void)!
                        
                        let url = "/messages/\(udid)?access-token=\(self.defaults.udid!)"
                        
                        self.present(processingVC, animated: true, completion: nil)
                        
                        let _delete = { () -> Void in
                            JAMAppDelegate.sharedSessionManager.request(self.baseUri + url, method: .delete).validate().responseData { (response: DataResponse<Data>) in
                                switch response.result {
                                case .success:
                                    // Delete from the local storage
                                    let deletedMessages = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(NSPredicate(format: "(correspondentUDID = %@) AND (isShort = NO)", argumentArray: [udid])) as [Message]
                                    print("Count of Deleted Messages: \(deletedMessages.count)")
                                    
                                    //self.tableView.reloadData()
                                    if let rowIndex = Array(self.conversations.keys).index(of: udid) {
                                        self.conversations.removeValue(forKey: udid)
                                        
                                        let indexPath = IndexPath(row: rowIndex, section: 0)
                                        self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.middle)
                                        self.placeholder.isHidden = !self.conversations.isEmpty
                                    }
                                    
                                    self.mapVC.updateNumberOfUnreadMessages()
                                    
                                    self.dismiss(animated: true, completion: nil)
                                case .failure(let error):
                                    if error is Alamofire.AFError {
                                        let statusCode = response.response!.statusCode
                                        let statusText = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                                        alert("Неполадки на сервере\n\n\(statusCode) \(statusText)")
                                    } else {
                                        alert("Проверьте подключение к интернету")
                                    }
                                }
                            }
                            
                            /*self.networker.request(
                                url,
                                httpMethod: "DELETE",
                                data: nil,
                                onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                                    // Delete from the local storage
                                    let deletedMessages = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(NSPredicate(format: "(correspondentUDID = %@) AND (isShort = NO)", argumentArray: [udid])) as [Message]
                                    print("Count of Deleted Messages: \(deletedMessages.count)")
                                    
                                    //self.tableView.reloadData()
                                    if let rowIndex = Array(self.conversations.keys).index(of: udid) {
                                        self.conversations.removeValue(forKey: udid)
                                        
                                        let indexPath = IndexPath(row: rowIndex, section: 0)
                                        self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.middle)
                                        self.placeholder.isHidden = !self.conversations.isEmpty
                                    }
                                    
                                    self.mapVC.updateNumberOfUnreadMessages()

                                    self.dismiss(animated: true, completion: nil)
                                },
                                onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                                    if (response != nil) {
                                        print(response!)
                                    }
                                    
                                    if (body != nil) {
                                        print(String(data: body!, encoding: String.Encoding.utf8)!)
                                    }
                                    
                                    if (error !== nil) {
                                        alert("Проверьте подключение к интернету")
                                    } else {
                                        alert("Неполадки на сервере")
                                    }
                                }
                            )
                            self.networker.start()*/
                        }
                        
                        let _alert_workaround = { (message: String) -> Void in
                            let alertController = UIAlertController(
                                title: "Удаление диалога",
                                message: message,
                                preferredStyle: UIAlertControllerStyle.alert
                            )
                            
                            let retry = UIAlertAction(
                                title: "Повтор",
                                style: UIAlertActionStyle.default,
                                handler: { (action: UIAlertAction!) -> Void in
                                    _delete()
                                }
                            )
                            let cancel = UIAlertAction(
                                title: "Отмена",
                                style: UIAlertActionStyle.cancel,
                                handler: { (action: UIAlertAction!) -> Void in
                                    self.dismiss(animated: true, completion: nil)
                                }
                            )
                            
                            alertController.addAction(retry)
                            alertController.addAction(cancel)
                            
                            processingVC.present(alertController, animated: true, completion: nil)
                        }
                        alert = _alert_workaround
                        
                        _delete()
                    }
                )
                
                alert.addAction(confirm)
                alert.addAction(cancel)
                self.present(alert, animated: true, completion: nil)
            }
        )
        let cancel = UIAlertAction(
            title: "Отмена",
            style: UIAlertActionStyle.cancel,
            handler: nil
        )
        
        menu.addAction(delete)
        menu.addAction(cancel)
        
        self.present(menu, animated: true, completion: nil)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? JAMAdverisementViewController {
            vc.udid = sender! as! String
            vc.mapVC = self.mapVC
        }

        if let vc = segue.destination as? JAMMessagesViewController {
            let udid = sender! as! String
            let index = Array(self.conversations.keys).index(of: udid)!
            let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as! JAMConversationsTableViewCell
            
            vc.avatar = cell.imgAvatar.image
            vc.udid = udid
            vc.mapVC = self.mapVC
            vc.conversationsVC = self
        }
    }

}
