//
//  JAMButton.swift
//  JustAMap
//
//  Created by Artur on 02/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

@IBDesignable
class JAMButton: UIButton {
    
    fileprivate var _untouchedBackgroundColor: UIColor!
    fileprivate var _untouchedTitleLabelColor: UIColor!
    fileprivate var _touched: Bool = false
    
    func setupColours() {
        if (self.backgroundColor === nil) {
            self.backgroundColor = UIColor.white
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupColours()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setupColours()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.animateAfterTouchBegin()
        self.sendActions(for: UIControlEvents.touchDown)
    }
    
    func animateAfterTouchBegin() {
        if !self._touched {
            self._untouchedBackgroundColor = self.backgroundColor
            self._untouchedTitleLabelColor = self.titleLabel!.textColor
            self._touched = true
        }
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.backgroundColor = self._untouchedTitleLabelColor
            self.titleLabel!.textColor = self._untouchedBackgroundColor
        })
    }
    
    func animateAfterTouchEnd() {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.backgroundColor = self._untouchedBackgroundColor
            self.titleLabel!.textColor = self._untouchedTitleLabelColor
        })
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self._touched = false
        self.animateAfterTouchEnd()
        self.sendActions(for: UIControlEvents.touchCancel)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first! 
        let point = touch.location(in: self)
        if !CGRect(origin: CGPoint.zero, size: self.frame.size).contains(point) {
            self.animateAfterTouchEnd()
        } else {
            self.animateAfterTouchBegin()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self._touched = false

        self.animateAfterTouchEnd()
        
        let touch = touches.first! 
        let point = touch.location(in: self)
        if CGRect(origin: CGPoint.zero, size: self.frame.size).contains(point) {
            self.sendActions(for: UIControlEvents.touchUpInside)
        }
    }
    
}
