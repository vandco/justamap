//
//  JAMMapViewController.swift
//  JustAMap
//
//  Created by Artur on 25/07/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit
import Mapbox
import Alamofire

let CLCOORDINATE_EPSILON: CGFloat = 0.005
func CLCOORDINATES_EQUAL(_ coord1: CLLocationCoordinate2D, _ coord2: CLLocationCoordinate2D) -> Bool {
    return (
        (fabs(CGFloat(coord1.latitude) - CGFloat(coord2.latitude)) < CLCOORDINATE_EPSILON)
        && (fabs(CGFloat(coord1.longitude) - CGFloat(coord2.longitude)) < CLCOORDINATE_EPSILON)
    )
}

func ==(coord1: CLLocationCoordinate2D, coord2: CLLocationCoordinate2D) -> Bool {
    return CLCOORDINATES_EQUAL(coord1, coord2)
}

class JAMMapViewController: UIViewController, UITabBarDelegate, MGLMapViewDelegate/*, MGLCalloutViewDelegate*/ {

    @IBOutlet weak var moveToCurLoc: UIView!
    var mapView: MGLMapView!
    @IBOutlet weak var tbiAdvertisement: UITabBarItem!
    @IBOutlet weak var tbiMessages: UITabBarItem!
    @IBOutlet weak var tbiRefresh: UITabBarItem!
    //@IBOutlet weak var tbiPing: UITabBarItem!
    @IBOutlet weak var tbiPartners: UITabBarItem!
    @IBOutlet weak var tabBar: UITabBar!
    
    var launchVC: JAMLaunchViewController?
    
    var annotations: Dictionary<String, JAMPointAnnotation> = [String: JAMPointAnnotation]()
    fileprivate var _hasOwnAdvertisement: Bool = false
    var hasOwnAdvertisement: Bool {
        get { return _hasOwnAdvertisement }
        set {
            if newValue {
                __UI_THREAD_TASK({ () -> Void in
                    let filledStar = UIImage(named: "Star Filled Blue")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                    let tintedFilledStar = filledStar.tintImage(color: self.tabBar.tintColor)
                    self.tbiAdvertisement.image = filledStar
                    self.tbiAdvertisement.selectedImage = tintedFilledStar
                })
            } else {
                __UI_THREAD_TASK({ () -> Void in
                    let emptyStar = UIImage(named: "Star Blue")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                    let tintedEmptyStar = emptyStar.tintImage(color: self.tabBar.tintColor)
                    self.tbiAdvertisement.image = emptyStar
                    self.tbiAdvertisement.selectedImage = tintedEmptyStar
                })
            }
            
            _hasOwnAdvertisement = newValue
        }
    }
    
    var isBanned: Bool = false
    
    //var networker: JAMNetworking!
    var baseUri: String!
    
    lazy var delegate: JAMAppDelegate = { return (UIApplication.shared.delegate as! JAMAppDelegate) }()
    lazy var defaults: UserDefaults = { return self.delegate.defaults }()
    
    fileprivate var _annotationRectForTap: CGRect?
    fileprivate var _annotationForTap: JAMPointAnnotation?
    fileprivate var _avatarOverCallout: UIImageView?
    
    fileprivate var _isFirstLoad: Bool = true
    fileprivate var _pendingForGPSAuth: Bool = false
    fileprivate var _mapViewIsReady: Bool = false
    
    fileprivate var _userLocationAnnotation: JAMUserLocationAnnotation!
    
    fileprivate var _regionFreshPolygon: MGLPolygon?
    
    fileprivate var _keepAliveTimer: Timer?
    fileprivate var _keepAliveTimerInterval: TimeInterval = 30
    fileprivate var _refreshingAdvertisementsTimer: Timer?
    
    fileprivate var _requestedExplicitlyEditingOfOwnAdvertisement: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationItem.titleView = UIView(frame: CGRect.zero)
        self.tabBar.selectedItem = nil
        
        self.updateNumberOfUnreadMessages()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func onAuthFailure(completion: @escaping RequestRetryCompletion, request: Request) -> Void {
        __UI_THREAD_TASK { () -> Void in
            let backgroundVC: UIViewController
            
            /*if let launchVC = self.launchVC {
                backgroundVC = launchVC
            } else {
                backgroundVC = self
            }*/
            
            backgroundVC = (self.delegate.window?.rootViewController)!
            
            let alertController = UIAlertController(
                title: "Ошибка авторизации",
                message: "Попробуйте переустановить приложение",
                preferredStyle: UIAlertControllerStyle.alert
            )
            let retry = UIAlertAction(
                title: "Повтор",
                style: UIAlertActionStyle.default,
                handler: { (action: UIAlertAction!) -> Void in
                    completion(true, 0)
                }
            )
            let cancel = UIAlertAction(
                title: "Отмена",
                style: UIAlertActionStyle.cancel,
                handler:  { (action: UIAlertAction!) -> Void in
                    completion(false, 0)
                }
            )
            let cleanInstall = UIAlertAction(
                title: "Переустановить",
                style: UIAlertActionStyle.destructive,
                handler:  { (action: UIAlertAction!) -> Void in
                    let alertCleanInstallController = UIAlertController(
                        title: "Переустановка приложения",
                        message: "Приложение сбросит все свои данные и закроется.",
                        preferredStyle: UIAlertControllerStyle.alert
                    )
                    let ok = UIAlertAction(
                        title: "Переустановить",
                        style: UIAlertActionStyle.destructive,
                        handler: { (action: UIAlertAction!) -> Void in
                            self.defaults.reset()
                             _ = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(nil) as [Advertisement]
                             _ = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(nil) as [Message]
                             self.delegate.coreDataDispatcher.commit()
                             exit(0)
                        }
                    )
                    let cancel = UIAlertAction(
                        title: "Только выйти",
                        style: UIAlertActionStyle.cancel,
                        handler:  { (action: UIAlertAction!) -> Void in
                            exit(0)
                        }
                    )
                    alertCleanInstallController.addAction(ok)
                    alertCleanInstallController.addAction(cancel)
                    
                    backgroundVC.present(alertCleanInstallController, animated: true)
                }
            )
            alertController.addAction(retry)
            alertController.addAction(cancel)
            alertController.addAction(cleanInstall)
            
            backgroundVC.present(alertController, animated: true, completion: nil)
        }
    }
    
    fileprivate var _onAuthFailure: AuthFailedHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.tintColor = FOREGROUND_BUTTON_COLOR
        self.navigationController!.navigationBar.isOpaque = true
        self.navigationController!.interactivePopGestureRecognizer?.isEnabled = false

        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: FOREGROUND_BUTTON_COLOR], for: UIControlState())
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: FOREGROUND_DISABLED_BUTTON_COLOR], for: UIControlState.disabled)
        
        self.tbiAdvertisement.image = self.tbiAdvertisement.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        self.tbiMessages.image = self.tbiMessages.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        self.tbiRefresh.image = self.tbiRefresh.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        self.tbiPartners.image = self.tbiPartners.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.tabBar.tintColor = FOREGROUND_BUTTON_COLOR
        //UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: FOREGROUND_BUTTON_COLOR], forState: UIControlState.Selected)
        
        self._userLocationAnnotation = JAMUserLocationAnnotation(image: UIImage(named: "Red Dot"))

        if _isFirstLoad {
            self._onAuthFailure = {(completion: @escaping RequestRetryCompletion, request: Request) -> Void in
                self.onAuthFailure(completion: completion, request: request)
            }
            
            let sessionConfiguration = URLSessionConfiguration.default
            sessionConfiguration.timeoutIntervalForRequest = Double(self.defaults.networkNoDataTimeoutInSeconds)
            sessionConfiguration.timeoutIntervalForResource = Double(self.defaults.networkFullTimeoutInSeconds)
            
            JAMAppDelegate.sharedSessionManager = Alamofire.SessionManager(configuration: sessionConfiguration)
            
            let requestHandler = JAMRequestHandler(numOfRetries: UInt(self.defaults.networkNumberOfAttempts))
            requestHandler.onAuthFailure = self._onAuthFailure
            JAMAppDelegate.sharedSessionManager.retrier = requestHandler

            self.initGeoLocation()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func authorizeForGPS() {
        if (self._pendingForGPSAuth) {
            self._pendingForGPSAuth = false
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        }

        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied)
            || (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.restricted)
        {
            let bundleDisplayName: String = Bundle.main.infoDictionary!["CFBundleDisplayName"] as! String
            
            let alert = UIAlertController(
                title: "Нет доступа к GPS",
                message: "Пожалуйста, разрешите приложению \(bundleDisplayName) доступ к вашему местоположению:\n\nПриложение «Настройки» → \(bundleDisplayName) → Геопозиция → Используя",
                preferredStyle: UIAlertControllerStyle.alert
            )
            
            let settings = UIAlertAction(
                title: "Настройки",
                style: UIAlertActionStyle.default,
                handler: { (_: UIAlertAction!) -> Void in
                    self._pendingForGPSAuth = true
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    NotificationCenter.default.addObserver(self, selector: #selector(JAMMapViewController.authorizeForGPS), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
                }
            )
            let retry = UIAlertAction(
                title: "Повтор",
                style: UIAlertActionStyle.cancel,
                handler: { (_: UIAlertAction!) -> Void in
                    self.authorizeForGPS()
                }
            )
            
            alert.addAction(settings)
            alert.addAction(retry)
            
            self.launchVC!.present(alert, animated: true, completion: nil)
        } else if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined) {
            self._pendingForGPSAuth = true
            NotificationCenter.default.addObserver(self, selector: #selector(JAMMapViewController.authorizeForGPS), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        } else {
            self.delegate.locationManager!.stopUpdatingLocation()
            self.delegate.locationManager!.startUpdatingLocation()
            NotificationCenter.default.addObserver(self, selector: #selector(self.locationBecameAvailable), name: NotificationKeys.LOCATION_UPDATED_NOTIFICATION_KEY.notification, object: nil)
        }
    }
    
    func initGeoLocation() {
        NotificationCenter.default.post(name: NotificationKeys.LAUNCH_STATUS_UPDATED_NOTIFICATION_KEY.notification, object: self, userInfo: ["status": "Определение местоположения..."])

        if (self.delegate.location == nil) {
            self.authorizeForGPS()
        } else {
            self.locationBecameAvailable()
        }
    }

    @objc func locationBecameAvailable() {
        if let location = self.delegate.location {
            NotificationCenter.default.removeObserver(self, name: NotificationKeys.LOCATION_UPDATED_NOTIFICATION_KEY.notification, object: nil)
            
            self.getBaseURI {
                self.gotBaseURI(location)
            }
        }
    }
    
    func getBaseURI(_ completionHandler: (() -> Void)?) {
        NotificationCenter.default.post(name: NotificationKeys.LAUNCH_STATUS_UPDATED_NOTIFICATION_KEY.notification, object: self, userInfo: ["status": "Соединение..."])

        //self.networker = JAMNetworking(baseUri: "http://just-a-map.vandco.ru/")
        self.baseUri = "http://just-a-map.vandco.ru"
        
        var url = "/balancer"
        if let udid = self.defaults.udid {
            url += "?access-token=" + udid
        }
        
        JAMAppDelegate.sharedSessionManager.request(self.baseUri + url).validate().responseJSON { (response: DataResponse<Any>) in
            switch response.result {
            case .success:
                self.defaults.baseURI = self.baseUri
            case .failure:
                break
            }

            __ASYNC_BLOCK {
                completionHandler?()
            }
        }
    }
    
    func gotBaseURI(_ location: CLLocationCoordinate2D) {
        let deviceWidth = UIScreen.main.bounds.width
        let deviceHeight = UIScreen.main.bounds.height
        let deviceDiagonal = sqrt(deviceWidth * deviceWidth + deviceHeight * deviceHeight)
        let metersInPixel = 10000.0 / Double(deviceDiagonal)
        let latInRadians = location.latitude / 180.0 * M_PI
        let minZoom = log2(EARTH_EQUATORIAL_CIRCUMFERENCE_IN_METERS * cos(latInRadians) / metersInPixel) - 8
        
        __UI_THREAD_TASK {
            self.mapView = MGLMapView(frame: self.view.frame, styleURL: URL(string: "mapbox://styles/shelart/cir7uxh1b0011h5ni1en5ka27"))
            self.mapView.translatesAutoresizingMaskIntoConstraints = false
            self.mapView.delegate = self
            self.mapView.attributionButton.isHidden = true
            self.mapView.minimumZoomLevel = minZoom
            self.view.addSubview(self.mapView)
            self.view.bringSubview(toFront: self.tabBar)
            self.view.bringSubview(toFront: self.moveToCurLoc)
            
            let views = ["mapView": self.mapView, "tabBar": self.tabBar] as [String : Any]
            let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[mapView]-0-[tabBar]-0-|", options: [], metrics: nil, views: views)
            let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[mapView]-0-|", options: [], metrics: nil, views: views)
            self.view.addConstraints(verticalConstraints)
            self.view.addConstraints(horizontalConstraints)
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            
            self.mapView.setCenter(location, zoomLevel: 12, animated: false)
            
            if self.defaults.udid != nil {
                self.firstLoadAdvertisements()
            } else {
                NotificationCenter.default.post(name: NotificationKeys.LAUNCH_STATUS_UPDATED_NOTIFICATION_KEY.notification, object: self, userInfo: ["status": "Регистрация устройства..."])
                
                let udid = randomAlphaNumericString(32, useLowercase: false, useDigits: true, useUppercase: true)
                
                let udidDict = [
                    "udid": udid
                ]
                
                var registerUDID: ((Void) -> Void)!
                let _registerUDID_workaround: ((Void) -> Void) = {
                    JAMAppDelegate.sharedSessionManager.request(
                        self.baseUri + "/users",
                        method: .post,
                        parameters: udidDict,
                        encoding: JSONEncoding.default
                    ).responseJSON(completionHandler: { (response: DataResponse<Any>) in
                        switch response.result {
                        case .success:
                            // We have not requested Alamofire validation
                            // So any response from the server is correct,
                            // even if a device is already registered
                            self.defaults.udid = udid
                            self.firstLoadAdvertisements()
                        case .failure:
                            let alertController = UIAlertController(
                                title: "Регистрация устройства",
                                message: "Нет подключения к интернету",
                                preferredStyle: UIAlertControllerStyle.alert
                            )
                            let retry = UIAlertAction(
                                title: "Повтор",
                                style: UIAlertActionStyle.default,
                                handler: { (action: UIAlertAction!) -> Void in
                                    registerUDID()
                                }
                            )
                            alertController.addAction(retry)
                            
                            self.launchVC!.present(alertController, animated: true, completion: nil)
                        }
                    })
                    
                    /*self.networker.request(
                        "/users",
                        httpMethod: "POST",
                        data: udidDict as AnyObject?,
                        onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                            self.defaults.udid = udid
                            
                            self.firstLoadAdvertisements()
                        },
                        onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                            if (error != nil) {
                                let alertController = UIAlertController(
                                    title: "Регистрация устройства",
                                    message: "Нет подключения к интернету",
                                    preferredStyle: UIAlertControllerStyle.alert
                                )
                                let retry = UIAlertAction(
                                    title: "Повтор",
                                    style: UIAlertActionStyle.default,
                                    handler: { (action: UIAlertAction!) -> Void in
                                        registerUDID()
                                    }
                                )
                                alertController.addAction(retry)
                                
                                self.launchVC!.present(alertController, animated: true, completion: nil)
                            } else {
                                // Already registered
                                self.defaults.udid = udid
                                
                                self.firstLoadAdvertisements()
                            }
                        }
                        )!.abortOnError = false
                    self.networker.start()*/
                }
                registerUDID = _registerUDID_workaround
                registerUDID()
            }
        }
    }
    
    @objc func showGPS() {
        if let location = self.delegate.location {
            self.mapView.removeAnnotation(self._userLocationAnnotation)
            self._userLocationAnnotation.coordinate = location
            //self.mapView.addAnnotation(self._userLocationAnnotation)
        }
    }
    
    func firstLoadAdvertisements() {
        if self.defaults.udid != nil {
            NotificationCenter.default.post(name: NotificationKeys.LAUNCH_STATUS_UPDATED_NOTIFICATION_KEY.notification, object: self, userInfo: ["status": "Очистка..."])

            // Clean local storage if midnight passed over
            if let lastTimeAppLaunched = self.defaults.lastTimeAppLaunched {
                var calendar = Calendar(identifier: Calendar.Identifier.gregorian)
                
                if let tz = TimeZone(identifier: "Europe/Moscow") {
                    calendar.timeZone = tz
                } else {
                    calendar.timeZone = TimeZone(secondsFromGMT: 3 * 3600)!
                }
                
                let dayLastTimeAppLaunched = calendar.component(Calendar.Component.day, from: lastTimeAppLaunched)
                let monthLastTimeAppLaunched = calendar.component(Calendar.Component.month, from: lastTimeAppLaunched)
                let yearLastTimeAppLaunched = calendar.component(Calendar.Component.year, from: lastTimeAppLaunched)
                
                var componentsLastTimeAppLaunched = DateComponents()
                componentsLastTimeAppLaunched.day = dayLastTimeAppLaunched
                componentsLastTimeAppLaunched.month = monthLastTimeAppLaunched
                componentsLastTimeAppLaunched.year = yearLastTimeAppLaunched

                let lastMidnight = calendar.date(from: componentsLastTimeAppLaunched)!.addingTimeInterval(3600 * 24)
                
                //let currentDay = calendar.component(NSCalendarUnit.CalendarUnitDay, fromDate: NSDate())
                
                if (Date() > lastMidnight) {
                    _ = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(nil) as [Advertisement]
                    _ = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(nil) as [Message]
                    self.delegate.coreDataDispatcher.commit()
                }
            }
            self.defaults.lastTimeAppLaunched = Date()
            
            NotificationCenter.default.post(name: NotificationKeys.LAUNCH_STATUS_UPDATED_NOTIFICATION_KEY.notification, object: self, userInfo: ["status": "Загрузка объявлений..."])

            // Load from local storage
            let advertisements = self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(nil) as [Advertisement]
            for advertisement in advertisements {
                let annotation = JAMPointAnnotation()
                annotation.udid = advertisement.udid
                annotation.coordinate = CLLocationCoordinate2D(
                    latitude: CLLocationDegrees(advertisement.latitude),
                    longitude: CLLocationDegrees(advertisement.longitude)
                )
                annotation.shortDescription = advertisement.descShort
                annotation.title = annotation.shortDescription
                annotation.submittedAt = advertisement.submittedAt
                
                if let storedPhotoMicro = advertisement.photoMicro {
                    if let circledPhoto = UIImage(data: storedPhotoMicro, scale: UIScreen.main.scale)
                    {
                        annotation.avatar = circledPhoto
                    }
                }
                
                self.annotations[annotation.udid] = annotation
                
                if (advertisement.udid == self.defaults.udid!) {
                    self.hasOwnAdvertisement = true
                }
            }
            
            self.mapView.addAnnotations(Array(self.annotations.values))
            self.showGPS()

            // Update from the server
            self.refreshAdvertisements({ () -> Void in
                __UI_THREAD_TASK {
                    NotificationCenter.default.post(name: NotificationKeys.LAUNCH_STATUS_UPDATED_NOTIFICATION_KEY.notification, object: self, userInfo: ["status": "Загрузка сообщений..."])
                }
                
                self.keepAliveAndObtainNewMessages() {
                    NotificationCenter.default.addObserver(self, selector: #selector(self.showGPS), name: NotificationKeys.LOCATION_UPDATED_NOTIFICATION_KEY.notification, object: nil)
                    NotificationCenter.default.post(name: NotificationKeys.LAUNCH_COMPLETED_NOTIFICATION_KEY.notification, object: self)

                    self._isFirstLoad = false
                    
                    self.registerForUpdatingAdvertisements()
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if (item === tbiRefresh) {
            self.defaults.lastAdvertisementsRefreshRegion = CGRect.zero
            tabBar.selectedItem = nil
            self.refreshAdvertisements() {}
        } else if (item === tbiAdvertisement) {
            if (self.hasOwnAdvertisement) {
                let menu = UIAlertController(
                    title: nil,
                    message: "Объявление уже размещено",
                    preferredStyle: UIAlertControllerStyle.actionSheet
                )
                
                let edit = UIAlertAction(
                    title: "Изменить",
                    style: UIAlertActionStyle.default,
                    handler: { (action: UIAlertAction!) -> Void in
                        self._requestedExplicitlyEditingOfOwnAdvertisement = true
                        self.performSegue(withIdentifier: "advertisement", sender: self.defaults.udid!)
                    }
                )
                let delete = UIAlertAction(
                    title: "Удалить",
                    style: UIAlertActionStyle.destructive,
                    handler: { (action: UIAlertAction!) -> Void in
                        let alert = UIAlertController(title: "Подтвердите удаление", message: "Все сообщения будут удалены", preferredStyle: UIAlertControllerStyle.alert)
                        let cancel = UIAlertAction(
                            title: "Отмена",
                            style: UIAlertActionStyle.cancel,
                            handler: { (action: UIAlertAction!) -> Void in
                                self.tabBar.selectedItem = nil
                            }
                        )
                        let confirm = UIAlertAction(
                            title: "Удалить",
                            style: UIAlertActionStyle.destructive,
                            handler: { (action: UIAlertAction!) -> Void in
                                self.tbiAdvertisement.isEnabled = false
                                
                                var deleteAdvertisement: (() -> Void)!
                                
                                let alert = { (message: String) -> Void in
                                    let alertController = UIAlertController(
                                        title: "Удаление объявления",
                                        message: message,
                                        preferredStyle: UIAlertControllerStyle.alert
                                    )
                                    let retry = UIAlertAction(
                                        title: "Повтор",
                                        style: UIAlertActionStyle.default,
                                        handler: { (action: UIAlertAction!) -> Void in
                                            deleteAdvertisement()
                                        }
                                    )
                                    let cancel = UIAlertAction(
                                        title: "Отмена",
                                        style: UIAlertActionStyle.cancel,
                                        handler: { (action: UIAlertAction!) -> Void in
                                            self.tbiAdvertisement.isEnabled = true
                                            self.tabBar.selectedItem = nil
                                        }
                                    )
                                    alertController.addAction(retry)
                                    alertController.addAction(cancel)
                                    self.present(alertController, animated: true, completion: nil)
                                }
                                
                                let _deleteAdvertisement_workaround = { () -> Void in
                                    JAMAppDelegate.sharedSessionManager.request(
                                        self.baseUri + "/advertisements/" + self.defaults.udid! + "?access-token=" + self.defaults.udid!,
                                        method: .delete
                                    ).validate().responseData(completionHandler: { (response: DataResponse<Data>) in
                                        switch response.result {
                                        case .success:
                                            // Update local storage
                                            _ = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [self.defaults.udid!])) as [Advertisement]
                                            self.delegate.coreDataDispatcher.commit()
                                            
                                            // Delete the pin from the map
                                            if let annotation = self.annotations[self.defaults.udid!] {
                                                self.mapView.removeAnnotation(annotation)
                                                self.annotations.removeValue(forKey: self.defaults.udid!)
                                            }
                                            
                                            // Empty the star on the map
                                            self.hasOwnAdvertisement = false
                                            
                                            self.tbiAdvertisement.isEnabled = true
                                            self.tabBar.selectedItem = nil
                                        case .failure(let error):
                                            if error is Alamofire.AFError {
                                                let statusCode = response.response!.statusCode
                                                let statusText = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                                                alert("Неполадки на сервере\n\n\(statusCode) \(statusText)")
                                            } else {
                                                alert("Проверьте подключение к интернету")
                                            }
                                        }
                                    })
                                    
                                    /*self.networker.request(
                                        "/advertisements/" + self.defaults.udid! + "?access-token=" + self.defaults.udid!,
                                        httpMethod: "DELETE",
                                        data: nil,
                                        onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                                            // Update local storage
                                            self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [self.defaults.udid!])) as [Advertisement]
                                            self.delegate.coreDataDispatcher.commit()
                                            
                                            // Delete the pin from the map
                                            if let annotation = self.annotations[self.defaults.udid!] {
                                                self.mapView.removeAnnotation(annotation)
                                                self.annotations.removeValue(forKey: self.defaults.udid!)
                                            }
                                            
                                            // Empty the star on the map
                                            self.hasOwnAdvertisement = false
                                            
                                            self.tbiAdvertisement.isEnabled = true
                                            self.tabBar.selectedItem = nil
                                        },
                                        onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                                            if (response != nil) {
                                                print(response!)
                                            }
                                            
                                            if (body != nil) {
                                                print(String(data: body!, encoding: String.Encoding.utf8)!)
                                            }
                                            
                                            if (error !== nil) {
                                                alert("Проверьте подключение к интернету")
                                            } else {
                                                alert("Неполадки на сервере")
                                            }
                                        }
                                    )
                                    self.networker.start()*/
                                }
                                
                                deleteAdvertisement = _deleteAdvertisement_workaround
                                deleteAdvertisement()
                            }
                        )
                        
                        alert.addAction(confirm)
                        alert.addAction(cancel)
                        self.present(alert, animated: true, completion: nil)
                    }
                )
                let cancel = UIAlertAction(
                    title: "Отмена",
                    style: UIAlertActionStyle.cancel,
                    handler: { (action: UIAlertAction!) -> Void in
                        self.tabBar.selectedItem = nil
                    }
                )
                
                menu.addAction(edit)
                menu.addAction(delete)
                menu.addAction(cancel)
                
                self.present(menu, animated: true, completion: nil)
            } else {
                self._requestedExplicitlyEditingOfOwnAdvertisement = true
                self.performSegue(withIdentifier: "advertisement", sender: self.defaults.udid!)
            }
        } else if (item == tbiMessages) {
            if (!self.hasOwnAdvertisement) {
                let alert = UIAlertController(title: nil, message: "Сообщения доступны после размещения объявления", preferredStyle: UIAlertControllerStyle.alert)
                let cancel = UIAlertAction(
                    title: "OK",
                    style: UIAlertActionStyle.cancel,
                    handler: { (action: UIAlertAction!) -> Void in
                        self.tabBar.selectedItem = nil
                    }
                )
                alert.addAction(cancel)
                self.present(alert, animated: true, completion: nil)
            } else {
                self.performSegue(withIdentifier: "conversations", sender: self)
            }
        //} else if (item == tbiPing) {
        //    self.performSegueWithIdentifier("ping", sender: self)
        } else if (item == tbiPartners) {
            self.performSegue(withIdentifier: "partners", sender: self)
        }
    }
    
    func refreshAdvertisements(_ completionHandler: (() -> Void)?) {
        let alert = { (message: String) -> Void in
            __UI_THREAD_TASK { () -> Void in
                let alertController = UIAlertController(
                    title: "Загрузка объявлений",
                    message: message,
                    preferredStyle: UIAlertControllerStyle.alert
                )
                let retry = UIAlertAction(
                    title: "Повтор",
                    style: UIAlertActionStyle.default,
                    handler: { (action: UIAlertAction!) -> Void in
                        self.refreshAdvertisements(completionHandler)
                    }
                )
                let cancel = UIAlertAction(
                    title: "Отмена",
                    style: UIAlertActionStyle.cancel,
                    handler:  { (action: UIAlertAction!) -> Void in
                        self.tbiRefresh.isEnabled = true
                        completionHandler?()
                    }
                )
                /*let cleanInstall = UIAlertAction(
                    title: "Переустановить",
                    style: UIAlertActionStyle.destructive,
                    handler:  { (action: UIAlertAction!) -> Void in
                        let alertCleanInstallController = UIAlertController(
                            title: "Переустановка приложения",
                            message: "Приложение сбросит все свои данные и закроется.",
                            preferredStyle: UIAlertControllerStyle.alert
                        )
                        let ok = UIAlertAction(
                            title: "Переустановить",
                            style: UIAlertActionStyle.destructive,
                            handler: { (action: UIAlertAction!) -> Void in
                                self.defaults.reset()
                                _ = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(nil) as [Advertisement]
                                _ = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(nil) as [Message]
                                self.delegate.coreDataDispatcher.commit()
                                exit(0)
                            }
                        )
                        let cancel = UIAlertAction(
                            title: "Только выйти",
                            style: UIAlertActionStyle.cancel,
                            handler:  { (action: UIAlertAction!) -> Void in
                                exit(0)
                            }
                        )
                        alertCleanInstallController.addAction(ok)
                        alertCleanInstallController.addAction(cancel)
                        
                        alertController.present(alertCleanInstallController, animated: true)
                    }
                )*/
                alertController.addAction(retry)
                alertController.addAction(cancel)
                //alertController.addAction(cleanInstall)

                if let launchVC = self.launchVC {
                    launchVC.present(alertController, animated: true, completion: nil)
                } else {
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
        var somethingChanged: Bool = false
        
        var loadAvatarForPoint: ((_ index: Int, _ completionHandler: (() -> Void)?) -> Void)!
        let _loadAvatarForPoint_workaround = { (index: Int, completionHandler: (() -> Void)?) -> Void in
            if (index >= self.annotations.count) {
                self.delegate.coreDataDispatcher.commit()

                self.mapView.addAnnotations(Array(self.annotations.values))
                if self._regionFreshPolygon != nil {
                    //self.mapView.addAnnotation(regionFreshPolygon)
                }
                
                __UI_THREAD_TASK{ () -> Void in
                    self.showGPS()
                    
                    self.tbiRefresh.badgeValue = nil
                    self.tbiRefresh.isEnabled = true
                    self.tbiAdvertisement.isEnabled = true
                    self.tbiMessages.isEnabled = true
                    self.tbiPartners.isEnabled = true
                }
                
                completionHandler?()
                
                NotificationCenter.default.post(name: NotificationKeys.ADVERTISEMENTS_REFRESHED_NOTIFICATION_KEY.notification, object: nil, userInfo: nil)
                
                return
            }
            
            __UI_THREAD_TASK{ () -> Void in
                self.tbiRefresh.badgeValue = String(format: "%d", arguments: [self.annotations.count - index])
            }
            
            let curUDID = Array(self.annotations.keys)[index]
            
            let advertisement = (self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [curUDID])) as [Advertisement]).first!
            
            //let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
            JAMAppDelegate.sharedSessionManager.request(
                self.baseUri + "/advertisements/" + curUDID + "/photo/micro/x" + self.delegate.scale + "?access-token=" + self.defaults.udid!
            ).validate().responseData(completionHandler: { (response: DataResponse<Data>) in
                switch response.result {
                case .success(let data):
                    if let downloadedPhoto = UIImage(data: data, scale: UIScreen.main.scale) {
                        let requiredSize = CGSize(width: AVATAR_SIZE, height: AVATAR_SIZE)
                        let photo: UIImage
                        if !downloadedPhoto.size.equalTo(requiredSize) {
                            photo = downloadedPhoto.scaleImage(scaledToSize: requiredSize)
                        } else {
                            photo = downloadedPhoto
                        }
                        
                        let circledPhoto: UIImage = photo.circleImage()
                        
                        self.annotations[curUDID]!.avatar = circledPhoto
                        advertisement.photoMicro = UIImagePNGRepresentation(circledPhoto)
                    }
                    __ASYNC_BLOCK { () -> Void in
                        loadAvatarForPoint(index + 1, completionHandler)
                    }
                case .failure(let error):
                    if error is Alamofire.AFError {
                        if response.response?.statusCode == 404 {
                            __ASYNC_BLOCK { () -> Void in
                                loadAvatarForPoint(index + 1, completionHandler)
                            }
                            return
                        } else if response.response?.statusCode == 401 {
                        } else {
                            let statusCode = response.response!.statusCode
                            let statusText = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                            alert("Неполадки на сервере\n\n\(statusCode) \(statusText)")
                        }
                    } else {
                        alert("Нет подключения к интернету")
                    }
                }
            })
            
            /*self.networker.download(
                "/advertisements/" + curUDID + "/photo/micro/x" + self.delegate.scale + "?access-token=" + self.defaults.udid!,
                onProgress: nil,
                onSuccess: { (location: URL?, response: URLResponse?, error: NSError?) -> Void in
                    if let body = self.networker.responseBody {
                        if let downloadedPhoto = UIImage(data: body, scale: UIScreen.main.scale) {
                            let requiredSize = CGSize(width: AVATAR_SIZE, height: AVATAR_SIZE)
                            let photo: UIImage
                            if !downloadedPhoto.size.equalTo(requiredSize) {
                                photo = downloadedPhoto.scaleImage(scaledToSize: requiredSize)
                            } else {
                                photo = downloadedPhoto
                            }
                            
                            let circledPhoto: UIImage = photo.circleImage()
                            
                            self.annotations[curUDID]!.avatar = circledPhoto
                            advertisement.photoMicro = UIImagePNGRepresentation(circledPhoto)
                        }
                    }
                    __ASYNC_BLOCK { () -> Void in
                        loadAvatarForPoint(index + 1, completionHandler)
                    }
                },
                onError: { (_: URL?, response: URLResponse?, error: NSError?) -> Void in
                    self.annotations[curUDID]!.avatar = UIImage.createCircle(diameter: AVATAR_SIZE, fillColor: UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 1))

                    if let response = response {
                        print(response)
                        if let response = response as? HTTPURLResponse {
                            if response.statusCode == 404 {
                                __ASYNC_BLOCK { () -> Void in
                                    loadAvatarForPoint(index + 1, completionHandler)
                                }
                                return
                            }
                        }
                    }
                    
                    if (error !== nil) {
                        alert("Нет подключения к интернету")
                    } else {
                        __ASYNC_BLOCK { () -> Void in
                            loadAvatarForPoint(index + 1, completionHandler)
                        }
                    }
                },
                downloadTo: nil,
                keepInMemoryOnSuccess: true,
                keepInMemoryOnError: false
            )
            self.networker.start()*/
        }
        loadAvatarForPoint = _loadAvatarForPoint_workaround
        
        let geoBounds = self.mapView.visibleCoordinateBounds
        var northLatitude = geoBounds.ne.latitude
        var southLatitude = geoBounds.sw.latitude
        var westLongitude = geoBounds.sw.longitude
        var eastLongitude = geoBounds.ne.longitude
        
        var nw = CLLocationCoordinate2D(latitude: northLatitude, longitude: westLongitude)
        var se = CLLocationCoordinate2D(latitude: southLatitude, longitude: eastLongitude)
        
        let nwPoint = CGPoint(x: nw.longitude, y: nw.latitude)
        let sePoint = CGPoint(x: se.longitude, y: se.latitude)
        let currentVisibleRegionX = nwPoint.x
        let currentVisibleRegionY = sePoint.y
        let currentVisibleRegionWidth = sePoint.x - nwPoint.x
        let currentVisibleRegionHeight = nwPoint.y - sePoint.y
        
        let currentVisibleRegion = CGRect(
            x: currentVisibleRegionX,
            y: currentVisibleRegionY,
            width: currentVisibleRegionWidth,
            height: currentVisibleRegionHeight
        )

        let currentRegion = CGRect(
            x: currentVisibleRegionX - currentVisibleRegionWidth * 2,
            y: currentVisibleRegionY - currentVisibleRegionHeight * 2,
            width: currentVisibleRegionWidth * 5,
            height: currentVisibleRegionHeight * 5
        )
        
        if (!self.defaults.lastAdvertisementsRefreshRegion.contains(currentVisibleRegion) || self._isFirstLoad)
        {
            let newRegion: CGRect
            
            newRegion = currentRegion
            
            northLatitude = CLLocationDegrees(newRegion.maxY)
            westLongitude = CLLocationDegrees(newRegion.minX)
            southLatitude = CLLocationDegrees(newRegion.minY)
            eastLongitude = CLLocationDegrees(newRegion.maxX)
            
            nw = CLLocationCoordinate2D(latitude: northLatitude, longitude: westLongitude)
            se = CLLocationCoordinate2D(latitude: southLatitude, longitude: eastLongitude)
            
            self.defaults.lastAdvertisementsRefreshRegion = newRegion
            
            /*var polyCoords = [
                nw,
                CLLocationCoordinate2D(latitude: northLatitude, longitude: eastLongitude),
                se,
                CLLocationCoordinate2D(latitude: southLatitude, longitude: westLongitude),
            ]*/
            if let regionFreshPolygon = self._regionFreshPolygon {
                self.mapView.removeAnnotation(regionFreshPolygon)
                self._regionFreshPolygon = nil
            }
            //self._regionFreshPolygon = MGLPolygon(coordinates: &polyCoords, count: UInt(polyCoords.count))
            //self.mapView.addAnnotation(self._regionFreshPolygon!)
            
            // include all advertisements with corresponding messages
            var advertisementsWithMessages = Set<String>()
            let storedMessages = self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(nil) as [Message]
            
            for storedMessage in storedMessages {
                advertisementsWithMessages.insert(storedMessage.correspondentUDID)
            }
            
            var url: String = String(format: "/advertisements/index/northWest/%lf/%lf", arguments: [nw.latitude, nw.longitude])
            url += String(format: "/southEast/%lf/%lf", arguments: [se.latitude, se.longitude])
            url += "/afterTimestamp/0"
            url += "/withAdvertisements"
            url += "?access-token=" + defaults.udid!
            
            print("URL: \(url)")
            
            self.tbiRefresh.isEnabled = false
            self.tbiAdvertisement.isEnabled = false
            self.tbiMessages.isEnabled = false
            self.tbiPartners.isEnabled = false
            
            JAMAppDelegate.sharedSessionManager.request(
                self.baseUri + url,
                method: .post,
                parameters: [:], //[String](advertisementsWithMessages),
                encoding: advertisementsWithMessages
            ).validate().responseJSON(completionHandler: { (response: DataResponse<Any>) in
                switch response.result {
                case .success(let advertisementsDict as [[String: AnyObject]]):
                    _ = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(nil) as [Advertisement]
                    
                    if let annotations = self.mapView.annotations {
                        self.mapView.removeAnnotations(annotations)
                    }
                    self.annotations.removeAll(keepingCapacity: true)
                    
                    for advertisement in advertisementsDict {
                        somethingChanged = true
                        
                        let point = JAMPointAnnotation()
                        point.udid = advertisement["udid"]! as! String
                        point.coordinate = CLLocationCoordinate2D(
                            latitude: CLLocationDegrees((advertisement["latitude"]! as! NSNumber).doubleValue),
                            longitude: CLLocationDegrees((advertisement["longitude"]! as! NSNumber).doubleValue)
                        )
                        point.shortDescription = advertisement["shortDescription"] as? String
                        point.title = point.shortDescription
                        
                        if let submittedAtMySQLString = advertisement["submitted_at"] as? String {
                            point.submittedAt = Date(mySqlTimestamp: submittedAtMySQLString)
                        }
                        
                        var doNotAppend = false
                        if (point.udid == self.defaults.udid!) {
                            self.hasOwnAdvertisement = true
                            if let lockedAtMySQLString = advertisement["locked_at"] as? String {
                                if Date(mySqlTimestamp: lockedAtMySQLString) != nil {
                                    self.isBanned = true
                                }
                            }
                        } else {
                            if let lockedAtMySQLString = advertisement["locked_at"] as? String {
                                if Date(mySqlTimestamp: lockedAtMySQLString) != nil {
                                    doNotAppend = true
                                }
                            }
                        }
                        
                        let advertisement: Advertisement
                        advertisement = self.delegate.coreDataDispatcher.newObject() as Advertisement
                        advertisement.udid = point.udid
                        
                        if !doNotAppend {
                            self.annotations[point.udid] = point
                            
                            advertisement.descShort = point.shortDescription
                            advertisement.submittedAt = point.submittedAt!
                            advertisement.latitude = point.coordinate.latitude
                            advertisement.longitude = point.coordinate.longitude
                        }
                    }
                    
                    if somethingChanged {
                        self.tbiRefresh.badgeValue = String(format: "%d", arguments: [self.annotations.count])
                        __ASYNC_BLOCK { () -> Void in
                            loadAvatarForPoint(0, completionHandler)
                        }
                    } else {
                        self.tbiRefresh.badgeValue = nil
                        self.tbiRefresh.isEnabled = true
                        self.tbiAdvertisement.isEnabled = true
                        self.tbiMessages.isEnabled = true
                        self.tbiPartners.isEnabled = true
                        completionHandler?()
                    }
                case .failure(let error):
                    if error is Alamofire.AFError {
                        let statusCode = response.response!.statusCode
                        let statusText = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                        alert("Неполадки на сервере\n\n\(statusCode) \(statusText)")
                    } else {
                        alert("Нет подключения к интернету")
                    }
                default:
                    fatalError("Swift compiler was not so stupid as the developer think")
                }
            })
            
            /*networker.request(
                url,
                httpMethod: "POST",
                data: [String](advertisementsWithMessages) as AnyObject?,
                onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                    let bodyStr = String(data: body!, encoding: String.Encoding.utf8)
                    print(bodyStr!)
                    
                    if let advertisementsDict = try! JSONSerialization.jsonObject(with: body!, options: []) as? [[String: AnyObject]]
                    {
                        self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(nil) as [Advertisement]
                        
                        if let annotations = self.mapView.annotations {
                            self.mapView.removeAnnotations(annotations)
                        }
                        self.annotations.removeAll(keepingCapacity: true)
                        
                        for advertisement in advertisementsDict {
                            somethingChanged = true
                            
                            let point = JAMPointAnnotation()
                            point.udid = advertisement["udid"]! as! String
                            point.coordinate = CLLocationCoordinate2D(
                                latitude: CLLocationDegrees((advertisement["latitude"]! as! NSNumber).doubleValue),
                                longitude: CLLocationDegrees((advertisement["longitude"]! as! NSNumber).doubleValue)
                            )
                            point.shortDescription = advertisement["shortDescription"] as? String
                            point.title = point.shortDescription
                            
                            if let submittedAtMySQLString = advertisement["submitted_at"] as? String {
                                point.submittedAt = Date(mySqlTimestamp: submittedAtMySQLString)
                            }
                            
                            var doNotAppend = false
                            if (point.udid == self.defaults.udid!) {
                                self.hasOwnAdvertisement = true
                                if let lockedAtMySQLString = advertisement["locked_at"] as? String {
                                    if Date(mySqlTimestamp: lockedAtMySQLString) != nil {
                                        self.isBanned = true
                                    }
                                }
                            } else {
                                if let lockedAtMySQLString = advertisement["locked_at"] as? String {
                                    if Date(mySqlTimestamp: lockedAtMySQLString) != nil {
                                        doNotAppend = true
                                    }
                                }
                            }
                            
                            let advertisement: Advertisement
                            advertisement = self.delegate.coreDataDispatcher.newObject() as Advertisement
                            advertisement.udid = point.udid
                            
                            if !doNotAppend {
                                self.annotations[point.udid] = point
                                
                                advertisement.descShort = point.shortDescription
                                advertisement.submittedAt = point.submittedAt!
                                advertisement.latitude = point.coordinate.latitude
                                advertisement.longitude = point.coordinate.longitude
                            }
                        }
                        
                        if somethingChanged {
                            self.tbiRefresh.badgeValue = String(format: "%d", arguments: [self.annotations.count])
                            __ASYNC_BLOCK { () -> Void in
                                loadAvatarForPoint(0, completionHandler)
                            }
                        } else {
                            self.tbiRefresh.badgeValue = nil
                            self.tbiRefresh.isEnabled = true
                            self.tbiAdvertisement.isEnabled = true
                            self.tbiMessages.isEnabled = true
                            self.tbiPartners.isEnabled = true
                            completionHandler?()
                        }
                    } else {
                        alert("Неожиданный ответ сервера")
                    }
                },
                onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                    if (response != nil) {
                        print(response!)
                    }
                    
                    if (body != nil) {
                        print(String(data: body!, encoding: String.Encoding.utf8)!)
                    }
                    
                    if (error !== nil) {
                        alert("Нет подключения к интернету")
                    } else {
                        alert("Неполадки на сервере")
                    }
                }
            )
            networker.start()*/
        } else {
            completionHandler?()
        }
    }
    
    // MARK: - Current Location Button
    
    @IBAction func centerOnCurrentLocation(_ sender: UIButton) {
        if let loc = self.delegate.location {
            self.mapView.setCenter(loc, animated: true)
        } else {
            let alertController = UIAlertController(
                title: "Нет доступа к GPS",
                message: "Не удалось определить ваше текущее местоположение.",
                preferredStyle: UIAlertControllerStyle.alert
            )
            let cancel = UIAlertAction(
                title: "OK",
                style: UIAlertActionStyle.cancel,
                handler: nil
                
            )
            alertController.addAction(cancel)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - MGLMapViewDelegate methods
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        self._mapViewIsReady = true
    }
    
    func mapView(_ mapView: MGLMapView, regionDidChangeAnimated animated: Bool) {
        if (!self._isFirstLoad) {
            self.refreshAdvertisements() {}
        }
    }
    
    func mapView(_ mapView: MGLMapView, alphaForShapeAnnotation annotation: MGLShape) -> CGFloat {
        return 0.1
    }
    
    func mapView(_ mapView: MGLMapView, strokeColorForShapeAnnotation annotation: MGLShape) -> UIColor {
        return UIColor.white
    }
    
    func mapView(_ mapView: MGLMapView, fillColorForPolygonAnnotation annotation: MGLPolygon) -> UIColor {
        //return UIColor(red: 59/255.0, green: 178/255.0, blue: 208/255.0, alpha: 1)
        return UIColor.green
    }
    
    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        if let annotation = annotation as? JAMPointAnnotation {
            if let avatar = annotation.avatar {
                return MGLAnnotationImage(image: avatar, reuseIdentifier: annotation.udid + String(format: "%d", [Date().timeIntervalSince1970]))
            }
        }
        
        if let annotation = annotation as? JAMUserLocationAnnotation {
            return annotation.annotationImage
        }
        
        return nil
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return false
    }
    
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        if let annotation = annotation as? JAMPointAnnotation {
            // Convert the annotation coordinates to a current screen coordinates
            // (centre of annotation view)
            let annotationPositionOnScreen = mapView.convert(annotation.coordinate, toPointTo: self.view)
            
            // Add callout
            let callout = JAMMapAnnotationAdvertisementView.instanceFromNib()
            callout.translatesAutoresizingMaskIntoConstraints = false
            callout.controller = self
            callout.representedObject = annotation
            callout.lblShortDescription!.text = annotation.shortDescription
            
            self.view.addSubview(callout)
            callout.setNeedsLayout()
            callout.layoutIfNeeded()
            
            let screenSize = UIScreen.main.bounds.size
            let sideMargin: CGFloat = 0
            
            let posYOfCallout: CGFloat
            if (annotationPositionOnScreen.y < screenSize.height / 2) {
                posYOfCallout = annotationPositionOnScreen.y + AVATAR_SIZE / 2
            } else {
                posYOfCallout = annotationPositionOnScreen.y - AVATAR_SIZE / 2 - callout.frame.size.height
            }
            
            let topOfCallout = NSLayoutConstraint(
                item: callout,
                attribute: NSLayoutAttribute.top,
                relatedBy: NSLayoutRelation.equal,
                toItem: self.view,
                attribute: NSLayoutAttribute.top,
                multiplier: 1,
                constant: posYOfCallout
            )
            let leftOfCallout = NSLayoutConstraint(
                item: callout,
                attribute: NSLayoutAttribute.leading,
                relatedBy: NSLayoutRelation.equal,
                toItem: self.view,
                attribute: NSLayoutAttribute.leading,
                multiplier: 1,
                constant: sideMargin
            )
            let rightOfCallout = NSLayoutConstraint(
                item: self.view,
                attribute: NSLayoutAttribute.trailing,
                relatedBy: NSLayoutRelation.equal,
                toItem: callout,
                attribute: NSLayoutAttribute.trailing,
                multiplier: 1,
                constant: sideMargin
            )
            self.view.addConstraints([topOfCallout, leftOfCallout, rightOfCallout])
            
            // Add avatar over callout
            let avatarBorder = UIImage.createCircledBorder(
                outerDiameter: annotation.avatar!.size.width,
                width: 1,
                color: UIColor(cgColor: callout.layer.borderColor!)
            )
            let borderedAvatar = UIImage.drawImage(
                fgImage: avatarBorder,
                inImage: annotation.avatar!
            )
            self._avatarOverCallout = UIImageView(image: borderedAvatar)
            self._avatarOverCallout!.translatesAutoresizingMaskIntoConstraints = false
            
            self.view.addSubview(self._avatarOverCallout!)
            
            let topOfAvatar = NSLayoutConstraint(
                item: self._avatarOverCallout!,
                attribute: NSLayoutAttribute.top,
                relatedBy: NSLayoutRelation.equal,
                toItem: self.view,
                attribute: NSLayoutAttribute.top,
                multiplier: 1,
                constant: annotationPositionOnScreen.y - AVATAR_SIZE / 2
            )
            let leftOfAvatar = NSLayoutConstraint(
                item: self._avatarOverCallout!,
                attribute: NSLayoutAttribute.leading,
                relatedBy: NSLayoutRelation.equal,
                toItem: self.view,
                attribute: NSLayoutAttribute.leading,
                multiplier: 1,
                constant: annotationPositionOnScreen.x - AVATAR_SIZE / 2
            )
            self.view.addConstraints([topOfAvatar, leftOfAvatar])
            
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            
            annotation.calloutView = callout
            
            self._annotationRectForTap = CGRect(origin: CGPoint(x: annotationPositionOnScreen.x - AVATAR_SIZE / 2, y: annotationPositionOnScreen.y - AVATAR_SIZE / 2), size: CGSize(width: AVATAR_SIZE, height: AVATAR_SIZE))
            self._annotationForTap = annotation
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.mapTapDispatcher(_:)))
            tapGestureRecognizer.numberOfTapsRequired = 1
            self.mapView.addGestureRecognizer(tapGestureRecognizer)
            annotation.tapGestureRecognizer = tapGestureRecognizer
        }
    }
    
    func mapView(_ mapView: MGLMapView, didDeselect annotation: MGLAnnotation) {
        (annotation as? JAMPointAnnotation)?.calloutView?.removeFromSuperview()
        self._avatarOverCallout?.removeFromSuperview()

        if let tapGestureRecognizer = self._annotationForTap?.tapGestureRecognizer {
            self.mapView.removeGestureRecognizer(tapGestureRecognizer)
        }
        self._annotationForTap = nil
        self._annotationRectForTap = nil
        self._avatarOverCallout = nil
    }
    
    @objc func mapTapDispatcher(_ sender: UITapGestureRecognizer) {
        if self._annotationRectForTap != nil {
            let tapLocation = sender.location(in: self.view)
            if self._annotationRectForTap!.contains(tapLocation) {
                self.annotationTappedAgain();
            } else {
                self.tapOutsideMap();
            }
        }
    }

    func annotationTappedAgain() {
        self.calloutViewTapped(self._annotationForTap!.calloutView!)
    }
    
    func tapOutsideMap() {
        self.mapView.deselectAnnotation(self._annotationForTap!, animated: true);
    }
    
    // MARK: - MGCalloutViewDelegate
    
    @objc func calloutViewTapped(_ calloutView: JAMMapAnnotationAdvertisementView) {
        if let udid = (calloutView.representedObject as? JAMPointAnnotation)?.udid {
            self.performSegue(withIdentifier: "advertisement", sender: udid)
            self.mapView.deselectAnnotation(calloutView.representedObject, animated: true)
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? JAMAdverisementViewController {
            vc.udid = sender! as! String
            vc.mapVC = self
            if (self._requestedExplicitlyEditingOfOwnAdvertisement) {
                vc.isExplicitlyEditing = true
                self._requestedExplicitlyEditingOfOwnAdvertisement = false
            }
        }

        if let vc = segue.destination as? JAMConversationsTableViewController {
            vc.mapVC = self
        }
        
        if let vc = segue.destination as? JAMPingViewController {
            vc.mapVC = self
        }
    }
    
    // MARK: - Messages Listener
    
    func registerForKeepingAlive() {
        self.registerForKeepingAlive(frequency: self._keepAliveTimerInterval)
    }
    
    func registerForKeepingAlive(frequency: TimeInterval) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(JAMMapViewController.unregisterOffKeepingAlive), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)

        self._keepAliveTimerInterval = frequency
        self._keepAliveTimer = Timer.scheduledTimer(timeInterval: frequency, target: self, selector: #selector(keepAlive_selector), userInfo: nil, repeats: false)
    }
    
    @objc func keepAlive_selector() {
        self.keepAliveAndObtainNewMessages() {
            NotificationCenter.default.post(name: NotificationKeys.MESSAGES_UPDATED_NOTIFICATION_KEY.notification, object: nil)
        }
    }

    func keepAliveAndObtainNewMessages(_ completionHandler: (() -> Void)? = nil) {
        self.unregisterOffKeepingAlive()
        
        let lastTimestampOfMessages: Date
        let storedMessages = self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(nil, sortedBy: "sentAt", inAscendingOrder: false) as [Message]
        if let newestStoredMessage = storedMessages.first {
            lastTimestampOfMessages = newestStoredMessage.sentAt!
        } else {
            lastTimestampOfMessages = Date(timeIntervalSince1970: 0)
        }
        
        var url = "/alive/"
        let timestamp = Int(lastTimestampOfMessages.timeIntervalSince1970)
        url += String(format: "%d", arguments: [timestamp])
        url += "?access-token=" + self.defaults.udid!
        
        JAMAppDelegate.sharedSessionManager.request(
            self.baseUri + url,
            method: .put
        ).validate().responseJSON { (response: DataResponse<Any>) in
            switch response.result {
            case .success(let dict as [String: AnyObject]):
                if let messages = dict["messages"] as? [[String: AnyObject]] {
                    print(messages)
                    let newMessages = JAMMessage.arrayOfMessages(messages)!
                    
                    let afterProcessingMessages = { () -> Void in
                        self.updateNumberOfUnreadMessages()
                        completionHandler?()
                        
                        self.registerForKeepingAlive()
                    }
                    
                    if (newMessages.count > 0) {
                        var numProcessedMessages: Int = 0
                        
                        for message in newMessages {
                            let messageToStore = self.delegate.coreDataDispatcher.newObject() as Message
                            var correspondingUDID: String
                            
                            if (message.toUserUDID == self.defaults.udid!) {
                                messageToStore.direction = Message.Direction.incoming
                                correspondingUDID = message.fromUserUDID
                                messageToStore.isUnread = true
                            } else {
                                messageToStore.direction = Message.Direction.outgoing
                                correspondingUDID = message.toUserUDID
                            }
                            messageToStore.text = message.text
                            messageToStore.isShort = false
                            messageToStore.sentAt = message.sentAt
                            
                            messageToStore.linkToAdvertisementUDID(
                                correspondingUDID,
                                scale: self.delegate.scale,
                                circleImage: { (image: UIImage) -> UIImage in
                                    return image.circleImage()
                            },
                                coreDataDispatcher: self.delegate.coreDataDispatcher,
                                baseUri: self.baseUri,
                                selfUDID: self.defaults.udid!,
                                completionHandler: { (advertisement, message) -> Void in
                                    numProcessedMessages += 1
                                    if (numProcessedMessages >= newMessages.count) {
                                        self.delegate.coreDataDispatcher.commit()
                                        
                                        afterProcessingMessages()
                                    }
                            },
                                errorHandler: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                                    numProcessedMessages += 1
                                    if (numProcessedMessages >= newMessages.count) {
                                        self.delegate.coreDataDispatcher.commit()
                                        
                                        afterProcessingMessages()
                                    }
                            }
                            )
                        }
                    } else {
                        afterProcessingMessages()
                    }
                } else {
                    completionHandler?()
                    self.registerForKeepingAlive()
                }
            case .success:
                completionHandler?()
                self.registerForKeepingAlive()
            case .failure:
                completionHandler?()
                self.registerForKeepingAlive()
            }
        }

        /*self.networker.request(
            url,
            httpMethod: "PUT",
            data: nil,
            onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                if (body == nil) {
                    completionHandler?()
                    self.registerForKeepingAlive()
                    return
                }
                
                let bodyStr = String(data: body!, encoding: String.Encoding.utf8)
                if (bodyStr != nil) {
                    print(bodyStr!)
                }
                
                if let dict = try! JSONSerialization.jsonObject(with: body!, options: []) as? [String: AnyObject]
                {
                    if let messages = dict["messages"] as? [[String: AnyObject]] {
                        print(messages)
                        let newMessages = JAMMessage.arrayOfMessages(messages)!
                        
                        let afterProcessingMessages = { () -> Void in
                            self.updateNumberOfUnreadMessages()
                            completionHandler?()
                            
                            self.registerForKeepingAlive()
                        }
                        
                        if (newMessages.count > 0) {
                            var numProcessedMessages: Int = 0
                            
                            for message in newMessages {
                                let messageToStore = self.delegate.coreDataDispatcher.newObject() as Message
                                var correspondingUDID: String
                                
                                if (message.toUserUDID == self.defaults.udid!) {
                                    messageToStore.direction = Message.Direction.incoming
                                    correspondingUDID = message.fromUserUDID
                                    messageToStore.isUnread = true
                                } else {
                                    messageToStore.direction = Message.Direction.outgoing
                                    correspondingUDID = message.toUserUDID
                                }
                                messageToStore.text = message.text
                                messageToStore.isShort = false
                                messageToStore.sentAt = message.sentAt
                                
                                messageToStore.linkToAdvertisementUDID(
                                    correspondingUDID,
                                    scale: self.delegate.scale,
                                    circleImage: { (image: UIImage) -> UIImage in
                                        return image.circleImage()
                                    },
                                    coreDataDispatcher: self.delegate.coreDataDispatcher,
                                    networker: self.networker,
                                    selfUDID: self.defaults.udid!,
                                    completionHandler: { (advertisement, message) -> Void in
                                        numProcessedMessages += 1
                                        if (numProcessedMessages >= newMessages.count) {
                                            self.delegate.coreDataDispatcher.commit()
                                            
                                            afterProcessingMessages()
                                        }
                                    },
                                    errorHandler: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                                        numProcessedMessages += 1
                                        if (numProcessedMessages >= newMessages.count) {
                                            self.delegate.coreDataDispatcher.commit()
                                            
                                            afterProcessingMessages()
                                        }
                                    }
                                )
                            }
                        } else {
                            afterProcessingMessages()
                        }
                    } else {
                        if let body = body {
                            if let body = String(data: body, encoding: String.Encoding.utf8) {
                                print(body)
                            }
                        }
                        
                        completionHandler?()
                        self.registerForKeepingAlive()
                    }
                } else {
                    if let body = body {
                        if let body = String(data: body, encoding: String.Encoding.utf8) {
                            print(body)
                        }
                    }
                    
                    completionHandler?()
                    self.registerForKeepingAlive()
                }
            },
            onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                if let response = response {
                    print(response)
                }
                
                if let body = body {
                    if let body = String(data: body, encoding: String.Encoding.utf8) {
                        print(body)
                    }
                }
                
                completionHandler?()
                self.registerForKeepingAlive()
            }
        )
        self.networker.start()*/
    }
    
    func unregisterOffKeepingAlive() {
        self._keepAliveTimer?.invalidate()
        self._keepAliveTimer = nil
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(JAMMapViewController.registerForKeepingAlive as (JAMMapViewController) -> () -> ()), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    func updateNumberOfUnreadMessages() {
        let storedMessagesUnreadCount = self.delegate.coreDataDispatcher.countObjects("Message", predicate: NSPredicate(format: "(isUnread = YES)", argumentArray: nil))
        if (storedMessagesUnreadCount > 0) {
            __UI_THREAD_TASK {
                self.tbiMessages.badgeValue = String(format: "%d", arguments: [storedMessagesUnreadCount])
            }
        } else {
            __UI_THREAD_TASK {
                self.tbiMessages.badgeValue = nil
            }
        }
    }
    
    
    // MARK: - Advertisements Listener
    
    func registerForUpdatingAdvertisements() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(JAMMapViewController.unregisterOffUpdatingAdvertisements), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        self._refreshingAdvertisementsTimer = Timer.scheduledTimer(timeInterval: 600, target: self, selector: #selector(JAMMapViewController.keepAlive_selector), userInfo: nil, repeats: false)
    }
    
    @objc func refreshAdvertisements_selector() {
        self.unregisterOffUpdatingAdvertisements()
        
        self.refreshAdvertisements {
            self.registerForUpdatingAdvertisements()
        }
    }
    
    func unregisterOffUpdatingAdvertisements() {
        self._refreshingAdvertisementsTimer?.invalidate()
        self._refreshingAdvertisementsTimer = nil
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(JAMMapViewController.registerForUpdatingAdvertisements), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
}

