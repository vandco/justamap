//
//  JAMRequestHandler.swift
//  JustAMap
//
//  Created by Artur on 26/01/2017.
//  Copyright © 2017 V&Co Ltd. All rights reserved.
//

import Foundation
import Alamofire

typealias AuthFailedHandler = (@escaping RequestRetryCompletion, Request) -> Void

class JAMRequestHandler: RequestRetrier {
    private let lock = NSLock()
    private var requestsToRetry: [RequestRetryCompletion] = []
    
    public var numOfRetries: UInt
    public var onAuthFailure: AuthFailedHandler?
    
    public init(numOfRetries: UInt) {
        self.numOfRetries = numOfRetries
    }
    
    // MARK: - RequestRetrier
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock(); defer { lock.unlock() }
        
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            print("RequestRetrier: auth failed")
            if self.onAuthFailure != nil {
                print("RequestRetrier: calling onAuthFailure")
                __ASYNC_BLOCK {
                    self.onAuthFailure!(completion, request)
                }
                return
            }
            print("RequestRetrier: no onAuthFailure set")
        }
        
        print("RequestRetrier: retried so far: \(request.retryCount) of \(self.numOfRetries)")
        if request.retryCount >= self.numOfRetries {
            print("RequestRetrier: stop retrying")
            completion(false, 0)
        } else {
            print("RequestRetrier: retrying")
            completion(true, 0)
        }
    }
}
