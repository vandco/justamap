//
//  JAMAdverisementViewController.swift
//  JustAMap
//
//  Created by Artur on 28/07/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit
import CoreLocation
import Photos
import AVFoundation
import Alamofire

let PI = M_PI

class JAMAdverisementViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, JAMNavigationControllerBackButtonDelegate {
    
    var udid: String!
    var brandNew: Bool!
    var photoMicro: Dictionary<String, UIImage> = Dictionary<String, UIImage>()
    var photoMini: Dictionary<String, UIImage> = Dictionary<String, UIImage>()
    var photoFull: UIImage?
    
    var mapVC: JAMMapViewController!
    var isExplicitlyEditing: Bool = false
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var imgCircleMask: UIImageView!
    @IBOutlet weak var btnPickPhoto: JAMRoundFramedButton!
    @IBOutlet weak var cnsZeroHeightBtnPickPhoto: NSLayoutConstraint!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var cnsZeroHeightLblDescription: NSLayoutConstraint!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var cnsHeightTxtDescription: NSLayoutConstraint!
    
    @IBOutlet weak var menuPublishActions: JAMActionsMenuView!
    @IBOutlet weak var menuMessageActions: JAMActionsMenuView!
    @IBOutlet weak var menuComplaintActions: JAMActionsMenuView!
    @IBOutlet weak var menuEditActions: JAMActionsMenuView!
    @IBOutlet weak var menuDeleteActions: JAMActionsMenuView!
    
    @IBOutlet weak var cnsHeightMenuPublishActions: NSLayoutConstraint!
    @IBOutlet weak var cnsZeroHeightMenuPublishActions: NSLayoutConstraint!
    @IBOutlet weak var cnsBottomMenuPublishActions: NSLayoutConstraint!
    
    @IBOutlet weak var cnsHeightMenuMessageActions: NSLayoutConstraint!
    @IBOutlet weak var cnsZeroHeightMenuMessageActions: NSLayoutConstraint!
    
    @IBOutlet weak var cnsHeightMenuComplaintActions: NSLayoutConstraint!
    @IBOutlet weak var cnsZeroHeightMenuComplaintActions: NSLayoutConstraint!
    @IBOutlet weak var cnsBottomMenuComplaintActions: NSLayoutConstraint!
    
    @IBOutlet weak var cnsHeightMenuEditActions: NSLayoutConstraint!
    @IBOutlet weak var cnsZeroHeightMenuEditActions: NSLayoutConstraint!
    
    @IBOutlet weak var cnsHeightMenuDeleteActions: NSLayoutConstraint!
    @IBOutlet weak var cnsZeroHeightMenuDeleteActions: NSLayoutConstraint!
    @IBOutlet weak var cnsBottomMenuDeleteActions: NSLayoutConstraint!
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var lblLoadingStatus: UILabel!
    @IBOutlet weak var pbLoading: UIProgressView!
    
    fileprivate var _pendingForGalleryAuth: Bool = false
    fileprivate var _pendingForCameraAuth: Bool = false

    lazy var delegate: JAMAppDelegate = { return (UIApplication.shared.delegate as! JAMAppDelegate) }()
    lazy var defaults: UserDefaults = { return self.delegate.defaults }()
    
    //var networker: JAMNetworking!
    var baseUri: String!
    var queue: JAMQueue!
    
    var kbHeight: CGFloat = 0
    var adjustedForKeyboard: Bool = false
    var scrollViewDefaultInsets: UIEdgeInsets?
    
    var isChanged: Bool = false

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.default
        self.navigationItem.titleView = UIView(frame: CGRect.zero)
        self.navigationController?.navigationBar.tintColor = FOREGROUND_BUTTON_COLOR
        
        if self.scrollViewDefaultInsets != nil {
        } else {
            self.scrollViewDefaultInsets = self.scrollView.contentInset
        }

        //self.fixHeightOfDescriptionTextView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //self.networker = JAMNetworking(baseUri: self.mapVC.networker.baseUri)
        self.baseUri = self.mapVC.baseUri
        
        self.imgCircleMask.image = UIImage.createCircleMask(diameter: self.imgCircleMask.frame.size.width, fillOutsideColor: UIColor.white)
        
        self.txtDescription.layer.cornerRadius = 7
        self.txtDescription.layer.borderColor = FOREGROUND_BUTTON_COLOR.cgColor
        
        self.loadAdvertisement()
        
        self.registerForKeyboardNotifications()
    }
    
    // MARK: - Loading View helpers
    
    func showLoading(animated: Bool, completionHandler: @escaping () -> Void) -> Void {
        __UI_THREAD_TASK { () -> Void in
            let _showLoading = { () -> Void in
                self.loadingView.alpha = 1
                
                self.imgPhoto.alpha = 0
                self.btnPickPhoto.alpha = 0
                self.lblDescription.alpha = 0
                self.txtDescription.alpha = 0
                
                self.menuPublishActions.alpha = 0
                
                self.menuMessageActions.alpha = 0
                self.menuComplaintActions.alpha = 0
                
                self.menuEditActions.alpha = 0
                self.menuDeleteActions.alpha = 0
            }
            
            if (animated) {
                UIView.animate(withDuration: 0.5, animations: _showLoading, completion: { (_) -> Void in
                    completionHandler()
                })
            } else {
                _showLoading()
            }
        }
    }
    
    func showLoading(animated: Bool) -> Void {
        self.showLoading(animated: animated) {}
    }
    
    func hideLoading(animated: Bool, completionHandler: @escaping () -> Void) -> Void {
        __UI_THREAD_TASK { () -> Void in
            let _hideLoading = { () -> Void in
                self.loadingView.alpha = 0
                
                self.imgPhoto.alpha = 1
                self.btnPickPhoto.alpha = 1
                self.lblDescription.alpha = 1
                self.txtDescription.alpha = 1
                
                self.menuPublishActions.alpha = 1
                
                self.menuMessageActions.alpha = 1
                self.menuComplaintActions.alpha = 1
                
                self.menuEditActions.alpha = 1
                self.menuDeleteActions.alpha = 1
            }
            
            if (animated) {
                UIView.animate(withDuration: 0.5, animations: _hideLoading, completion: { (_) -> Void in
                    completionHandler()
                })
            } else {
                _hideLoading()
            }
        }
    }
    
    func hideLoading(animated: Bool) -> Void {
        self.hideLoading(animated: animated) {}
    }
    
    // MARK: - Setup UI
    
    func setupForUDID() {
        __UI_THREAD_TASK {
            self.disableEdit()
            
            if (self.udid == self.defaults.udid!) {
                if (self.isExplicitlyEditing) {
                    self.setupAsEditableOwn()
                } else {
                    self.setupAsNotEditableOwn()
                }
            } else {
                self.setupAsAlien()
            }
            
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    func disableEdit() {
        // Hide 'Choose Photo' button
        self.btnPickPhoto.isHidden = true
        self.cnsZeroHeightBtnPickPhoto.priority = 999
        self.cnsZeroHeightLblDescription.priority = 999
        
        // Disable text view
        self.txtDescription.isEditable = false
        self.txtDescription.layer.borderWidth = 0
        
        // Hide 'Publish' button
        self.menuPublishActions.isHidden = true
        //self.cnsHeightMenuPublishActions.priority = 1
        //self.cnsZeroHeightMenuPublishActions.priority = 999
        self.cnsBottomMenuPublishActions.priority = 1
    }
    
    func enableEdit() {
        // Show 'Choose Photo' button
        self.btnPickPhoto.isHidden = false
        self.setTitleForPickPhotoButton()
        self.cnsZeroHeightBtnPickPhoto.priority = 1
        self.cnsZeroHeightLblDescription.priority = 1
        
        // Enable text view
        self.txtDescription.isEditable = true
        self.txtDescription.layer.borderWidth = 1
        
        // Show 'Publish' button
        self.menuPublishActions.isHidden = false
        //self.cnsHeightMenuPublishActions.priority = 998
        //self.cnsZeroHeightMenuPublishActions.priority = 1
        self.cnsBottomMenuPublishActions.priority = 999
    }
    
    func hideEditAndDeleteActions() {
        self.menuEditActions.isHidden = true
        //self.cnsHeightMenuEditActions.priority = 1
        //self.cnsZeroHeightMenuEditActions.priority = 999
        
        self.menuDeleteActions.isHidden = true
        //self.cnsHeightMenuDeleteActions.priority = 1
        //self.cnsZeroHeightMenuDeleteActions.priority = 999
        self.cnsBottomMenuDeleteActions.priority = 1
    }
    
    func showEditAndDeleteActions() {
        self.menuEditActions.isHidden = false
        //self.cnsHeightMenuEditActions.priority = 999
        //self.cnsZeroHeightMenuEditActions.priority = 1
        
        self.menuDeleteActions.isHidden = false
        //self.cnsHeightMenuDeleteActions.priority = 999
        //self.cnsZeroHeightMenuDeleteActions.priority = 1
        self.cnsBottomMenuDeleteActions.priority = 999
    }
    
    func hideMessagesAndComplaintActions() {
        self.menuMessageActions.isHidden = true
        //self.cnsHeightMenuMessageActions.priority = 1
        //self.cnsZeroHeightMenuMessageActions.priority = 999
        
        self.menuComplaintActions.isHidden = true
        //self.cnsHeightMenuComplaintActions.priority = 1
        //self.cnsZeroHeightMenuComplaintActions.priority = 999
        self.cnsBottomMenuComplaintActions.priority = 1
    }
    
    func showMessagesAndComplaintActions() {
        self.menuMessageActions.isHidden = false
        //self.cnsHeightMenuMessageActions.priority = 999
        //self.cnsZeroHeightMenuMessageActions.priority = 1
        
        self.menuComplaintActions.isHidden = false
        //self.cnsHeightMenuComplaintActions.priority = 999
        //self.cnsZeroHeightMenuComplaintActions.priority = 1
        self.cnsBottomMenuComplaintActions.priority = 999
    }
    
    func setupAsAlien() {
        self.disableEdit()

        // Hide 'Edit' & 'Delete' buttons
        self.hideEditAndDeleteActions()

        // Show 'Messages' & 'Complaint' buttons
        self.showMessagesAndComplaintActions()
    }
    
    func setupAsNotEditableOwn() {
        self.disableEdit()

        // Hide 'Messages' & 'Complaint' buttons
        self.hideMessagesAndComplaintActions()

        // Show 'Edit' & 'Delete' buttons
        self.showEditAndDeleteActions()

        let editButton = UIBarButtonItem(
            title: "Изменить",
            style: UIBarButtonItemStyle.done,
            target: self,
            action: #selector(JAMAdverisementViewController.editOwn))
        editButton.setTitleTextAttributes([NSForegroundColorAttributeName: FOREGROUND_DISABLED_BUTTON_COLOR], for: UIControlState.disabled)
        self.navigationItem.rightBarButtonItem = editButton
    }
    
    func setupAsEditableOwn() {
        // Hide 'Messages' & 'Complaint' buttons
        self.hideMessagesAndComplaintActions()
        
        // Hide 'Edit' & 'Delete' buttons
        self.hideEditAndDeleteActions()

        self.enableEdit()

        // Configure additional params
        self.isChanged = false
        
        let publishButton = UIBarButtonItem(
            title: "Разместить",
            style: UIBarButtonItemStyle.done,
            target: self,
            action: #selector(JAMAdverisementViewController.publish))
        publishButton.setTitleTextAttributes([NSForegroundColorAttributeName: FOREGROUND_DISABLED_BUTTON_COLOR], for: UIControlState.disabled)
        self.navigationItem.rightBarButtonItem = publishButton
    }
    
    // MARK: - Load advertisement
    
    @objc func refresh() {
        self.loadAdvertisement(force: true)
    }
    
    func loadAdvertisement() {
        self.loadAdvertisement(force: false)
    }
    
    func loadAdvertisement(force: Bool) {
        self.navigationItem.rightBarButtonItem = nil
        
        self.lblLoadingStatus.text = "Загрузка объявления..."
        
        let alert = { (message: String) -> Void in
            let alertController = UIAlertController(
                title: "Загрузка объявления",
                message: message,
                preferredStyle: UIAlertControllerStyle.alert
            )
            let retry = UIAlertAction(
                title: "Повтор",
                style: UIAlertActionStyle.default,
                handler: { (action: UIAlertAction!) -> Void in
                    __UI_THREAD_TASK {
                        self.loadAdvertisement()
                    }
                }
            )
            let cancel = UIAlertAction(
                title: "Отмена",
                style: UIAlertActionStyle.cancel,
                handler:  { (action: UIAlertAction!) -> Void in
                    __UI_THREAD_TASK {
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                }
                
            )
            alertController.addAction(retry)
            alertController.addAction(cancel)
            __UI_THREAD_TASK {
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        var advertisement: Advertisement!
        
        var needsToDownload: Bool = false
        
        let completeLoading = { () -> Void in
            //self.networker.successHandler = nil
            //self.networker.errorHandler = nil
            self.queue.reset()
            
            if (self.brandNew!) {
                self.delegate.coreDataDispatcher.deleteObject(advertisement)
            }

            self.setupForUDID()
            
            self.delegate.coreDataDispatcher.commit()
            
            if needsToDownload {
                self.hideLoading(animated: true)
            }
        }
        
        self.queue = JAMQueue(onComplete: completeLoading)
        
        let loadAdvertisementText = { () -> Void in
            self.queue.enqueueAsync {
                JAMAppDelegate.sharedSessionManager.request(self.baseUri + "/advertisements/" + self.udid + "?fields=&expand=description&access-token=" + self.defaults.udid!)
                .validate().responseJSON { (response: DataResponse<Any>) in
                    switch response.result {
                    case .success(let advertisementDict as [String: Any]):
                        self.brandNew = false
                        self.txtDescription.text = advertisementDict["description"] as! String
                        advertisement.desc = self.txtDescription.text
                    case .success:
                        alert("Неожиданный ответ сервера\n\n\(response.data!)")
                    case .failure(let error):
                        if !(error is Alamofire.AFError) {
                            alert("Проверьте подключение к интернету")
                        } else if (self.udid != self.defaults.udid!) {
                            /*if response.response?.statusCode == 404 {
                             _alert("Объявление не найдено")
                             } else {
                             alert("Неполадки на сервере")
                             }*/
                            
                            alert("Объявление не найдено")
                        } else {
                            self.txtDescription.text = nil
                            self.photoMini.removeAll(keepingCapacity: true)
                            self.imgPhoto.image = nil
                            self.setTitleForPickPhotoButton()
                            self.brandNew = true
                            //completeLoading()
                            //return
                        }
                    }
                    
                    self.queue.nextTask()
                }
            }
            
            /*self.networker.request(
                "/advertisements/" + self.udid + "?fields=&expand=description&access-token=" + self.defaults.udid!,
                httpMethod: "GET",
                data: nil,
                onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                    if let advertisementDict = try! JSONSerialization.jsonObject(with: body!, options: []) as? [String: AnyObject] {
                        self.brandNew = false
                        self.txtDescription.text = advertisementDict["description"] as! String
                        advertisement.desc = self.txtDescription.text
                    } else {
                        alert("Неожиданный ответ сервера")
                    }
                },
                onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                    if (error !== nil) {
                        alert("Проверьте подключение к интернету")
                    } else if (self.udid != self.defaults.udid!) {
                        alert("Объявление не найдено")
                    } else {
                        self.txtDescription.text = nil
                        self.photoMini.removeAll(keepingCapacity: true)
                        self.imgPhoto.image = nil
                        self.setTitleForPickPhotoButton()
                        self.brandNew = true
                        completeLoading()
                    }
                }
            )*/
        }
        
        let loadPhotoMiniAsyncTaskResponseHandler = { (response: DataResponse<Data>) in
            switch response.result {
            case .success(let body):
                let photoMini = UIImage(data: body, scale: UIScreen.main.scale)
                if (photoMini === nil) {
                    alert("Не удалось загрузить миниатюру")
                } else {
                    self.photoMini[self.delegate.scale] = photoMini
                    self.imgPhoto.image = self.photoMini[self.delegate.scale]
                    advertisement.photoMini = body
                }
            case .failure(let error):
                if error is Alamofire.AFError {
                    if (response.response?.statusCode == 404) {
                        //completeLoading()
                        break
                    } else {
                        let statusCode = response.response!.statusCode
                        let statusText = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                        alert("Ошибка сервера\n\n\(statusCode) \(statusText)")
                    }
                } else {
                    alert("Проверьте подключение к интернету")
                }
            }
            
            self.queue.nextTask()
        }
        
        let loadPhotoMiniAsyncTask = { () -> Void in
            let url = "/advertisements/" + self.udid + "/photo/mini/x" + self.delegate.scale + "?access-token=" + self.defaults.udid!
            JAMAppDelegate.sharedSessionManager.request(self.baseUri + url)
            .validate().responseData(completionHandler: loadPhotoMiniAsyncTaskResponseHandler)
        }
        
        let loadPhotoMini = { () -> Void in
            self.queue.enqueueAsync(task: loadPhotoMiniAsyncTask)
            
            /*self.networker.download(
                "/advertisements/" + self.udid + "/photo/mini/x" + self.delegate.scale + "?access-token=" + self.defaults.udid!,
                onProgress: nil,
                onSuccess: { (location: URL?, response: URLResponse?, error: NSError?) -> Void in
                    if let body = self.networker.responseBody {
                        var photoMini = UIImage(data: body, scale: UIScreen.main.scale)
                        if (photoMini === nil) {
                            alert("Не удалось загрузить миниатюру")
                        } else {
                            self.photoMini[self.delegate.scale] = photoMini
                            self.imgPhoto.image = self.photoMini[self.delegate.scale]
                            advertisement.photoMini = body
                        }
                    } else {
                        alert("Ошибка сервера")
                    }
                },
                onError: { (_: URL?, response: URLResponse?, error: NSError?) -> Void in
                    if (response == nil) {
                        alert("Проверьте подключение к интернету")
                    } else {
                        print(response)
                        if let response = response as? HTTPURLResponse {
                            if (response.statusCode == 404) {
                                completeLoading()
                                return
                            }
                        }
                        alert("Ошибка сервера")
                    }
                },
                downloadTo: nil,
                keepInMemoryOnSuccess: true,
                keepInMemoryOnError: false
            )*/
        }
        
        if let storedAdvertisement = (self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [self.udid])) as [Advertisement]).first
        {
            self.brandNew = false
            advertisement = storedAdvertisement
        } else {
            advertisement = self.delegate.coreDataDispatcher.newObject() as Advertisement
            advertisement.udid = self.udid
        }
        
        if (force || (advertisement.desc == nil)) {
            needsToDownload = true
            loadAdvertisementText()
        } else {
            self.txtDescription.text = advertisement.desc
        }
        
        if (force || (advertisement.photoMini == nil)) {
            needsToDownload = true
            loadPhotoMini()
        } else {
            if let photoMini = UIImage(data: advertisement.photoMini! as Data, scale: UIScreen.main.scale) {
                self.photoMini[self.delegate.scale] = photoMini
                self.imgPhoto.image = self.photoMini[self.delegate.scale]
            } else {
                needsToDownload = true
                loadPhotoMini()
            }
        }
        
        if needsToDownload {
            self.showLoading(animated: false)
        } else {
            self.hideLoading(animated: false)
        }
        
        //self.networker.successHandler = completeLoading
        //self.networker.errorHandler   = nil

        //self.networker.start()
        self.queue.nextTask()
    }
    
    // MARK: - Back
    
    deinit {
        //NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        if (parent === nil) {
            //self.saveDraft()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewControllerShouldPopOnBackButton() -> Bool {
        if (!self.isChanged) {
            return true
        }
        
        let alert = UIAlertController(
            title: nil,
            message: "У вас есть несохраненные изменения",
            preferredStyle: UIAlertControllerStyle.alert
        )
        let forget = UIAlertAction(
            title: "Выйти",
            style: UIAlertActionStyle.destructive
        ) { (action: UIAlertAction!) -> Void in
            _ = self.navigationController?.popViewController(animated: true)
        }
        let edit = UIAlertAction(title: "Редактировать", style: UIAlertActionStyle.cancel, handler: nil)
        
        alert.addAction(edit)
        alert.addAction(forget)
        self.present(alert, animated: true, completion: nil)
        
        return false
    }
    
    /*func saveDraft() {
        if ((advertisementModel === nil) || (!advertisementModel!.isDraft.boolValue)) {
            advertisementModel = self.delegate.coreDataDispatcher.newObject() as Advertisement
            advertisementModel!.udid = self.defaults.udid!
            advertisementModel!.isDraft = true
        }
        advertisementModel!.desc = self.txtDescription.text
        
        if (self.imgPhoto.image !== nil) {
            advertisementModel!.photo = NSData(data: UIImagePNGRepresentation(self.imgPhoto.image!))
        } else {
            advertisementModel!.photo = NSData()
        }
        
        self.delegate.coreDataDispatcher.commit()
    }*/
    
    func setTitleForPickPhotoButton() {
        if (self.imgPhoto.image !== nil) {
            self.btnPickPhoto.setTitle("ЗАМЕНИТЬ ФОТО", for: UIControlState())
        } else {
            self.btnPickPhoto.setTitle("ВЫБРАТЬ ФОТО", for: UIControlState())
        }
    }
    
    // MARK: - Pick Photo Button
    
    @IBAction func pickPhoto(_ sender: AnyObject) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let fromGallery = UIAlertAction(title: "Из галереи", style: UIAlertActionStyle.default) { (action: UIAlertAction!) -> Void in
            self.authorizeForGallery()
        }
        let fromCamera = UIAlertAction(title: "Сделать фото", style: UIAlertActionStyle.default) { (action: UIAlertAction!) -> Void in
            self.authorizeForCamera()
        }
        let cancel = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(fromGallery)
        alert.addAction(fromCamera)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Take photo from camera
    
    func authorizeForCamera() {
        if (self._pendingForCameraAuth) {
            self._pendingForCameraAuth = false
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        }
        
        let auth = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        
        if ((auth == AVAuthorizationStatus.denied)
            || (auth == AVAuthorizationStatus.restricted))
        {
            let bundleDisplayName: String = Bundle.main.infoDictionary!["CFBundleDisplayName"] as! String
            
            let alert = UIAlertController(
                title: "Нет доступа к камере",
                message: "Пожалуйста, разрешите приложению \(bundleDisplayName) доступ к камере вашего устройства:\n\nПриложение «Настройки» → \(bundleDisplayName) → Камера",
                preferredStyle: UIAlertControllerStyle.alert
            )
            
            let settings = UIAlertAction(
                title: "Настройки",
                style: UIAlertActionStyle.default,
                handler: { (_: UIAlertAction!) -> Void in
                    self._pendingForCameraAuth = true
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    NotificationCenter.default.addObserver(self, selector: #selector(JAMAdverisementViewController.authorizeForCamera), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
                }
            )
            let cancel = UIAlertAction(
                title: "Отмена",
                style: UIAlertActionStyle.cancel,
                handler: nil
            )
            
            alert.addAction(settings)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        } else if (auth == AVAuthorizationStatus.notDetermined) {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted: Bool) -> Void in
                if (granted) {
                    self.takePhotoFromCamera()
                }
            })
        } else {
            self.takePhotoFromCamera()
        }
    }

    func takePhotoFromCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureMode.photo
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - Take photo from gallery
    
    func authorizeForGallery() {
        if (self._pendingForGalleryAuth) {
            self._pendingForGalleryAuth = false
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        }
        
        let auth = PHPhotoLibrary.authorizationStatus()
        
        if ((auth == PHAuthorizationStatus.denied)
            || (auth == PHAuthorizationStatus.restricted))
        {
            let bundleDisplayName: String = Bundle.main.infoDictionary!["CFBundleDisplayName"] as! String
            
            let alert = UIAlertController(
                title: "Нет доступа к фотогалерее",
                message: "Пожалуйста, разрешите приложению \(bundleDisplayName) доступ к вашей галерее:\n\nПриложение «Настройки» → \(bundleDisplayName) → Фотографии",
                preferredStyle: UIAlertControllerStyle.alert
            )
            
            let settings = UIAlertAction(
                title: "Настройки",
                style: UIAlertActionStyle.default,
                handler: { (_: UIAlertAction!) -> Void in
                    self._pendingForGalleryAuth = true
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                    NotificationCenter.default.addObserver(self, selector: #selector(JAMAdverisementViewController.authorizeForGallery), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
                }
            )
            let cancel = UIAlertAction(
                title: "Отмена",
                style: UIAlertActionStyle.cancel,
                handler: nil
            )
            
            alert.addAction(settings)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        } else if (auth == PHAuthorizationStatus.notDetermined) {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                if (status == PHAuthorizationStatus.authorized) {
                    self.takePhotoFromGallery()
                }
            })
        } else {
            self.takePhotoFromGallery()
        }
    }
    
    func takePhotoFromGallery() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - Publish
    
    @IBAction func publishTapped(_ sender: JAMButton) {
        self.publish()
    }
    
    @objc func publish() {
        self.txtDescription.resignFirstResponder()
        
        var message = ""
        let pickPhotoMessage = "укажите фото"
        let enterDescriptionMessage = "введите текст"
        let isPhoto = (self.imgPhoto.image !== nil)
        let isDesc  = (self.txtDescription.text != "")
        if (!isDesc && !isPhoto) {
            message = "Пожалуйста, " + pickPhotoMessage + " и " + enterDescriptionMessage
        } else if (!isPhoto) {
            message = "Пожалуйста, " + pickPhotoMessage
        } else if (!isDesc) {
            message = "Пожалуйста, " + enterDescriptionMessage
        }
        
        if (message != "") {
            let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
            let cancel = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        let postToServer = { (location: CLLocationCoordinate2D?) -> Void in
            let alert = { (message: String) -> Void in
                let alertController = UIAlertController(
                    title: "Размещение объявления",
                    message: message,
                    preferredStyle: UIAlertControllerStyle.alert
                )
                let retry = UIAlertAction(
                    title: "Повтор",
                    style: UIAlertActionStyle.default,
                    handler: { (action: UIAlertAction!) -> Void in
                        self.publish()
                    }
                )
                let cancel = UIAlertAction(
                    title: "Отмена",
                    style: UIAlertActionStyle.cancel,
                    handler: { (action: UIAlertAction!) -> Void in
                        self.hideLoading(animated: true) {
                            //self.navigationItem.rightBarButtonItem?.enabled = true
                            self.navBackEnabled = true
                        }
                    }
                    
                )
                alertController.addAction(retry)
                alertController.addAction(cancel)
                
                __UI_THREAD_TASK {
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
            self.lblLoadingStatus.text = "Размещение..."
            //self.navigationItem.rightBarButtonItem?.enabled = false
            self.navBackEnabled = false
            self.showLoading(animated: true)
            
            var advertisementDict: Dictionary<String, AnyObject> = [
                "description": self.txtDescription.text as AnyObject,
            ]
            
            if let location = location {
                advertisementDict["latitude"] = location.latitude as AnyObject?
                advertisementDict["longitude"] = location.longitude as AnyObject?
            }
            
            let urlComponent: String = (self.brandNew!) ? ("") : ("/" + self.defaults.udid!)
            //let method: String = (self.brandNew!) ? "POST" : "PUT"
            let method: Alamofire.HTTPMethod = (self.brandNew!) ? .post : .put
            
            let photosUploaded = { (fullPhotoPNGData: Data?) -> Void in
                // Update local storage
                let advertisement: Advertisement
                if let storedAdvertisement = (self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [self.udid])) as [Advertisement]).first
                {
                    advertisement = storedAdvertisement
                } else {
                    advertisement = self.delegate.coreDataDispatcher.newObject() as Advertisement
                    advertisement.udid = self.udid
                }
                advertisement.desc = self.txtDescription.text
                if (self.txtDescription.text.characters.count > 140) {
                    advertisement.descShort = self.txtDescription.text[0...140] + "\u{2026}"
                } else {
                    advertisement.descShort = self.txtDescription.text
                }
                advertisement.submittedAt = Date()
                advertisement.photoFullSize = fullPhotoPNGData
                advertisement.photoMini = UIImagePNGRepresentation(self.photoMini[self.delegate.scale]!)!
                if let location = location {
                    advertisement.latitude = location.latitude
                    advertisement.longitude = location.longitude
                }
                self.delegate.coreDataDispatcher.commit()

                // Update a pin on the map
                let newAnnotation = JAMPointAnnotation()
                newAnnotation.udid = self.defaults.udid!
                if let location = location {
                    newAnnotation.coordinate = location
                }
                newAnnotation.shortDescription = advertisement.descShort
                newAnnotation.title = newAnnotation.shortDescription
                newAnnotation.submittedAt = Date(timeIntervalSinceNow: 0)
                if let photoMicro = self.photoMicro[self.delegate.scale] {
                    newAnnotation.avatar = photoMicro.circleImage()
                }

                if let annotation = self.mapVC.annotations[self.defaults.udid!] {
                    if (location == nil) {
                        newAnnotation.coordinate = annotation.coordinate
                    }
                    
                    if (self.photoMicro[self.delegate.scale] === nil) {
                        newAnnotation.avatar = annotation.avatar
                    }
                    
                    self.mapVC.mapView.removeAnnotation(annotation)
                    self.mapVC.annotations.removeValue(forKey: self.defaults.udid!)
                }
                
                self.mapVC.annotations[newAnnotation.udid] = newAnnotation
                self.mapVC.mapView.addAnnotation(newAnnotation)

                // Fill the start on the map
                self.mapVC.hasOwnAdvertisement = true
                
                // Notify a user about success
                self.hideLoading(animated: true)
                
                let alert = UIAlertController(title: "Ваше объявление добавлено на карту", message: "Обращаем внимание, что все объявления и сообщения удаляются в 00:00 по московскому времени", preferredStyle: UIAlertControllerStyle.alert)
                let ok = UIAlertAction(
                    title: "OK",
                    style: UIAlertActionStyle.default,
                    handler:  { (action: UIAlertAction!) -> Void in
                        self.navBackEnabled = true
                        //self.navigationController?.navigationBar.tintColor = FOREGROUND_BUTTON_COLOR
                        _ = self.navigationController?.popViewController(animated: true)
                        self.mapVC.mapView.setCenter(newAnnotation.coordinate, animated: true)
                    }
                )
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            }
            
            /*let uploadPhotoError = { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                self.pbLoading.isHidden = true

                if (response != nil) {
                    print(response!)
                }
                
                if (body != nil) {
                    print(String(data: body!, encoding: String.Encoding.utf8)!)
                }
                
                if (error !== nil) {
                    alert("Проверьте подключение к интернету")
                } else {
                    alert("Неполадки на сервере")
                }
            }*/
            
            let uploadPhoto = { (photo: UIImage, kindOfSize: String, scale: String?, callback: ((_ photoPNGData: Data?) -> Void)?) -> Void in
                let photoPNGData = UIImagePNGRepresentation(photo)
                let photoJPEGData = UIImageJPEGRepresentation(photo, 0.8)
                
                print("Uploading: Kind: \(kindOfSize): " + String(format: "%d", photoJPEGData!.count) + " bytes")
                
                var url = "/advertisements/" + self.defaults.udid! + "/photo/" + kindOfSize
                if let scale = scale {
                    url += "/x" + scale
                }
                url += "?access-token=" + self.defaults.udid!
                
                self.queue.enqueueAsync {
                    JAMAppDelegate.sharedSessionManager.upload(
                        photoJPEGData!,
                        to: self.baseUri + url,
                        method: .put
                    ).uploadProgress { progress in
                        __UI_THREAD_TASK { () -> Void in
                            self.pbLoading.progress = Float(progress.fractionCompleted)
                        }
                    }.validate().responseData { (response: DataResponse<Data>) in
                        switch response.result {
                        case .success:
                            callback?(photoPNGData)
                            self.queue.nextTask()
                        case .failure(let error):
                            if error is Alamofire.AFError {
                                alert("Проверьте подключение к интернету")
                            } else {
                                let statusCode = response.response!.statusCode
                                let statusText = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                                alert("Неполадки на сервере\n\n\(statusCode) \(statusText)")
                            }
                        }
                    }
                }

                /*self.networker.upload(
                    url,
                    httpMethod: "PUT",
                    rawData: photoJPEGData,
                    onProgress: nil,
                    onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                        callback?(photoPNGData)
                    },
                    onError: uploadPhotoError
                )*/
            }
            
            let uploadPhotos = { () -> Void in
                self.pbLoading.isHidden = false
                
                self.queue.reset()
                
                uploadPhoto(self.photoMicro["1"]!, "micro", "1", nil)
                uploadPhoto(self.photoMicro["2"]!, "micro", "2", nil)
                uploadPhoto(self.photoMicro["3"]!, "micro", "3", nil)
                uploadPhoto(self.photoMini["1"]!, "mini", "1", nil)
                uploadPhoto(self.photoMini["2"]!, "mini", "2", nil)
                uploadPhoto(self.photoMini["3"]!, "mini", "3", nil)
                uploadPhoto(self.photoFull!, "full", nil, photosUploaded)
                
                /*self.networker.progressHandler = { (bytesWrittenSinceLastCall: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) -> Void in
                    __UI_THREAD_TASK { () -> Void in
                        self.pbLoading.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
                    }
                }*/
                
                let onComplete = { () -> Void in
                    self.pbLoading.isHidden = true
                    //self.networker.successHandler  = nil
                    //self.networker.errorHandler    = nil
                    //self.networker.progressHandler = nil
                    self.queue.reset()
                }

                //self.networker.successHandler = onComplete
                //self.networker.errorHandler   = onComplete
                
                //self.networker.start()
                self.queue.completionHandler = onComplete
                self.queue.nextTask()
            }
            
            JAMAppDelegate.sharedSessionManager.request(
                self.baseUri + "/advertisements" + urlComponent + "?access-token=" + self.defaults.udid!,
                method: method,
                parameters: advertisementDict,
                encoding: JSONEncoding.default
            ).validate().responseData(completionHandler: { (response: DataResponse<Data>) in
                switch response.result {
                case .success:
                    if self.photoMicro[self.delegate.scale] != nil {
                        uploadPhotos()
                    } else {
                        photosUploaded(nil)
                    }
                case .failure(let error):
                    if error is Alamofire.AFError {
                        if self.photoMicro[self.delegate.scale] != nil {
                            uploadPhotos()
                        } else {
                            let statusCode = response.response!.statusCode
                            let statusText = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                            alert("Неполадки на сервере\n\n\(statusCode) \(statusText)")
                        }
                    } else {
                        alert("Проверьте подключение к интернету")
                    }
                }
            })
            
            /*self.networker.request(
                "/advertisements" + urlComponent + "?access-token=" + self.defaults.udid!,
                httpMethod: method,
                data: advertisementDict as AnyObject?,
                onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                    if self.photoMicro[self.delegate.scale] != nil {
                        uploadPhotos()
                    } else {
                        photosUploaded(nil)
                    }
                },
                onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                    if (response != nil) {
                        print(response!)
                    }
                    
                    if (body != nil) {
                        print(String(data: body!, encoding: String.Encoding.utf8)!)
                    }
                    
                    if (error !== nil) {
                        alert("Проверьте подключение к интернету")
                    } else {
                        if self.photoMicro[self.delegate.scale] != nil {
                            uploadPhotos()
                        } else {
                            alert("Неполадки на сервере")
                        }
                    }
                }
            )
            self.networker.start()*/
        }
        
        let getGPS = { (callback: (_ location: CLLocationCoordinate2D?) -> Void) -> Void in
            let location = self.delegate.location
            
            if let location = location {
                callback(location)
            } else {
                self.hideLoading(animated: false)
                //self.navigationItem.rightBarButtonItem?.enabled = true
                self.navBackEnabled = true
                
                let gpsAuthStatus = CLLocationManager.authorizationStatus()
                switch (gpsAuthStatus) {
                case CLAuthorizationStatus.notDetermined:
                    let alert = UIAlertController(title: nil, message: "Чтобы разместить объявление, приложение должно иметь доступ к GPS", preferredStyle: UIAlertControllerStyle.alert)
                    let ok = UIAlertAction(
                        title: "OK",
                        style: UIAlertActionStyle.default,
                        handler: { (action: UIAlertAction!) -> Void in
                            self.delegate.locationManager!.requestWhenInUseAuthorization()
                        }
                    )
                    
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                    
                    return
                    
                case CLAuthorizationStatus.restricted:
                    let alert = UIAlertController(title: nil, message: "Доступ к GPS запрещен", preferredStyle: UIAlertControllerStyle.alert)
                    let cancel = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                    alert.addAction(cancel)
                    self.present(alert, animated: true, completion: nil)
                    
                    return
                    
                case CLAuthorizationStatus.denied:
                    let alert = UIAlertController(title: nil, message: "Нет доступа к координатам GPS", preferredStyle: UIAlertControllerStyle.alert)
                    let cancel = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                    let ok = UIAlertAction(
                        title: "Настройки",
                        style: UIAlertActionStyle.default,
                        handler: { (action: UIAlertAction!) -> Void in
                            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                        }
                    )
                    alert.addAction(ok)
                    alert.addAction(cancel)
                    self.present(alert, animated: true, completion: nil)
                    
                    return
                    
                default:
                    let alert = UIAlertController(title: nil, message: "Нет доступа к координатам GPS", preferredStyle: UIAlertControllerStyle.alert)
                    let cancel = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                    alert.addAction(cancel)
                    self.present(alert, animated: true, completion: nil)
                    
                    return
                }
            }
        }
        
        if (!self.brandNew) {
            let alert = UIAlertController(title: "Изменение объявления", message: "Обновить координаты GPS?", preferredStyle: UIAlertControllerStyle.alert)
            let yes = UIAlertAction(
                title: "Да",
                style: UIAlertActionStyle.default,
                handler: { (action: UIAlertAction!) -> Void in
                    getGPS(postToServer)
                }
            )
            let no = UIAlertAction(
                title: "Нет",
                style: UIAlertActionStyle.default,
                handler: { (action: UIAlertAction!) -> Void in
                    postToServer(nil)
                }
            )
            alert.addAction(yes)
            alert.addAction(no)
            self.present(alert, animated: true, completion: nil)
        } else {
            getGPS(postToServer)
        }
    }
    
    // MARK: - Navigation Back Button State
    
    var navBackEnabled: Bool {
        get { return self.navigationController!.navigationBar.isUserInteractionEnabled }
        set {
            self.navigationController!.navigationBar.isUserInteractionEnabled = newValue
            self.navigationController!.navigationBar.tintColor
                = (newValue) ? FOREGROUND_BUTTON_COLOR
                             : FOREGROUND_DISABLED_BUTTON_COLOR
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        self.isChanged = true

        self.photoFull = info[UIImagePickerControllerOriginalImage] as? UIImage
        if (self.photoFull !== nil) {
            //self.navigationItem.rightBarButtonItem?.enabled = false
            self.navBackEnabled = false
            self.lblLoadingStatus.text = "Сжатие фото..."
            self.showLoading(animated: true) {
                __ASYNC_BLOCK({ () -> Void in
                    let rotatedImage = self.photoFull!.orientImageCorrectly()
                    self.photoFull = rotatedImage
                    let croppedImage = rotatedImage.cropImageTopLeft()
                    
                    self.photoMicro["1"] = croppedImage.scaleImage(scaledToSize: CGSize(width: AVATAR_SIZE, height: AVATAR_SIZE), scale: 1)
                    self.photoMicro["2"] = croppedImage.scaleImage(scaledToSize: CGSize(width: AVATAR_SIZE, height: AVATAR_SIZE), scale: 2)
                    self.photoMicro["3"] = croppedImage.scaleImage(scaledToSize: CGSize(width: AVATAR_SIZE, height: AVATAR_SIZE), scale: 3)

                    self.photoMini["1"] = croppedImage.scaleImage(scaledToSize: self.imgPhoto.frame.size, scale: 1)
                    self.photoMini["2"] = croppedImage.scaleImage(scaledToSize: self.imgPhoto.frame.size, scale: 2)
                    self.photoMini["3"] = croppedImage.scaleImage(scaledToSize: self.imgPhoto.frame.size, scale: 3)
                    
                    if (self.photoFull!.size.width > MAX_PHOTO_FULL_SIZE.width) {
                        let height = (MAX_PHOTO_FULL_SIZE.width * self.photoFull!.size.height) / self.photoFull!.size.width
                        self.photoFull = self.photoFull!.scaleImage(scaledToSize: CGSize(width: MAX_PHOTO_FULL_SIZE.width, height: height), scale: 1)
                    }
                    
                    __UI_THREAD_TASK({ () -> Void in
                        self.imgPhoto.image = self.photoMini[self.delegate.scale]
                        self.setTitleForPickPhotoButton()
                        self.hideLoading(animated: true) {
                            //self.navigationItem.rightBarButtonItem?.enabled = true
                            self.navBackEnabled = true
                        }
                    })
                })
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Full-sized photo
    
    @IBAction func miniPhotoTapped(_ sender: UITapGestureRecognizer) {
        if self.imgPhoto.image != nil {
            self.performSegue(withIdentifier: "photoFull", sender: self.udid)
        }
    }
    
    // MARK: - Actions
    
    @IBAction func showChat() {
        if (!self.mapVC.hasOwnAdvertisement) {
            let alert = UIAlertController(title: nil, message: "Сообщения доступны после размещения объявления", preferredStyle: UIAlertControllerStyle.alert)
            let cancel = UIAlertAction(
                title: "OK",
                style: UIAlertActionStyle.cancel,
                handler: nil
            )
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        } else {
            self.performSegue(withIdentifier: "messages", sender: self.udid)
        }
    }
    
    @IBAction func complain() {
        let alert = UIAlertController(title: "Отправить жалобу?", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        let yes = UIAlertAction(
            title: "Да",
            style: UIAlertActionStyle.destructive,
            handler: { (action: UIAlertAction!) in
                self._complain()
            }
        )
        let no = UIAlertAction(
            title: "Нет",
            style: UIAlertActionStyle.cancel,
            handler: nil
        )
        alert.addAction(yes)
        alert.addAction(no)
        self.present(alert, animated: true, completion: nil)
    }
    
    func _complain() {
        self.lblLoadingStatus.text = "Подача жалобы..."
        
        self.showLoading(animated: true) {
            let complaintDict: Dictionary<String, AnyObject> = [
                "advertisement_user_udid": self.udid as AnyObject,
            ]
            
            var alert: ((String) -> Void)!
            
            let _complain = { () -> Void in
                JAMAppDelegate.sharedSessionManager.request(
                    self.baseUri + "/complaints?access-token=" + self.defaults.udid!,
                    method: .post,
                    parameters: complaintDict,
                    encoding: JSONEncoding.default
                ).validate().responseData { (response: DataResponse<Data>) in
                    switch response.result {
                    case .success:
                        // Delete from the map
                        if let annotation = self.mapVC.annotations[self.udid] {
                            self.mapVC.mapView.removeAnnotation(annotation)
                            self.mapVC.annotations.removeValue(forKey: self.udid)
                        }
                        
                        // Delete from local storage
                        _ = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [self.udid])) as [Advertisement]
                        
                        _ = self.navigationController?.popViewController(animated: true)
                    case .failure(let error):
                        if error is Alamofire.AFError {
                            let statusCode = response.response!.statusCode
                            let statusText = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                            alert("Неполадки на сервере\n\n\(statusCode) \(statusText)")
                        } else {
                            alert("Проверьте подключение к интернету")
                        }
                    }
                }
                
                /*self.networker.request(
                    "/complaints?access-token=" + self.defaults.udid!,
                    httpMethod: "POST",
                    data: complaintDict as AnyObject?,
                    onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                        // Delete from the map
                        if let annotation = self.mapVC.annotations[self.udid] {
                            self.mapVC.mapView.removeAnnotation(annotation)
                            self.mapVC.annotations.removeValue(forKey: self.udid)
                        }
                        
                        // Delete from local storage
                        self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [self.udid])) as [Advertisement]
                        
                        self.navigationController?.popViewController(animated: true)
                    },
                    onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                        if (response != nil) {
                            print(response!)
                        }
                        
                        if (body != nil) {
                            print(String(data: body!, encoding: String.Encoding.utf8)!)
                        }
                        
                        if (error !== nil) {
                            alert("Проверьте подключение к интернету")
                        } else {
                            alert("Неполадки на сервере")
                        }
                    }
                )
                self.networker.start()*/
            }
            
            let _alert_workaround = { (message: String) -> Void in
                let alertController = UIAlertController(
                    title: "Подача жалобы",
                    message: message,
                    preferredStyle: UIAlertControllerStyle.alert
                )
                let retry = UIAlertAction(
                    title: "Повтор",
                    style: UIAlertActionStyle.default,
                    handler: { (action: UIAlertAction!) -> Void in
                        _complain()
                    }
                )
                let cancel = UIAlertAction(
                    title: "Отмена",
                    style: UIAlertActionStyle.cancel,
                    handler: { (action: UIAlertAction!) -> Void in
                        self.hideLoading(animated: true)
                    }
                    
                )
                alertController.addAction(retry)
                alertController.addAction(cancel)
                self.present(alertController, animated: true, completion: nil)
            }
            alert = _alert_workaround
            
            _complain()
        }
    }
    
    @IBAction func editOwn() {
        self.setupAsEditableOwn()
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    @IBAction func deleteOwn() {
        let alert = UIAlertController(
            title: "Подтвердите удаление",
            message: "Все сообщения будут удалены",
            preferredStyle: UIAlertControllerStyle.alert
        )
        let cancel = UIAlertAction(
            title: "Отмена",
            style: UIAlertActionStyle.cancel,
            handler: nil
        )
        let confirm = UIAlertAction(
            title: "Удалить",
            style: UIAlertActionStyle.destructive,
            handler: { (action: UIAlertAction!) -> Void in
                self._deleteOwn()
            }
        )
        
        alert.addAction(confirm)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func _deleteOwn() {
        self.lblLoadingStatus.text = "Удаление объявления..."
        
        self.showLoading(animated: true) {
            self.navBackEnabled = false
            
            var deleteAdvertisement: (() -> Void)!
            
            let alert = { (message: String) -> Void in
                let alertController = UIAlertController(
                    title: "Удаление объявления",
                    message: message,
                    preferredStyle: UIAlertControllerStyle.alert
                )
                let retry = UIAlertAction(
                    title: "Повтор",
                    style: UIAlertActionStyle.default,
                    handler: { (action: UIAlertAction!) -> Void in
                        deleteAdvertisement()
                    }
                )
                let cancel = UIAlertAction(
                    title: "Отмена",
                    style: UIAlertActionStyle.cancel,
                    handler: { (action: UIAlertAction!) -> Void in
                        self.navBackEnabled = true
                        self.hideLoading(animated: true)
                    }
                )
                alertController.addAction(retry)
                alertController.addAction(cancel)
                self.present(alertController, animated: true, completion: nil)
            }
            
            let _deleteAdvertisement_workaround = { () -> Void in
                JAMAppDelegate.sharedSessionManager.request(
                    self.baseUri + "/advertisements/" + self.defaults.udid! + "?access-token=" + self.defaults.udid!,
                    method: .delete
                ).validate().responseData { (response: DataResponse<Data>) in
                    switch response.result {
                    case .success:
                        // Update local storage
                        _ = self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [self.defaults.udid!])) as [Advertisement]
                        self.delegate.coreDataDispatcher.commit()
                        
                        // Delete the pin from the map
                        if let annotation = self.mapVC.annotations[self.defaults.udid!] {
                            self.mapVC.mapView.removeAnnotation(annotation)
                            self.mapVC.annotations.removeValue(forKey: self.defaults.udid!)
                        }
                        
                        // Empty the star on the map
                        self.mapVC.hasOwnAdvertisement = false
                        
                        // Pop up to the map to ensure the user will not find out himself at 'deleted' conversation
                        //let VCs = self.navigationController!.viewControllers
                        self.navigationController!.popToViewController(self.mapVC, animated: true)
                    case .failure(let error):
                        if error is Alamofire.AFError {
                            let statusCode = response.response!.statusCode
                            let statusText = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                            alert("Неполадки на сервере\n\n\(statusCode) \(statusText)")
                        } else {
                            alert("Проверьте подключение к интернету")
                        }
                    }
                }

                /*self.networker.request(
                    "/advertisements/" + self.defaults.udid! + "?access-token=" + self.defaults.udid!,
                    httpMethod: "DELETE",
                    data: nil,
                    onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                        // Update local storage
                        self.delegate.coreDataDispatcher.deleteObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [self.defaults.udid!])) as [Advertisement]
                        self.delegate.coreDataDispatcher.commit()
                        
                        // Delete the pin from the map
                        if let annotation = self.mapVC.annotations[self.defaults.udid!] {
                            self.mapVC.mapView.removeAnnotation(annotation)
                            self.mapVC.annotations.removeValue(forKey: self.defaults.udid!)
                        }
                        
                        // Empty the star on the map
                        self.mapVC.hasOwnAdvertisement = false
                        
                        // Pop up to the map to ensure the user will not find out himself at 'deleted' conversation
                        let VCs = self.navigationController!.viewControllers 
                        self.navigationController!.popToViewController(self.mapVC, animated: true)
                    },
                    onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                        if (response != nil) {
                            print(response!)
                        }
                        
                        if (body != nil) {
                            print(String(data: body!, encoding: String.Encoding.utf8)!)
                        }
                        
                        if (error !== nil) {
                            alert("Проверьте подключение к интернету")
                        } else {
                            alert("Неполадки на сервере")
                        }
                    }
                )
                self.networker.start()*/
            }
            
            deleteAdvertisement = _deleteAdvertisement_workaround
            deleteAdvertisement()
        }
    }
    
    // MARK: - Text View
    
    func textViewDidChange(_ textView: UITextView) {
        self.isChanged = true
        //self.fixHeightOfDescriptionTextView()
    }
    
    /*func fixHeightOfDescriptionTextView() {
        let contentSize = self.txtDescription.contentSize
        self.cnsHeightTxtDescription.constant = max(contentSize.height, 138)
        self.view.layoutIfNeeded()
    }*/
    
    // MARK: - Keyboard
    // http://stackoverflow.com/a/32783960/2175025
    
    @IBAction func tappedOutsideTextView(_ sender: UITapGestureRecognizer) {
        self.txtDescription.resignFirstResponder()
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(JAMAdverisementViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(JAMAdverisementViewController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(JAMAdverisementViewController.applicationWillResign(_:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UIApplicationDelegate.applicationWillEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(JAMAdverisementViewController.keyboardWillChangeHeight(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc func keyboardWillShow(_ aNotification: Notification) {
        let info = aNotification.userInfo!
        let kbSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        self.adjustForKeyboardHeight(kbSize.height)
    }
    
    @objc func keyboardWillChangeHeight(_ aNotification: Notification) {
        //self.restoreAfterKeyboard()
        self.keyboardWillShow(aNotification)
    }
    
    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
        self.restoreAfterKeyboard()
    }
    
    @objc func applicationWillResign(_ aNotification: Notification) {
        if self.txtDescription.isFirstResponder {
            self.view.endEditing(true)
            self.adjustedForKeyboard = false;
        }
    }
    
    @objc func applicationWillEnterForeground(_ aNotification: Notification) {
        if self.txtDescription.isFirstResponder {
            self.view.endEditing(true)
            self.adjustedForKeyboard = false;
        }
    }
    
    @objc func adjustForKeyboardHeight(_ height: CGFloat) {
        print("Adjusting for keyboard")
        
        let contentInsets = UIEdgeInsetsMake(
            self.scrollViewDefaultInsets!.top,
            self.scrollViewDefaultInsets!.left,
            self.scrollViewDefaultInsets!.bottom + height,
            self.scrollViewDefaultInsets!.right
        )
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        self.callSelector(#selector(self.moveToView(_:)), object: self.menuPublishActions, delay: 0.1)
        
        self.kbHeight = height
        self.adjustedForKeyboard = true
    }
    
    @objc func moveToView(_ view: UIView) {
        var rect = self.view.frame
        rect.size.height -= self.kbHeight
        if (!rect.contains(view.frame.origin)) {
            self.scrollView.scrollRectToVisible(view.frame, animated: true)
        }
    }
    
    @objc func restoreAfterKeyboard() {
        print("Restoring after keyboard")
        
        self.scrollView.contentInset = self.scrollViewDefaultInsets!
        self.scrollView.scrollIndicatorInsets = self.scrollViewDefaultInsets!
    
        self.kbHeight = 0
        self.adjustedForKeyboard = false
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? JAMPhotoViewController {
            vc.udid = sender! as! String
            vc.advertisementVC = self
            vc.photoFull = self.photoFull
        }
        
        if let vc = segue.destination as? JAMMessagesViewController {
            let udid = sender! as! String
            
            vc.avatar = self.photoMini[self.delegate.scale]!.circleImage()
            vc.udid = udid
            vc.mapVC = self.mapVC
        }
    }

}
