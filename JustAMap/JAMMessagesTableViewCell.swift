//
//  JAMMessagesTableViewCell.swift
//  JustAMap
//
//  Created by Artur on 09/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

class JAMMessagesTableViewCell: UITableViewCell {

    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var viewBox: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    
    var messagesVC: JAMMessagesViewController!
    var udid: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.viewBox.layer.cornerRadius = 7
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(JAMMessagesTableViewCell.avatarTapped))
        self.imgAvatar!.addGestureRecognizer(tapRecognizer)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func avatarTapped() {
        self.messagesVC.avatarTapped(self.udid)
    }

}
