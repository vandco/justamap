//
//  JAMSendFormView.swift
//  JustAMap
//
//  Created by Artur on 09/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit
import Alamofire

class JAMSendFormView: UIView, UITextViewDelegate {

    var tableViewController: JAMMessagesViewController?
    
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var cnsHeightTxtMessage: NSLayoutConstraint!

    @IBOutlet weak var btnSendText: JAMRoundFramedButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.txtMessage.layer.cornerRadius = 7
        self.txtMessage.layer.borderWidth = 1
        self.txtMessage.layer.borderColor = FOREGROUND_BUTTON_COLOR.cgColor
        self.fixHeightOfMessageTextView()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Text View
    
    func textViewDidChange(_ textView: UITextView) {
        //self.isChanged = true
        self.fixHeightOfMessageTextView()
    }
    
    func fixHeightOfMessageTextView() {
        let contentSize = self.txtMessage.contentSize
        self.cnsHeightTxtMessage.constant = max(contentSize.height, 37)
        self.setNeedsLayout()
        self.tableViewController?.view.layoutIfNeeded()
        //self.tableViewController?.tableView.beginUpdates()
        //self.tableViewController?.tableView.endUpdates()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return textView.text.characters.count + (text.characters.count - range.length) <= MAX_CHARACTERS_IN_MESSAGE
    }
    
    // MARK: - Buttons
        
    @IBAction func sendMessage(_ sender: JAMRoundFramedButton) {
        if (self.txtMessage.text == "") {
            if let tableViewController = self.tableViewController {
                let alertController = UIAlertController(title: nil, message: "Введите текст сообщения", preferredStyle: UIAlertControllerStyle.alert)
                let dismiss = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                alertController.addAction(dismiss)
                tableViewController.present(alertController, animated: true, completion: nil)
            }
        } else {
            if let tableViewController = self.tableViewController {
                self.btnSendText.isEnabled = false
                tableViewController.beginSending(animated: true, completion: nil)
                
                let message = JAMSentMessage(
                    text: self.txtMessage.text,
                    toUserUDID: tableViewController.udid,
                    fromUserUDID: tableViewController.defaults.udid!
                )
                message.inProcess = true

                self.txtMessage.text = ""
                self.fixHeightOfMessageTextView()
                
                tableViewController.messages.append(message)
                tableViewController.tableView.reloadData()
                
                self._sendMessage(message: message)
            }
        }
    }
    
    fileprivate func _sendMessage(message messageSending: JAMSentMessage) {
        if let tableViewController = self.tableViewController {
            let alert = { (message: String) -> Void in
                let alertController = UIAlertController(
                    title: "Отправка сообщения",
                    message: message,
                    preferredStyle: UIAlertControllerStyle.alert
                )
                let retry = UIAlertAction(
                    title: "Повтор",
                    style: UIAlertActionStyle.default,
                    handler: { (action: UIAlertAction!) -> Void in
                        self._sendMessage(message: messageSending)
                    }
                )
                let cancel = UIAlertAction(
                    title: "Отмена",
                    style: UIAlertActionStyle.cancel,
                    handler: { (action: UIAlertAction!) -> Void in
                        for i in 0..<tableViewController.messages.count {
                            let message = tableViewController.messages[i]
                            if (message.text == messageSending.text) {
                                tableViewController.messages.remove(at: i)
                                break
                            }
                        }
                        
                        tableViewController.tableView.reloadData()

                        self.btnSendText.isEnabled = true
                        tableViewController.endSending(animated: true, completion: nil)
                    }
                )
                alertController.addAction(retry)
                alertController.addAction(cancel)
                tableViewController.sendingVC.present(alertController, animated: true, completion: nil)
                print("Messages in Table View: \(tableViewController.messages)")
            }
            
            JAMAppDelegate.sharedSessionManager.request(
                tableViewController.baseUri + "/messages/" + tableViewController.udid + "?access-token=" + tableViewController.mapVC.defaults.udid!,
                method: .post,
                parameters: messageSending.dictionary,
                encoding: JSONEncoding.default
            ).validate().responseJSON { (response: DataResponse<Any>) in
                var messageFromServer: JAMSentMessage? = nil
                
                switch response.result {
                case .success(let body as [String: AnyObject]):
                    messageFromServer = JAMSentMessage(dictionary: body)
                    
                case .success:
                    break
                    
                case .failure(let error):
                    if error is Alamofire.AFError {
                        if response.response?.statusCode == 404 {
                            tableViewController.noAdvertisementView.isHidden = false
                            tableViewController.sendFormView.isHidden = true
                            tableViewController.cnsNoAdvertisementToTableView.priority = 999
                            tableViewController.cnsSendFormToTableView.priority = 1
                            
                            for i in 0..<tableViewController.messages.count {
                                let message = tableViewController.messages[i]
                                if (message.text == messageSending.text) {
                                    tableViewController.messages.remove(at: i)
                                    break
                                }
                            }
                            
                            tableViewController.tableView.reloadData()
                        } else {
                            alert("Неожиданный ответ сервера\n\n\(response.data!)")
                            return
                        }
                    } else {
                        alert("Проверьте подключение к интернету")
                        return
                    }
                }
                
                NotificationCenter.default.post(
                    name: NotificationKeys.MESSAGES_SENT_NOTIFICATION_KEY.notification,
                    object: nil,
                    userInfo: [
                        "messageSending": messageSending,
                        "messageFromServer": messageFromServer!
                    ]
                )
                
                self.btnSendText.isEnabled = true
                tableViewController.endSending(animated: true, completion: nil)
            }


            /*tableViewController.networker.request(
                "/messages/" + tableViewController.udid + "?access-token=" + tableViewController.mapVC.defaults.udid!,
                httpMethod: "POST",
                data: messageSending.dictionary as AnyObject?,
                onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                    var messageFromServer: JAMSentMessage? = nil

                    if (body != nil) {
                        print(String(data: body!, encoding: String.Encoding.utf8)!)
                        
                        if let body = try! JSONSerialization.jsonObject(with: body!, options: []) as? [String: AnyObject]
                        {
                            messageFromServer = JAMSentMessage(dictionary: body)
                        }
                    }
                    
                    NotificationCenter.default.post(
                        name: NotificationKeys.MESSAGES_SENT_NOTIFICATION_KEY.notification,
                        object: nil,
                        userInfo: [
                            "messageSending": messageSending,
                            "messageFromServer": messageFromServer!
                        ]
                    )

                    self.btnSendText.isEnabled = true
                    tableViewController.endSending(animated: true, completion: nil)
                },
                onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                    if (response === nil) {
                        alert("Проверьте подключение к интернету")
                    } else {
                        print(response!)
                        
                        if let body = body {
                            if let body = String(data: body, encoding: String.Encoding.utf8) {
                                print(body)
                            }
                        }
                        
                        var allRight: Bool = false
                        
                        if let response = response as? HTTPURLResponse {
                            if (response.statusCode == 404) {
                                allRight = true
                                
                                tableViewController.noAdvertisementView.isHidden = false
                                tableViewController.sendFormView.isHidden = true
                                tableViewController.cnsNoAdvertisementToTableView.priority = 999
                                tableViewController.cnsSendFormToTableView.priority = 1
                            }
                        }
                        
                        if (!allRight) {
                            alert("Неожиданный ответ сервера")
                        } else {
                            for i in 0..<tableViewController.messages.count {
                                let message = tableViewController.messages[i]
                                if (message.text == messageSending.text) {
                                    tableViewController.messages.remove(at: i)
                                    break
                                }
                            }
                            
                            tableViewController.tableView.reloadData()
                            
                            self.btnSendText.isEnabled = true
                            tableViewController.endSending(animated: true, completion: nil)
                        }
                    }
                }
            )
            tableViewController.networker.start()*/
        }
    }
    
    @IBAction func ignore(_ sender: JAMActionView) {
        if let tableViewController = self.tableViewController {
            let alert = UIAlertController(
                title: "Блокировать пользователя?",
                message: "Вы больше не сможете получить доступ к переписке с ним до 00:00 по московскому времени",
                preferredStyle: UIAlertControllerStyle.alert
            )
            let yes = UIAlertAction(
                title: "Да",
                style: UIAlertActionStyle.destructive,
                handler: { (action: UIAlertAction!) in
                    self._complainOrIgnore(status: "Блокировка пользователя", complaint: false)
                }
            )
            let no = UIAlertAction(
                title: "Нет",
                style: UIAlertActionStyle.cancel,
                handler: nil
            )
            alert.addAction(yes)
            alert.addAction(no)
            tableViewController.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func complain(_ sender: JAMActionView) {
        if let tableViewController = self.tableViewController {
            let alert = UIAlertController(title: "Отправить жалобу?", message: nil, preferredStyle: UIAlertControllerStyle.alert)
            let yes = UIAlertAction(
                title: "Да",
                style: UIAlertActionStyle.destructive,
                handler: { (action: UIAlertAction!) in
                    self._complainOrIgnore(status: "Подача жалобы", complaint: true)
                }
            )
            let no = UIAlertAction(
                title: "Нет",
                style: UIAlertActionStyle.cancel,
                handler: nil
            )
            alert.addAction(yes)
            alert.addAction(no)
            tableViewController.present(alert, animated: true, completion: nil)
        }
    }
    
    func _complainOrIgnore(status: String, complaint: Bool) {
        let processingVC = JAMProcessingViewController(status: status + "...")
        
        let key = (complaint) ? "advertisement_user_udid" : "to_user_udid"
        let complaintOrIgnoreDict: [String: String] = [
            key: self.tableViewController!.udid,
        ]
            
        var alert: ((String) -> Void)!
        
        let url = "/" + ((complaint) ? "complaints" : "ignores") + "?access-token=" + self.tableViewController!.mapVC.defaults.udid!
        
        let _complain = { () -> Void in
            self.tableViewController!.present(processingVC, animated: true, completion: nil)
            
            JAMAppDelegate.sharedSessionManager.request(
                self.tableViewController!.baseUri + url,
                method: .post,
                parameters: complaintOrIgnoreDict,
                encoding: JSONEncoding.default
            ).validate().responseData { (response: DataResponse<Data>) in
                switch response.result {
                case .success:
                    // Delete from the map
                    if let annotation = self.tableViewController!.mapVC.annotations[self.tableViewController!.udid] {
                        self.tableViewController!.mapVC.mapView.removeAnnotation(annotation)
                        self.tableViewController!.mapVC.annotations.removeValue(forKey: self.tableViewController!.udid)
                    }
                    
                    if let conversationsVC = self.tableViewController!.conversationsVC {
                        conversationsVC.conversations.removeValue(forKey: self.tableViewController!.udid)
                        conversationsVC.tableView.reloadData()
                        
                        conversationsVC.placeholder.isHidden = !conversationsVC.conversations.isEmpty
                    }
                    
                    _ = self.tableViewController!.navigationController?.popViewController(animated: true)
                case .failure(let error):
                    if error is Alamofire.AFError {
                        let statusCode = response.response!.statusCode
                        let statusText = HTTPURLResponse.localizedString(forStatusCode: statusCode)
                        alert("Неполадки на сервере\n\n\(statusCode) \(statusText)")
                    } else {
                        alert("Проверьте подключение к интернету")
                    }
                }
            }

            /*self.tableViewController!.networker.request(
                url,
                httpMethod: "POST",
                data: complaintOrIgnoreDict as AnyObject?,
                onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                    // Delete from the map
                    if let annotation = self.tableViewController!.mapVC.annotations[self.tableViewController!.udid] {
                        self.tableViewController!.mapVC.mapView.removeAnnotation(annotation)
                        self.tableViewController!.mapVC.annotations.removeValue(forKey: self.tableViewController!.udid)
                    }
                    
                    if let conversationsVC = self.tableViewController!.conversationsVC {
                        conversationsVC.conversations.removeValue(forKey: self.tableViewController!.udid)
                        conversationsVC.tableView.reloadData()
                        
                        conversationsVC.placeholder.isHidden = !conversationsVC.conversations.isEmpty
                    }
                    
                    self.tableViewController!.navigationController?.popViewController(animated: true)
                },
                onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                    if (response != nil) {
                        print(response!)
                    }
                    
                    if (body != nil) {
                        print(String(data: body!, encoding: String.Encoding.utf8)!)
                    }
                    
                    if (error !== nil) {
                        alert("Проверьте подключение к интернету")
                    } else {
                        alert("Неполадки на сервере")
                    }
                }
            )
            self.tableViewController!.networker.start()*/
        }
        
        let _alert_workaround = { (message: String) -> Void in
            let alertController = UIAlertController(
                title: status,
                message: message,
                preferredStyle: UIAlertControllerStyle.alert
            )
            
            let retry = UIAlertAction(
                title: "Повтор",
                style: UIAlertActionStyle.default,
                handler: { (action: UIAlertAction!) -> Void in
                    _complain()
                }
            )
            let cancel = UIAlertAction(
                title: "Отмена",
                style: UIAlertActionStyle.cancel,
                handler: { (action: UIAlertAction!) -> Void in
                    self.tableViewController!.presentedViewController?.dismiss(animated: true, completion: nil)
                }
            )
            
            alertController.addAction(retry)
            alertController.addAction(cancel)
            
            if let currentlyPresentedVC = self.tableViewController!.presentedViewController {
                currentlyPresentedVC.dismiss(
                    animated: true,
                    completion: { () -> Void in
                        self.tableViewController!.present(alertController, animated: true, completion: nil)
                    }
                )
            } else {
                self.tableViewController!.present(alertController, animated: true, completion: nil)
            }
        }
        alert = _alert_workaround
        
        _complain()
    }
}
