//
//  UIFont+Traits.swift
//  JustAMap
//
//  Created by Artur on 31/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

extension UIFont {
    func withTraits(_ traits: UIFontDescriptorSymbolicTraits) -> UIFont {
        let oldDescriptor = self.fontDescriptor
        let oldTraits = oldDescriptor.symbolicTraits
        
        let newTraits = UIFontDescriptorSymbolicTraits(rawValue: oldTraits.rawValue | traits.rawValue)
        
        let descriptor = oldDescriptor.withSymbolicTraits(newTraits)
        return UIFont(descriptor: descriptor!, size: 0)
    }
    
    func withoutTraits(_ traits: UIFontDescriptorSymbolicTraits) -> UIFont {
        let oldDescriptor = self.fontDescriptor
        let oldTraits = oldDescriptor.symbolicTraits
        
        let newTraits = UIFontDescriptorSymbolicTraits(rawValue: oldTraits.rawValue & ~(traits.rawValue))
        
        let descriptor = oldDescriptor.withSymbolicTraits(newTraits)
        return UIFont(descriptor: descriptor!, size: 0)
    }
    
    func makeBold() -> UIFont {
        return self.withTraits(UIFontDescriptorSymbolicTraits.traitBold)
    }
    
    func isBold() -> Bool {
        return (self.fontDescriptor.symbolicTraits.rawValue & UIFontDescriptorSymbolicTraits.traitBold.rawValue) != 0
    }
    
    func makeUnbold() -> UIFont {
        if (self.isBold()) {
            return self.withoutTraits(UIFontDescriptorSymbolicTraits.traitBold)
        } else {
            return self
        }
    }
    
    func makeItalic() -> UIFont {
        return self.withTraits(UIFontDescriptorSymbolicTraits.traitItalic)
    }

    func isItalic() -> Bool {
        return (self.fontDescriptor.symbolicTraits.rawValue & UIFontDescriptorSymbolicTraits.traitItalic.rawValue) != 0
    }

    func makeUnitalic() -> UIFont {
        if (self.isItalic()) {
            return self.withoutTraits(UIFontDescriptorSymbolicTraits.traitItalic)
        } else {
            return self
        }
    }
}
