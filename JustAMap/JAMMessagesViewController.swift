//
//  JAMMessagesViewController.swift
//  JustAMap
//
//  Created by Artur on 09/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit
import Alamofire

class JAMMessagesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    lazy var delegate: JAMAppDelegate = { return (UIApplication.shared.delegate as! JAMAppDelegate) }()
    lazy var defaults: UserDefaults = { return self.delegate.defaults }()
    
    @IBOutlet weak var tableView: UITableView!
    //var refreshControl: UIRefreshControl!
    var processingVC: JAMProcessingViewController!
    var sendingVC: JAMProcessingViewController!

    //var networker: JAMNetworking!
    var baseUri: String!
    
    var mapVC: JAMMapViewController!
    var conversationsVC: JAMConversationsTableViewController?
    var messages: [JAMSentMessage] = []
    var udid: String!
    var avatar: UIImage?

    var kbHeight: CGFloat = 0
    var adjustedForKeyboard: Bool = false

    @IBOutlet weak var sendFormView: JAMSendFormView!
    @IBOutlet weak var noAdvertisementView: UIView!
    
    @IBOutlet weak var cnsSendFormToTableView: NSLayoutConstraint!
    @IBOutlet weak var cnsNoAdvertisementToTableView: NSLayoutConstraint!
    
    // MARK: - Navigation Bar Enabled State
    
    var navBarEnabled: Bool {
        get { return self.navigationController!.navigationBar.isUserInteractionEnabled }
        set {
            self.navigationController!.navigationBar.isUserInteractionEnabled = newValue
            self.navigationController!.navigationBar.tintColor
                = (newValue) ? FOREGROUND_BUTTON_COLOR
                : FOREGROUND_DISABLED_BUTTON_COLOR
        }
    }
    
    // MARK: - Initialize View Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refreshButton = UIBarButtonItem(
            title: "Обновить",
            style: UIBarButtonItemStyle.plain,
            target: self,
            action: #selector(JAMMessagesViewController.refresh))
        self.navigationItem.rightBarButtonItem = refreshButton
        
        self.navBarEnabled = false

        sendFormView.tableViewController = self
        
        let hideKeyboardGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(JAMMessagesViewController.tappedOutsideTextView(_:)))
        self.view.addGestureRecognizer(hideKeyboardGestureRecognizer)
        
        self.registerForKeyboardNotifications()

        //self.networker = JAMNetworking(baseUri: self.mapVC.networker.baseUri)
        self.baseUri = self.mapVC.baseUri
        
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: CGFloat.leastNormalMagnitude)) // because zero-height is just ignored
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 48
        
        self.firstLoadMessages()
        
        self.processingVC = JAMProcessingViewController(status: "Загрузка сообщений...")

        NotificationCenter.default.addObserver(self, selector: #selector(messageSent_selector), name: NotificationKeys.MESSAGES_SENT_NOTIFICATION_KEY.notification, object: nil)

        self.sendingVC = JAMProcessingViewController(status: "Отправка сообщения...")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.default
        self.navigationItem.titleView = UIView(frame: CGRect.zero)
        
        self.mapVC.unregisterOffKeepingAlive()
        self.mapVC.registerForKeepingAlive(frequency: 5)

        self.beginRefresh(animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.mapVC.unregisterOffKeepingAlive()
        self.mapVC.registerForKeepingAlive(frequency: 30)
        
        if let currentlyPresentedVC = self.presentedViewController {
            currentlyPresentedVC.dismiss(
                animated: animated,
                completion: nil
            )
        }
        
        if (self.sendFormView.txtMessage.text.characters.count > 0) {
            let messageToStore = self.delegate.coreDataDispatcher.newObject() as Message
            
            messageToStore.direction = Message.Direction.outgoing
            messageToStore.text = self.sendFormView.txtMessage.text!
            messageToStore.isShort = false
            messageToStore.isUnread = false
            messageToStore.sentAt = nil
            
            messageToStore.linkToAdvertisementUDID(
                self.udid,
                scale: self.delegate.scale,
                circleImage: { (image: UIImage) -> UIImage in
                    return image.circleImage()
                },
                coreDataDispatcher: self.delegate.coreDataDispatcher,
                baseUri: self.baseUri,
                selfUDID: self.defaults.udid!,
                completionHandler: { (advertisement: Advertisement, message: Message) -> Void in
                    self.delegate.coreDataDispatcher.commit()
                    if let conversationsVC = self.conversationsVC {
                        conversationsVC.reloadMessagesFromLocalStorage()
                    }
                },
                errorHandler: nil
            )
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Refresh Messages
    
    func firstLoadMessages() {
        //let advertisement = (self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [self.udid])) as [Advertisement]).first!
        let storedMessages = self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(correspondentUDID = %@) AND (isShort = NO) AND (sentAt != nil)", argumentArray: [self.udid])) as [Message]
        print("Count of Stored Messages: \(storedMessages.count)")
        
        for storedMessage in storedMessages {
            let toUserUDID: String
            let fromUserUDID: String
            
            if (storedMessage.direction == Message.Direction.incoming) {
                toUserUDID = self.defaults.udid!
                fromUserUDID = self.udid
            } else {
                toUserUDID = self.udid
                fromUserUDID = self.defaults.udid!
            }
            
            let message = JAMSentMessage(
                text: storedMessage.text,
                toUserUDID: toUserUDID,
                fromUserUDID: fromUserUDID,
                sentAt: storedMessage.sentAt!
            )
            message.storedMessage = storedMessage

            self.messages.append(message)
            
            storedMessage.isUnread = false
        }
        self.delegate.coreDataDispatcher.commit()
        
        self.tableView.reloadData()
        
        let draftMessages = self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(correspondentUDID = %@) AND (isShort = NO) AND (sentAt = nil)", argumentArray: [self.udid])) as [Message]
        if let draftMessage = draftMessages.first {
            self.sendFormView.txtMessage.text = draftMessage.text
            self.sendFormView.textViewDidChange(self.sendFormView.txtMessage)
            self.delegate.coreDataDispatcher.deleteObject(draftMessage)
            self.delegate.coreDataDispatcher.commit()
            if let conversationsVC = self.conversationsVC {
                conversationsVC.reloadMessagesFromLocalStorage()
            }
        }
    }
    
    @objc func reloadMessagesFromLocalStorage() {
        self.messages.removeAll(keepingCapacity: true)
        self.firstLoadMessages()
    }
    
    @objc func refresh() {
        self.beginRefresh(animated: true)
    }
    
    func beginRefresh(animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NotificationKeys.MESSAGES_UPDATED_NOTIFICATION_KEY.notification, object: nil)
        
        self.navBarEnabled = false
        self.present(self.processingVC, animated: animated) { () -> Void in
            self.refreshMessages(self)
        }
    }
    
    func endRefresh() {
        self.scrollToBottom()
        
        self.dismiss(animated: true) { () -> Void in
            NotificationCenter.default.addObserver(self, selector: #selector(self.reloadMessagesFromLocalStorage), name: NotificationKeys.MESSAGES_UPDATED_NOTIFICATION_KEY.notification, object: nil)
            self.navBarEnabled = true
        }
    }
    
    @objc func refreshMessages(_ sender: AnyObject) {
        let alert = { (message: String) -> Void in
            let alertController = UIAlertController(
                title: "Загрузка сообщений",
                message: message,
                preferredStyle: UIAlertControllerStyle.alert
            )
            let retry = UIAlertAction(
                title: "Повтор",
                style: UIAlertActionStyle.default,
                handler: { (action: UIAlertAction!) -> Void in
                    self.refreshMessages(alertController)
                }
            )
            let cancel = UIAlertAction(
                title: "Отмена",
                style: UIAlertActionStyle.cancel,
                handler:  { (action: UIAlertAction!) -> Void in
                    self.endRefresh()
                }
                
            )
            alertController.addAction(retry)
            alertController.addAction(cancel)
            self.processingVC.present(alertController, animated: true, completion: nil)
        }
        
        let lastTimestampOfMessages: Date
        //var advertisement = (self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [self.udid])) as [Advertisement]).first!
        var storedMessages = self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(correspondentUDID = %@) AND (isShort = NO)", argumentArray: [self.udid])) as [Message]
        if let newestStoredMessage = storedMessages.last {
            lastTimestampOfMessages = newestStoredMessage.sentAt!
        } else {
            lastTimestampOfMessages = Date(timeIntervalSince1970: 0)
        }
        
        var url: String = "/messages/" + self.udid + "/afterTimestamp/"
        url += String(format: "%d", arguments: [Int(lastTimestampOfMessages.timeIntervalSince1970)])
        url += "?access-token=" + defaults.udid!
        
        JAMAppDelegate.sharedSessionManager.request(self.baseUri + url).validate().responseJSON { (response: DataResponse<Any>) in
            switch response.result {
            case .success(let messages as [[String: Any]]):
                print(messages)
                let newMessages = JAMSentMessage.arrayOfMessages(messages as [[String : AnyObject]])!
                self.messages += newMessages
                
                for message in newMessages {
                    var messageToStore: Message
                    if let foundMessage = (self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(
                        NSPredicate(
                            format: "(correspondentUDID = %@) AND (sentAt = %@)",
                            argumentArray: [self.udid, message.sentAt!])
                        ) as [Message]).first
                    {
                        messageToStore = foundMessage
                    } else {
                        messageToStore = self.delegate.coreDataDispatcher.newObject() as Message
                    }
                    
                    if (message.toUserUDID == self.defaults.udid!) {
                        messageToStore.direction = Message.Direction.incoming
                    } else {
                        messageToStore.direction = Message.Direction.outgoing
                    }
                    messageToStore.text = message.text
                    messageToStore.isShort = false
                    messageToStore.isUnread = false
                    messageToStore.sentAt = message.sentAt
                    messageToStore.correspondentUDID = self.udid
                    
                    storedMessages.append(messageToStore)
                }
                
                let storedUnreadMessages = self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(correspondentUDID = %@) AND (isUnread = YES)", argumentArray: [self.udid])) as [Message]
                for unreadMessage in storedUnreadMessages {
                    unreadMessage.isUnread = false
                }
                
                self.delegate.coreDataDispatcher.commit()
                
                self.mapVC.updateNumberOfUnreadMessages()
                self.tableView.reloadData()
                
            case .success:
                alert("Неожиданный ответ сервера\n\n\(response.data!)")
                
            case .failure(let error):
                if error is Alamofire.AFError {
                    if response.response?.statusCode == 404 {
                        self.noAdvertisementView.isHidden = false
                        self.sendFormView.isHidden = true
                        self.cnsNoAdvertisementToTableView.priority = 999
                        self.cnsSendFormToTableView.priority = 1
                    } else {
                        alert("Неожиданный ответ сервера\n\n\(response.data!)")
                    }
                } else {
                    alert("Проверьте подключение к интернету")
                }
            }
            
            self.endRefresh()
        }
        
        /*networker.request(
            url,
            httpMethod: "GET",
            data: nil,
            onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                if let messages = try! JSONSerialization.jsonObject(with: body!, options: []) as? [[String: AnyObject]] {
                    print(messages)
                    let newMessages = JAMSentMessage.arrayOfMessages(messages)!
                    self.messages += newMessages

                    for message in newMessages {
                        var messageToStore: Message
                        if let foundMessage = (self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(
                            NSPredicate(
                                format: "(correspondentUDID = %@) AND (sentAt = %@)",
                                argumentArray: [self.udid, message.sentAt!])
                            ) as [Message]).first
                        {
                            messageToStore = foundMessage
                        } else {
                            messageToStore = self.delegate.coreDataDispatcher.newObject() as Message
                        }
                        
                        if (message.toUserUDID == self.defaults.udid!) {
                            messageToStore.direction = Message.Direction.incoming
                        } else {
                            messageToStore.direction = Message.Direction.outgoing
                        }
                        messageToStore.text = message.text
                        messageToStore.isShort = false
                        messageToStore.isUnread = false
                        messageToStore.sentAt = message.sentAt
                        messageToStore.correspondentUDID = self.udid
                        
                        storedMessages.append(messageToStore)
                    }
                    
                    let storedUnreadMessages = self.delegate.coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(correspondentUDID = %@) AND (isUnread = YES)", argumentArray: [self.udid])) as [Message]
                    for unreadMessage in storedUnreadMessages {
                        unreadMessage.isUnread = false
                    }
                    
                    self.delegate.coreDataDispatcher.commit()

                    self.mapVC.updateNumberOfUnreadMessages()
                    self.tableView.reloadData()

                    self.endRefresh()
                } else {
                    if let body = body {
                        if let body = String(data: body, encoding: String.Encoding.utf8) {
                            print(body)
                        }
                    }
                    
                    alert("Неожиданный ответ сервера")
                }
            },
            onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                if (response === nil) {
                    alert("Проверьте подключение к интернету")
                } else {
                    print(response!)
                    
                    if let body = body {
                        if let body = String(data: body, encoding: String.Encoding.utf8) {
                            print(body)
                        }
                    }
                    
                    var allRight: Bool = false
                    
                    if let response = response as? HTTPURLResponse {
                        if (response.statusCode == 404) {
                            allRight = true
                            
                            self.noAdvertisementView.isHidden = false
                            self.sendFormView.isHidden = true
                            self.cnsNoAdvertisementToTableView.priority = 999
                            self.cnsSendFormToTableView.priority = 1
                        }
                    }
                    
                    if (!allRight) {
                        alert("Неожиданный ответ сервера")
                    }
                }
                
                self.endRefresh()
            }
        )
        networker.start()*/
    }
    
    // MARK: - Sending message
    
    func beginSending(animated: Bool, completion: CompletionHandler?) {
        NotificationCenter.default.removeObserver(self, name: NotificationKeys.MESSAGES_UPDATED_NOTIFICATION_KEY.notification, object: nil)
        
        self.present(self.sendingVC, animated: animated, completion: completion)
    }
    
    func endSending(animated: Bool, completion: CompletionHandler?) {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadMessagesFromLocalStorage), name: NotificationKeys.MESSAGES_UPDATED_NOTIFICATION_KEY.notification, object: nil)
        
        self.dismiss(animated: animated, completion: completion)
    }
    
    @objc func messageSent_selector(_ notification: Notification) {
        let userInfo = notification.userInfo! as! [String: JAMSentMessage]
        self.messageSent(messageSending: userInfo["messageSending"]!, messageFromServer: userInfo["messageFromServer"]!)
    }
    
    func messageSent(messageSending: JAMSentMessage, messageFromServer: JAMSentMessage) {
        if let i = self.messages.index(of: messageSending) {
            let messageFound = self.messages[i]
            
            messageFound.inProcess = false
            
            messageFound.sentAt = messageFromServer.sentAt
            
            let messageToStore = self.delegate.coreDataDispatcher.newObject() as Message
            
            messageToStore.direction = Message.Direction.outgoing
            messageToStore.text = messageFound.text
            messageToStore.isShort = false
            messageToStore.isUnread = false
            messageToStore.sentAt = messageFound.sentAt
            
            messageFound.storedMessage = messageToStore
            
            messageToStore.linkToAdvertisementUDID(
                self.udid,
                scale: self.delegate.scale,
                circleImage: { (image: UIImage) -> UIImage in
                    return image.circleImage()
                },
                coreDataDispatcher: self.delegate.coreDataDispatcher,
                baseUri: self.baseUri,
                selfUDID: self.defaults.udid!,
                completionHandler: { (advertisement: Advertisement, message: Message) -> Void in
                    self.delegate.coreDataDispatcher.commit()
                },
                errorHandler: nil
            )
            
            self.tableView.reloadData()
            self.scrollToBottom()
            
            if let conversationsVC = self.conversationsVC {
                if let rowIndex = Array(conversationsVC.conversations.keys).index(of: self.udid) {
                    conversationsVC.conversations[self.udid] = messageFound
                    
                    let indexPath = IndexPath(row: rowIndex, section: 0)
                    conversationsVC.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                }
            }
        }
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("Index Path Row: \(indexPath.row)")
        print("Messages Count: \(self.messages.count)")
        
        let message = self.messages[indexPath.row]
        var cell: JAMMessagesTableViewCell
        
        if (message.toUserUDID == self.defaults.udid!) {
            cell = tableView.dequeueReusableCell(withIdentifier: "incomingMessage", for: indexPath) as! JAMIncomingMessageTableViewCell
            cell.udid = self.udid

            cell.imgAvatar.image = self.avatar
        } else {
            var _cell: JAMOutgoingMessageTableViewCell
            _cell = tableView.dequeueReusableCell(withIdentifier: "outgoingMessage", for: indexPath) as! JAMOutgoingMessageTableViewCell
            _cell.udid = self.mapVC.defaults.udid!
            
            _cell.imgAvatar.image = self.mapVC.annotations[_cell.udid]?.avatar
            
            _cell.acvPending.isHidden = !message.inProcess
            
            cell = _cell
        }
        
        cell.messagesVC = self
        
        cell.lblMessage.text = message.text
        
        return cell
    }
    
    /*override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }*/
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    func avatarTapped(_ udid: String) {
        self.performSegue(withIdentifier: "advertisement", sender: udid)
    }
    
    // MARK: - Table view scroll to the bottom
    
    func scrollToBottom() {
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        if (self.tableView.contentSize.height > self.tableView.bounds.height) {
            self.tableView.scrollRectToVisible(CGRect(
                x: 0,
                y: self.tableView.contentSize.height - self.tableView.bounds.height,
                width: self.tableView.bounds.width,
                height: self.tableView.bounds.height
            ), animated: true)
        }
    }
    
    func isScrolledToBottom() -> Bool {
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        if (self.tableView.contentSize.height > self.tableView.bounds.height) {
            print("Content Height: \(self.tableView.contentSize.height)")
            print("Table Height: \(self.tableView.bounds.height)")
            print("Content Offset Y: \(self.tableView.contentOffset.y)")
            return (self.tableView.contentOffset.y <= self.tableView.bounds.height - self.tableView.contentSize.height)
        }
        return true
    }
    
    // MARK: - Keyboard
    
    @objc func tappedOutsideTextView(_ sender: UITapGestureRecognizer) {
        self.sendFormView.txtMessage.resignFirstResponder()
    }

    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(JAMMessagesViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(JAMMessagesViewController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(JAMMessagesViewController.applicationWillResign(_:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UIApplicationDelegate.applicationWillEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(JAMMessagesViewController.keyboardWillChangeHeight(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc func keyboardWillShow(_ aNotification: Notification) {
        let info = aNotification.userInfo!
        let kbSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        self.adjustForKeyboardHeight(kbSize.height)
    }
    
    @objc func keyboardWillChangeHeight(_ aNotification: Notification) {
        if (self.adjustedForKeyboard) {
            self.restoreAfterKeyboard()
            self.keyboardWillShow(aNotification)
        }
    }
    
    @objc func keyboardWillBeHidden(_ aNotification: Notification) {
        self.restoreAfterKeyboard()
    }
    
    @objc func applicationWillResign(_ aNotification: Notification) {
        if self.sendFormView.txtMessage.isFirstResponder {
            self.view.endEditing(true)
            self.adjustedForKeyboard = false;
        }
    }
    
    @objc func applicationWillEnterForeground(_ aNotification: Notification) {
        if self.sendFormView.txtMessage.isFirstResponder {
            self.view.endEditing(true)
            self.adjustedForKeyboard = false;
        }
    }
    
    @objc func adjustForKeyboardHeight(_ height: CGFloat) {
        if (self.adjustedForKeyboard) {
            self.restoreAfterKeyboard()
        }
        
        let neededHeight = height
        
        var frame = self.view.frame
        frame.size.height -= neededHeight
        self.view.frame = frame
        
        //self.callSelector("moveToView:", object: self.sendFormView, delay: 0.1)
        //if self.isScrolledToBottom() {
        self.callSelector(#selector(scrollToBottom), object: nil, delay: 0.1)
        //}
        
        self.kbHeight = neededHeight
        self.adjustedForKeyboard = true
    }
    
    @objc func moveToView(_ view: UIView) {
        let viewFrameInTableView = view.convert(view.bounds, to: self.tableView)
        if (viewFrameInTableView.size.height > self.tableView.bounds.height - 16) {
            self.tableView.setContentOffset(CGPoint(x: 0, y: viewFrameInTableView.origin.y + 8), animated: true)
        } else {
            let y = viewFrameInTableView.origin.y + viewFrameInTableView.size.height - self.tableView.bounds.size.height + 8
            if (y > 0) {
                self.tableView.setContentOffset(CGPoint(x: 0, y: y), animated: true)
            }
        }
    }
    
    @objc func restoreAfterKeyboard() {
        var frame = self.view.frame
        frame.size.height += self.kbHeight
        self.view.frame = frame
        
        self.kbHeight = 0;
        self.adjustedForKeyboard = false;
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? JAMAdverisementViewController {
            vc.udid = sender! as! String
            vc.mapVC = self.mapVC
        }
    }
}
