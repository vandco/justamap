//
//  JAMConversationsTableViewCell.swift
//  JustAMap
//
//  Created by Artur on 09/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

class JAMConversationsTableViewCell: UITableViewCell {

    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblShortMessage: UILabel!
    var conversationsVC: JAMConversationsTableViewController!
    var udid: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(JAMConversationsTableViewCell.avatarTapped))
        self.imgAvatar!.addGestureRecognizer(tapRecognizer)
        
        let longTapRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(JAMConversationsTableViewCell.longTap))
        self.addGestureRecognizer(longTapRecognizer)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func avatarTapped() {
        self.conversationsVC.avatarTapped(self.udid)
    }
    
    @objc func longTap() {
        print("UDID: \(self.udid)")
        print("My UDID: \(self.conversationsVC.defaults.udid!)")
        self.conversationsVC.longTap(self.udid)
    }
    
    fileprivate var _hasUnread: Bool = false
    var hasUnread: Bool {
        get { return self._hasUnread }
        set {
            self._hasUnread = newValue
            if (newValue) {
                self.lblShortMessage.font = self.lblShortMessage.font.makeBold()
            } else {
                self.lblShortMessage.font = self.lblShortMessage.font.makeUnbold()
            }
        }
    }

    fileprivate var _hasDraft: Bool = false
    var hasDraft: Bool {
        get { return self._hasDraft }
        set {
            self._hasDraft = newValue
            if (newValue) {
                self.lblShortMessage.font = self.lblShortMessage.font.makeItalic()
            } else {
                self.lblShortMessage.font = self.lblShortMessage.font.makeUnitalic()
            }
        }
    }
    
}
