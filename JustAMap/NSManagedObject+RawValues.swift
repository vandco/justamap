//
//  NSManagedObject+RawValues.swift
//  JustAMap
//
//  Created by Artur on 30/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//
//  http://stackoverflow.com/a/35378402/2175025
//

import CoreData

extension NSManagedObject {
    func setRawValue<ValueType: RawRepresentable>(_ value: ValueType, forKey key: String)
    {
        self.willChangeValue(forKey: key)
        self.setPrimitiveValue(value.rawValue as AnyObject, forKey: key)
        self.didChangeValue(forKey: key)
    }

    func rawValueForKey<ValueType: RawRepresentable>(_ key: String) -> ValueType?
    {
        self.willAccessValue(forKey: key)
        let result = self.primitiveValue(forKey: key) as! ValueType.RawValue
        self.didAccessValue(forKey: key)
        return ValueType(rawValue:result)
    }
}
