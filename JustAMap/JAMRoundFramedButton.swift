//
//  JAMRoundFramedButton.swift
//  JustAMap
//
//  Created by Artur on 28/07/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

@IBDesignable
class JAMRoundFramedButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 5
    @IBInspectable var borderWidth: CGFloat = 1
    
    fileprivate var _preservedBackgroundColor: UIColor?
    fileprivate var _preservedForegroundColor: UIColor?
    
    fileprivate var _touched: Bool = false
    
    override var isEnabled: Bool {
        get { return super.isEnabled }
        set {
            super.isEnabled = newValue
            self.layer.borderColor = self.titleColor(for: self.state)?.cgColor
        }
    }

    func setupRoundFrameAndColours() {
        self.layer.cornerRadius = self.cornerRadius;
        self.layer.borderWidth = self.borderWidth;
        
        if (self.titleColor(for: UIControlState()) === nil) {
            self.setTitleColor(UIColor.blue, for: UIControlState())
        }
        
        if (self.backgroundColor === nil) {
            self.backgroundColor = UIColor.white
        }
        
        self.layer.borderColor = self.titleColor(for: UIControlState())!.cgColor
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupRoundFrameAndColours()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setupRoundFrameAndColours()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.animateAfterTouchBegin()
        self.sendActions(for: UIControlEvents.touchDown)
    }
    
    func animateAfterTouchBegin() {
        if !self._touched {
            self._preservedBackgroundColor = self.backgroundColor
            self._preservedForegroundColor = self.titleColor(for: UIControlState())
            self._touched = true
        }
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.backgroundColor = self._preservedForegroundColor
            self.titleLabel?.textColor = self._preservedBackgroundColor
        })
    }
    
    func animateAfterTouchEnd() {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.backgroundColor = self._preservedBackgroundColor!
            self.titleLabel?.textColor = self._preservedForegroundColor
        })
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent!) {
        self._touched = false
        self.animateAfterTouchEnd()
        self.sendActions(for: UIControlEvents.touchCancel)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first! 
        let point = touch.location(in: self)
        if !CGRect(origin: CGPoint.zero, size: self.frame.size).contains(point) {
            self.animateAfterTouchEnd()
        } else {
            self.animateAfterTouchBegin()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self._touched = false
        self.animateAfterTouchEnd()
        
        let touch = touches.first! 
        let point = touch.location(in: self)
        if CGRect(origin: CGPoint.zero, size: self.frame.size).contains(point) {
            self.sendActions(for: UIControlEvents.touchUpInside)
        }
    }

}
