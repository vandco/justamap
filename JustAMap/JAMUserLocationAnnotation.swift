//
//  JAMUserLocationAnnotation.swift
//  JustAMap
//
//  Created by Artur on 26/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit
import Mapbox

class JAMUserLocationAnnotation: MGLPointAnnotation {
    fileprivate var _image: UIImage?
    var image: UIImage? {
        get { return self._image }
        set {
            self._image = newValue
            if let image = newValue {
                self._annotationImage = MGLAnnotationImage(image: image, reuseIdentifier: "userLocation" + String(format: "%d", [Date().timeIntervalSince1970]))
            } else {
                self._annotationImage = nil
            }
        }
    }
    fileprivate var _annotationImage: MGLAnnotationImage?
    var annotationImage: MGLAnnotationImage? {
        get { return self._annotationImage }
    }
    
    init(image: UIImage?) {
        super.init()
        self.image = image
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
