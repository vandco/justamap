//
//  JAMMessage.swift
//  JustAMap
//
//  Created by Artur on 30/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import Foundation

open class JAMMessage {
    var text: String!
    var sentAt: Date?
    var inProcess: Bool = false
    var isUnread: Bool = false
    var toUserUDID: String!
    var fromUserUDID: String!
    var storedMessage: Message?
    
    init(text: String, toUserUDID: String, fromUserUDID: String, sentAt: Date? = Date()) {
        self.text = text
        self.toUserUDID = toUserUDID
        self.fromUserUDID = fromUserUDID
        self.sentAt = sentAt
    }
    
    init?(dictionary: [String: AnyObject]) {
        if let text = dictionary["text"] as? String {
            self.text = text
        } else {
            return nil
        }
        
        if let sentAtTimestamp = dictionary["sent_at_timestamp"] as? NSString {
            self.sentAt = Date(timeIntervalSince1970: sentAtTimestamp.doubleValue)
        } else if let sentAt = dictionary["sent_at"] as? String {
            if let sentAt = Date(mySqlTimestamp: sentAt) {
                self.sentAt = sentAt
            } else {
                return nil
            }
        } else {
            return nil
        }
        
        if let toUserUDID = dictionary["toUserUDID"] as? String {
            self.toUserUDID = toUserUDID
        } else {
            return nil
        }
        
        if let fromUserUDID = dictionary["fromUserUDID"] as? String {
            self.fromUserUDID = fromUserUDID
        } else {
            return nil
        }
    }
    
    var dictionary: [String: AnyObject] {
        get {
            return [
                "text": self.text as AnyObject,
                "toUserUDID": self.toUserUDID as AnyObject,
                "inProcess": self.inProcess as AnyObject,
            ]
        }
    }
    
    var shortText: String {
        get {
            if (self.text.characters.count > 64) {
                return self.text[0...64] + "\u{2026}"
            } else {
                return self.text
            }
        }
    }
    
    class func arrayOfMessages(_ array: [[String: AnyObject]]) -> [JAMMessage]? {
        var result: [JAMMessage] = []
        
        for messageDict in array {
            if let message = JAMMessage(dictionary: messageDict) {
                result.append(message)
            } else {
                return nil
            }
        }
        
        return result
    }
}

open class JAMSentMessage: JAMMessage, Comparable {
    init(text: String, toUserUDID: String, fromUserUDID: String, sentAt: Date = Date()) {
        super.init(text: text, toUserUDID: toUserUDID, fromUserUDID: fromUserUDID, sentAt: sentAt)
    }
    
    override init?(dictionary: [String: AnyObject]) {
        super.init(dictionary: dictionary)
    }

    class func arrayOfMessages(_ array: [[String: AnyObject]]) -> [JAMSentMessage]? {
        return super.arrayOfMessages(array) as! [JAMSentMessage]?
    }

    func compare(_ rhs: JAMSentMessage) -> ComparisonResult {
        return self.sentAt!.compare(rhs.sentAt!)
    }
    
    class func compare(lhs: JAMSentMessage, rhs: JAMSentMessage) -> ComparisonResult {
        return lhs.compare(rhs)
    }
}

public func ==(lhs: JAMSentMessage, rhs: JAMSentMessage) -> Bool {
    return lhs === rhs || lhs.compare(rhs) == .orderedSame
}

public func <(lhs: JAMSentMessage, rhs: JAMSentMessage) -> Bool {
    return lhs.compare(rhs) == .orderedAscending
}
