//
//  UIImage+Processing.swift
//  JustAMap
//
//  Created by Artur on 09/09/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit
import AVFoundation

extension UIImage {
    
    // MARK: - Create template images
    
    fileprivate class func _bezierPathCircleInsideOut(radius: CGFloat, centre: CGPoint) -> UIBezierPath {
        let offsetPoint = CGPoint(x: centre.x - radius, y: centre.y - radius)
        
        let bezierPathTopLeft: UIBezierPath = UIBezierPath()
        bezierPathTopLeft.addArc(
            withCenter: CGPoint(x: radius + offsetPoint.x, y: radius + offsetPoint.y),
            radius: radius,
            startAngle: CGFloat(3 * PI/2),
            endAngle: CGFloat(PI),
            clockwise: false)
        bezierPathTopLeft.addLine(to: offsetPoint)
        bezierPathTopLeft.close()
        
        let bezierPathBottomLeft: UIBezierPath = UIBezierPath()
        bezierPathBottomLeft.addArc(
            withCenter: CGPoint(x: radius + offsetPoint.x, y: radius + offsetPoint.y),
            radius: radius,
            startAngle: CGFloat(PI),
            endAngle: CGFloat(PI / 2),
            clockwise: false)
        bezierPathBottomLeft.addLine(to: CGPoint(x: offsetPoint.x, y: offsetPoint.y + radius * 2))
        bezierPathBottomLeft.close()
        
        let bezierPathBottomRight: UIBezierPath = UIBezierPath()
        bezierPathBottomRight.addArc(
            withCenter: CGPoint(x: radius + offsetPoint.x, y: radius + offsetPoint.y),
            radius: radius,
            startAngle: CGFloat(PI / 2),
            endAngle: CGFloat(0),
            clockwise: false)
        bezierPathBottomRight.addLine(to: CGPoint(x: offsetPoint.x + radius * 2, y: offsetPoint.y + radius * 2))
        bezierPathBottomRight.close()
        
        let bezierPathTopRight: UIBezierPath = UIBezierPath()
        bezierPathTopRight.addArc(
            withCenter: CGPoint(x: radius + offsetPoint.x, y: radius + offsetPoint.y),
            radius: radius,
            startAngle: CGFloat(0),
            endAngle: CGFloat(3 * PI / 2),
            clockwise: false)
        bezierPathTopRight.addLine(to: CGPoint(x: offsetPoint.x + radius * 2, y: offsetPoint.y))
        bezierPathTopRight.close()
        
        let bezierPathResult: UIBezierPath = UIBezierPath()
        bezierPathResult.append(bezierPathTopLeft)
        bezierPathResult.append(bezierPathBottomLeft)
        bezierPathResult.append(bezierPathBottomRight)
        bezierPathResult.append(bezierPathTopRight)
        
        return bezierPathResult
    }
    
    fileprivate class func _bezierPathCircle(radius: CGFloat, centre: CGPoint) -> UIBezierPath {
        let offsetPoint = CGPoint(x: centre.x - radius, y: centre.y - radius)
        
        return UIBezierPath(ovalIn: CGRect(x: offsetPoint.x, y: offsetPoint.y, width: radius * 2, height: radius * 2))
    }
    
    public class func createCircleMask(diameter: CGFloat, fillOutsideColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: diameter, height: diameter), false, 0)
        let rect = CGRect(origin: CGPoint.zero, size: CGSize(width: diameter, height: diameter))
        self._bezierPathCircleInsideOut(radius: diameter / 2, centre: CGPoint(x: diameter / 2, y: diameter / 2)).addClip()
        let context: CGContext = UIGraphicsGetCurrentContext()!
        context.setFillColor(fillOutsideColor.cgColor)
        context.fill(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    public class func createCircle(diameter: CGFloat, fillColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: diameter, height: diameter), false, 0)
        let rect = CGRect(origin: CGPoint.zero, size: CGSize(width: diameter, height: diameter))
        let context: CGContext = UIGraphicsGetCurrentContext()!
        context.setFillColor(fillColor.cgColor)
        context.fill(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!.circleImage()
    }
    
    public class func createCircledBorder(outerDiameter: CGFloat, width: CGFloat, color: UIColor) -> UIImage {
        let outerRadius = outerDiameter / 2
        let innerRadius = outerRadius - width / 2
        let innerDiameter = innerRadius * 2
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: outerDiameter, height: outerDiameter), false, 0)
        let rect = CGRect(origin: CGPoint(x: width / 2, y: width / 2), size: CGSize(width: innerDiameter, height: innerDiameter))
        
        let context: CGContext = UIGraphicsGetCurrentContext()!
        context.setFillColor(UIColor.clear.cgColor)
        context.setStrokeColor(color.cgColor)
        context.setLineWidth(width)
        context.addEllipse(in: rect)
        context.strokePath()
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    // http://stackoverflow.com/a/7313050/2175025
    public class func drawImage(fgImage: UIImage, inImage bgImage: UIImage) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bgImage.size, false, 0)
        
        bgImage.draw(in: CGRect(origin: CGPoint.zero, size: bgImage.size))
        fgImage.draw(in: CGRect(origin: CGPoint.zero, size: fgImage.size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    // MARK: - Process images
    // http://stackoverflow.com/a/24498221/2175025
    // http://stackoverflow.com/a/21329347/2175025
    
    public func circleImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0)
        let rect = CGRect(origin: CGPoint.zero, size: self.size)
        UIBezierPath(roundedRect: rect, cornerRadius: self.size.width / 2).addClip()
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    public func scaleImage(scaledToSize newSize: CGSize, scale: CGFloat = 0) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, scale)
        let containerRect = CGRect(origin: CGPoint.zero, size: newSize)
        let fitRect = AVMakeRect(aspectRatio: self.size, insideRect: containerRect)
        self.draw(in: fitRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    // http://stackoverflow.com/a/29940940/2175025
    public func orientImageCorrectly() -> UIImage {
        let orientation = self.imageOrientation
        print("Orientation: \(orientation.rawValue)")
        
        let imgRef = self.cgImage
        
        let width  = CGFloat((imgRef?.width)!)
        let height = CGFloat((imgRef?.height)!)
        let imageSize = CGSize(width: width, height: height)
        
        var transform = CGAffineTransform.identity
        var bounds = CGRect(x: 0, y: 0, width: width, height: height)
        var boundHeight: CGFloat
        let scaleRatio = bounds.size.width / width
        
        switch (orientation) {
        case UIImageOrientation.up:
            transform = CGAffineTransform.identity
            
        case UIImageOrientation.upMirrored:
            transform = CGAffineTransform(translationX: imageSize.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            
        case UIImageOrientation.down:
            transform = CGAffineTransform(translationX: imageSize.width, y: imageSize.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            
        case UIImageOrientation.downMirrored:
            transform = CGAffineTransform(translationX: 0, y: imageSize.height)
            transform = transform.scaledBy(x: 1, y: -1)
            
        case UIImageOrientation.leftMirrored:
            boundHeight = bounds.size.height
            bounds.size.height = bounds.size.width
            bounds.size.width = boundHeight
            transform = CGAffineTransform(translationX: imageSize.height, y: imageSize.width)
            transform = transform.scaledBy(x: -1, y: 1)
            transform = transform.rotated(by: CGFloat(3 * M_PI / 2.0))
            
        case UIImageOrientation.left:
            boundHeight = bounds.size.height
            bounds.size.height = bounds.size.width
            bounds.size.width = boundHeight
            transform = CGAffineTransform(translationX: 0, y: imageSize.width)
            transform = transform.rotated(by: CGFloat(3.0 * M_PI / 2.0))
            
        case UIImageOrientation.rightMirrored:
            boundHeight = bounds.size.height
            bounds.size.height = bounds.size.width
            bounds.size.width = boundHeight
            transform = CGAffineTransform(scaleX: -1, y: 1)
            transform = transform.rotated(by: CGFloat(M_PI / 2.0))
            
        case UIImageOrientation.right:
            boundHeight = bounds.size.height
            bounds.size.height = bounds.size.width
            bounds.size.width = boundHeight
            transform = CGAffineTransform(translationX: imageSize.height, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI / 2.0))
        }
        
        UIGraphicsBeginImageContext(bounds.size)
        let context = UIGraphicsGetCurrentContext()
        
        if (orientation == UIImageOrientation.right)
            || (orientation == UIImageOrientation.left)
        {
            context?.scaleBy(x: -scaleRatio, y: scaleRatio)
            context?.translateBy(x: -height, y: 0)
        } else {
            context?.scaleBy(x: scaleRatio, y: -scaleRatio)
            context?.translateBy(x: 0, y: -height)
        }
        
        context?.concatenate(transform)
        
        context?.draw(imgRef!, in: CGRect(x: 0, y: 0, width: width, height: height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    public func cropImageTopLeft() -> UIImage {
        let dim = min(self.size.width, self.size.height)
        let croppedRect = CGRect(origin: CGPoint.zero, size: CGSize(width: dim, height: dim))
        let cgImage = self.cgImage?.cropping(to: croppedRect)
        return UIImage(cgImage: cgImage!)
    }
    
    public func tintImage(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0)
        let rect = CGRect(origin: CGPoint.zero, size: self.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        
        context?.setBlendMode(CGBlendMode.normal)
        UIColor.black.setFill()
        context?.fill(rect)
        
        context?.setBlendMode(CGBlendMode.normal)
        context?.draw(self.cgImage!, in: rect)
        
        context?.setBlendMode(CGBlendMode.color)
        color.setFill()
        context?.fill(rect)
        
        context?.setBlendMode(CGBlendMode.destinationIn)
        context?.draw(self.cgImage!, in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}
