//
//  JAMActionsMenuView.swift
//  JustAMap
//
//  Created by Artur on 10/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

@IBDesignable
class JAMActionsMenuView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 7
    @IBInspectable var borderWidth: CGFloat = 0.5
    @IBInspectable var borderColor: UIColor = UIColor(red: 200.0 / 255.0, green: 199.0 / 255.0, blue: 204.0 / 255.0, alpha: 1)
    
    var ownConstraints: [NSLayoutConstraint] = []
    
    override var isHidden: Bool {
        get { return super.isHidden }
        set {
            super.isHidden = newValue
            self.setNeedsUpdateConstraints()
        }
    }
    
    required init(coder aCoder: NSCoder) {
        super.init(coder: aCoder)!
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override class var requiresConstraintBasedLayout : Bool {
        return true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.cornerRadius
        self.layer.borderWidth  = self.borderWidth
        self.layer.borderColor  = self.borderColor.cgColor
        
        self.clipsToBounds = true
    }
    
    override func updateConstraints() {
        self.setupConstraints()
        super.updateConstraints()
    }
    
    func setupConstraints() {
        self.removeConstraints(self.ownConstraints)
        self.ownConstraints.removeAll(keepingCapacity: true)
        
        if (self.isHidden) {
            let zeroHeightConstraint = NSLayoutConstraint(
                item: self,
                attribute: NSLayoutAttribute.height,
                relatedBy: NSLayoutRelation.equal,
                toItem: nil,
                attribute: NSLayoutAttribute.height,
                multiplier: 1,
                constant: 0
            )
            self.ownConstraints += [zeroHeightConstraint]
        } else {
            //var newConstraints = [] as [NSLayoutConstraint]
            
            for i in 0..<self.subviews.count {
                if let subview = self.subviews[i] as? JAMActionView {
                    if (subview._topConstraint === nil) {
                        if (i == 0) {
                            subview._topConstraint = NSLayoutConstraint(
                                item: self,
                                attribute: NSLayoutAttribute.top,
                                relatedBy: NSLayoutRelation.equal,
                                toItem: subview,
                                attribute: NSLayoutAttribute.top,
                                multiplier: 1,
                                constant: 0
                            )
                        } else {
                            subview._topConstraint = NSLayoutConstraint(
                                item: self.subviews[i - 1] ,
                                attribute: NSLayoutAttribute.bottom,
                                relatedBy: NSLayoutRelation.equal,
                                toItem: subview,
                                attribute: NSLayoutAttribute.top,
                                multiplier: 1,
                                constant: -1
                            )
                            
                            if let prevSubview = self.subviews[i - 1] as? JAMActionView {
                                prevSubview._bottomConstraint = subview._topConstraint
                            }
                        }
                    }
                    self.ownConstraints += [subview._topConstraint!]
                    
                    if (subview._bottomConstraint === nil) {
                        if (i == self.subviews.count - 1) {
                            subview._bottomConstraint = NSLayoutConstraint(
                                item: self,
                                attribute: NSLayoutAttribute.bottom,
                                relatedBy: NSLayoutRelation.equal,
                                toItem: subview,
                                attribute: NSLayoutAttribute.bottom,
                                multiplier: 1,
                                constant: 0
                            )
                        } else {
                            subview._bottomConstraint = NSLayoutConstraint(
                                item: self.subviews[i + 1] ,
                                attribute: NSLayoutAttribute.top,
                                relatedBy: NSLayoutRelation.equal,
                                toItem: subview,
                                attribute: NSLayoutAttribute.bottom,
                                multiplier: 1,
                                constant: -1
                            )
                            
                            if let nextSubview = self.subviews[i + 1] as? JAMActionView {
                                nextSubview._topConstraint = subview._bottomConstraint
                            }
                        }
                    }
                    self.ownConstraints += [subview._bottomConstraint!]
                    
                    if (subview._leftConstraint === nil) {
                        subview._leftConstraint = NSLayoutConstraint(
                            item: self,
                            attribute: NSLayoutAttribute.left,
                            relatedBy: NSLayoutRelation.equal,
                            toItem: subview,
                            attribute: NSLayoutAttribute.left,
                            multiplier: 1,
                            constant: 0
                        )
                    }
                    self.ownConstraints += [subview._leftConstraint!]
                    
                    if (subview._rightConstraint === nil) {
                        subview._rightConstraint = NSLayoutConstraint(
                            item: self,
                            attribute: NSLayoutAttribute.right,
                            relatedBy: NSLayoutRelation.equal,
                            toItem: subview,
                            attribute: NSLayoutAttribute.right,
                            multiplier: 1,
                            constant: 0
                        )
                    }
                    self.ownConstraints += [subview._rightConstraint!]
                }
            }
        }
        
        self.addConstraints(self.ownConstraints)
    }
}


@IBDesignable
class JAMActionView: JAMButton {
    
    fileprivate var _topConstraint: NSLayoutConstraint?
    fileprivate var _leftConstraint: NSLayoutConstraint?
    fileprivate var _rightConstraint: NSLayoutConstraint?
    fileprivate var _bottomConstraint: NSLayoutConstraint?
    
    required init(coder aCoder: NSCoder) {
        super.init(coder: aCoder)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override class var requiresConstraintBasedLayout : Bool {
        return true
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let actionsMenu = self.superview as? JAMActionsMenuView {
            self.layer.borderWidth = actionsMenu.borderWidth
            self.layer.borderColor = actionsMenu.borderColor.cgColor
        }
    }
    
}
