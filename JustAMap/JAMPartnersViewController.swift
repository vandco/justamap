//
//  JAMPartnersViewController.swift
//  JustAMap
//
//  Created by Artur on 04/10/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit

class JAMPartnersViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.default
        self.navigationItem.titleView = UIView(frame: CGRect.zero)

        let partnerViews = self.scrollView.subviews 
        for i in 0..<(partnerViews.count) {
            let partnerView = partnerViews[i]
            let bottomBorderView = UIView()
            bottomBorderView.translatesAutoresizingMaskIntoConstraints = false
            
            bottomBorderView.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
            partnerView.addSubview(bottomBorderView)
            
            let bottomOfBorderBottom = NSLayoutConstraint(
                item: bottomBorderView,
                attribute: NSLayoutAttribute.bottom,
                relatedBy: NSLayoutRelation.equal,
                toItem: partnerView,
                attribute: NSLayoutAttribute.bottom,
                multiplier: 1,
                constant: 0
            )
            let leftOfBorderBottom = NSLayoutConstraint(
                item: bottomBorderView,
                attribute: NSLayoutAttribute.leading,
                relatedBy: NSLayoutRelation.equal,
                toItem: partnerView,
                attribute: NSLayoutAttribute.leading,
                multiplier: 1,
                constant: 0
            )
            let rightOfBorderBottom = NSLayoutConstraint(
                item: bottomBorderView,
                attribute: NSLayoutAttribute.trailing,
                relatedBy: NSLayoutRelation.equal,
                toItem: partnerView,
                attribute: NSLayoutAttribute.trailing,
                multiplier: 1,
                constant: 0
            )
            let heightOfBorderBottom = NSLayoutConstraint(
                item: bottomBorderView,
                attribute: NSLayoutAttribute.height,
                relatedBy: NSLayoutRelation.equal,
                toItem: nil,
                attribute: NSLayoutAttribute.notAnAttribute,
                multiplier: 1,
                constant: 0.5
            )

            bottomBorderView.addConstraint(heightOfBorderBottom)
            partnerView.addConstraints([bottomOfBorderBottom, leftOfBorderBottom, rightOfBorderBottom])
        }
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openLink(_ sender: UIButton) {
        UIApplication.shared.openURL(URL(string: sender.titleLabel!.text!)!)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
