//
//  JAMPingViewController.swift
//  JustAMap
//
//  Created by Artur on 06/09/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

class JAMPingViewController: UIViewController, UITextViewDelegate, JAMNavigationControllerBackButtonDelegate {

    //var networker: JAMNetworking!
    var baseUri: String!
    
    lazy var delegate: JAMAppDelegate = { return (UIApplication.shared.delegate as! JAMAppDelegate) }()
    lazy var defaults: UserDefaults = { return self.delegate.defaults }()

    var mapVC: JAMMapViewController!
    
    @IBOutlet weak var textView: UITextView!
    
    fileprivate var _startedAt: Double? = nil
    fileprivate var _player: AVAudioPlayer!
    
    fileprivate var _stats: [Double] = []
    
    fileprivate var _timer: Timer?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.default
        self.navigationItem.titleView = UIView(frame: CGRect.zero)
        self.navigationController?.navigationBar.tintColor = FOREGROUND_BUTTON_COLOR
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        //self.networker = JAMNetworking(baseUri: self.mapVC.networker.baseUri)
        //self.networker.numberOfAttempts = 1
        //self.networker.noDataTimeoutInSeconds = 180
        self.baseUri = self.mapVC.baseUri
        
        self.textView.text = self.textView.text + "Соединение... " // fucking Swift, it cannot apply +=
        self.getBaseURI()
    }
    
    func getBaseURI() {
        var url = "/balancer"
        if let udid = self.defaults.udid {
            url += "?access-token=" + udid
        }
        
        JAMAppDelegate.sharedSessionManager.request(url).validate().responseJSON { (response: DataResponse<Any>) in
            switch response.result {
            case .success(let baseURIs as [String]):
                let menu = UIAlertController(
                    title: "Доступны следующие сервера",
                    message: "Выберите нужный",
                    preferredStyle: UIAlertControllerStyle.actionSheet
                )
                
                for possibleBaseURI in baseURIs {
                    let server = UIAlertAction(
                        title: possibleBaseURI,
                        style: UIAlertActionStyle.default,
                        handler: { (action: UIAlertAction!) -> Void in
                            self._timer?.invalidate()
                            //self.networker.abort()
                            //self.networker = JAMNetworking(baseUri: possibleBaseURI)
                            self.baseUri = possibleBaseURI
                            
                            self.textView.text = self.textView.text + possibleBaseURI + "\n"
                            self.textViewDidChange(self.textView)
                            
                            self.ping()
                    }
                    )
                    
                    menu.addAction(server)
                }
                
                let cancel = UIAlertAction(
                    title: "Отмена",
                    style: UIAlertActionStyle.cancel,
                    handler: { (action: UIAlertAction!) -> Void in
                        self._timer?.invalidate()
                        //self.networker.abort()
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                )
                
                menu.addAction(cancel)
                
                __ASYNC_BLOCK {
                    self.present(menu, animated: true, completion: nil)
                }
            case .success:
                let reason: String
                reason = "unparseable JSON"
                __UI_THREAD_TASK {
                    self.textView.text = self.textView.text + "Fail: " + reason + "\n"
                    self.textViewDidChange(self.textView)
                }
            case .failure:
                let reason: String
                if (response.error != nil) {
                    reason = response.error!.localizedDescription
                } else {
                    reason = "unknown reason"
                }
                
                __UI_THREAD_TASK {
                    self.textView.text = self.textView.text + "Fail: " + reason + "\n"
                    self.textViewDidChange(self.textView)
                }
            }
        }
        
        /*self.networker.request(
            url,
            httpMethod: "GET",
            data: nil,
            onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                if let baseURIs = (try! JSONSerialization.jsonObject(with: body!, options: []) as? [String])
                {
                    var menu = UIAlertController(
                        title: "Доступны следующие сервера",
                        message: "Выберите нужный",
                        preferredStyle: UIAlertControllerStyle.actionSheet
                    )
                    
                    for possibleBaseURI in baseURIs {
                        let server = UIAlertAction(
                            title: possibleBaseURI,
                            style: UIAlertActionStyle.default,
                            handler: { (action: UIAlertAction!) -> Void in
                                self._timer?.invalidate()
                                self.networker.abort()
                                self.networker = JAMNetworking(baseUri: possibleBaseURI)
                                
                                self.textView.text = self.textView.text + possibleBaseURI + "\n"
                                self.textViewDidChange(self.textView)
                                
                                self.ping()
                            }
                        )
                        
                        menu.addAction(server)
                    }
                    
                    let cancel = UIAlertAction(
                        title: "Отмена",
                        style: UIAlertActionStyle.cancel,
                        handler: { (action: UIAlertAction!) -> Void in
                            self._timer?.invalidate()
                            self.networker.abort()
                            self.navigationController?.popViewController(animated: true)
                        }
                    )
                    
                    menu.addAction(cancel)
                    
                    __ASYNC_BLOCK {
                        self.present(menu, animated: true, completion: nil)
                    }
                }
            },
            onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                let reason: String
                if (error !== nil) {
                    reason = error!.localizedDescription
                } else {
                    reason = "unknown reason"
                    if let body = body {
                        print(String(data: body, encoding: String.Encoding.utf8)!)
                    }
                }

                __ASYNC_BLOCK {
                    self.textView.text = self.textView.text + "Fail: " + reason + "\n"
                    self.textViewDidChange(self.textView)
                }
            }
        )
        self.networker.start()*/
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.tintColor = FOREGROUND_BUTTON_COLOR
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: FOREGROUND_BUTTON_COLOR], for: UIControlState())
        
        let pathOfSound = URL(fileURLWithPath: Bundle.main.path(forResource: "click", ofType: "wav")!)
        self._player = try! AVAudioPlayer(contentsOf: pathOfSound)
        self._player.numberOfLoops = 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    func viewControllerShouldPopOnBackButton() -> Bool {
        self._timer?.invalidate()
        //self.networker.abort()
        
        if let statsJSON = try? JSONSerialization.data(withJSONObject: self._stats, options: []) {
            let processingVC = JAMProcessingViewController(status: "Отправка статистики...")
            self.present(processingVC, animated: true) { () -> Void in
                let dict: [String: String] = [
                    "stat": String(data: statsJSON, encoding: String.Encoding.utf8)!
                ]
                
                JAMAppDelegate.sharedSessionManager.request(
                    self.baseUri + "/stats?access-token=" + self.defaults.udid!,
                    method: .post,
                    parameters: dict,
                    encoding: JSONEncoding.default
                ).validate().responseData { (response: DataResponse<Data>) in
                    switch response.result {
                    case .failure:
                        self._player.currentTime = 0
                        self._player.play()
                    default:
                        break
                    }
                    
                    self.dismiss(animated: true) { () -> Void in
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                }
                
                /*self.networker.request(
                    "/stats?access-token=" + self.defaults.udid!,
                    httpMethod: "POST",
                    data: dict as AnyObject?,
                    onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                        self.dismiss(animated: true) { () -> Void in
                            self.navigationController?.popViewController(animated: true)
                        }
                    },
                    onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                        if (response != nil) {
                            print(response!)
                        }
                        
                        if (body != nil) {
                            print(String(data: body!, encoding: String.Encoding.utf8)!)
                        }
                        
                        self._player.currentTime = 0
                        self._player.play()

                        self.dismiss(animated: true) { () -> Void in
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                )
                self.networker.start()*/
            }
            
            return false
        } else {
            return true
        }
    }
    
    deinit {
        self._timer?.invalidate()
        //self.networker.abort()
    }
    
    @objc func ping() {
        self.textView.text = self.textView.text + "Запрос... " // fucking Swift, it cannot apply +=
        
        if (self._startedAt == nil) {
            self._startedAt = Date().timeIntervalSince1970
        }
        
        JAMAppDelegate.sharedSessionManager.request(self.baseUri + "/advertisements/index/northWest/90/-180/southEast/-90/180/afterTimestamp/0?access-token=" + self.defaults.udid!).validate()
        .responseJSON { (response: DataResponse<Any>) in
            let reason: String
            let appendText: String

            switch response.result {
            case .success(let advertisementsDict as [[String: Any]]):
                let finishedAt: Double = Date().timeIntervalSince1970
                let elapsed: Double = finishedAt - self._startedAt!
                let elapsedSeconds: Int = Int(elapsed)
                let elapsed100Decimals: Int = Int((elapsed - Double(elapsedSeconds)) * 100)
                self._stats.append(elapsed)

                appendText =
                    "OK: "
                    + String(format: "%d", arguments:[advertisementsDict.count])
                    + " advertisements, "
                    + String(format: "%d.%d", arguments: [elapsedSeconds, elapsed100Decimals])
                    + "\n"
                
                self._startedAt = nil
            
            case .success:
                self._player.currentTime = 0
                self._player.play()
                
                if (response.error != nil) {
                    reason = response.error!.localizedDescription
                } else {
                    reason = "uparseable JSON"
                }
                
                appendText = "Fail: " + reason + "\n"
            
            case .failure:
                self._player.currentTime = 0
                self._player.play()
                
                if (response.error != nil) {
                    reason = response.error!.localizedDescription
                } else {
                    reason = "unknown reason"
                }
                
                appendText = "Fail: " + reason
            }
            
            __UI_THREAD_TASK {
                self.textView.text = self.textView.text + appendText
                self.textViewDidChange(self.textView)
            }
            
            self._timer = Timer.scheduledTimer(
                timeInterval: 0,
                target: self,
                selector: #selector(self.ping),
                userInfo: nil,
                repeats: false
            )
        }
        
        /*self.networker.request(
            "/advertisements/index/northWest/90/-180/southEast/-90/180/afterTimestamp/0?access-token=" + self.defaults.udid!,
            httpMethod: "GET",
            data: nil,
            onSuccess: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                let finishedAt: Double = Date().timeIntervalSince1970
                let elapsed: Double = finishedAt - self._startedAt!
                let elapsedSeconds: Int = Int(elapsed)
                let elapsed100Decimals: Int = Int((elapsed - Double(elapsedSeconds)) * 100)
                
                self._stats.append(elapsed)
                
                if let body = body {
                    print("Body:")
                    if let bodyStr = String(data: body, encoding: String.Encoding.utf8) {
                        print(bodyStr)
                    } else {
                        print(body)
                    }
                    
                    if let advertisementsDict = try! JSONSerialization.jsonObject(with: body, options: []) as? [[String: AnyObject]]
                    {
                        self.textView.text =
                            self.textView.text + "OK: "
                            + String(format: "%d", arguments:[advertisementsDict.count])
                            + " advertisements, "
                            + String(format: "%d.%d", arguments: [elapsedSeconds, elapsed100Decimals])
                            + "\n"
                    } else {
                        self.textView.text = self.textView.text + "Fail: JSON parse error, " + String(format: "%d.%d", arguments: [elapsedSeconds, elapsed100Decimals]) + "\n"
                        self._player.currentTime = 0
                        self._player.play()
                    }
                } else {
                    self.textView.text = self.textView.text + "Fail: no body, " + String(format: "%d.%d", arguments: [elapsedSeconds, elapsed100Decimals]) + "\n"
                    self._player.currentTime = 0
                    self._player.play()
                }
                
                self.textViewDidChange(self.textView)
                
                self._startedAt = nil
                
                /*if (elapsed > 5) {
                    self._player.currentTime = 0
                    self._player.play()
                }*/
                
                self._timer = Timer.scheduledTimer(
                    timeInterval: 1,
                    target: self,
                    selector: #selector(self.ping),
                    userInfo: nil,
                    repeats: false
                )
            },
            onError: { (body: Data?, response: URLResponse?, error: NSError?) -> Void in
                self._player.currentTime = 0
                self._player.play()

                let reason: String
                if (error !== nil) {
                    reason = error!.localizedDescription
                } else {
                    reason = "unknown reason"
                    if let body = body {
                        print(String(data: body, encoding: String.Encoding.utf8)!)
                    }
                }

                self.textView.text = self.textView.text + "Fail: " + reason + "\n"
                self.textViewDidChange(self.textView)

                self._timer = Timer.scheduledTimer(
                    timeInterval: 0,
                    target: self,
                    selector: #selector(self.ping),
                    userInfo: nil,
                    repeats: false
                )
            }
        )?.abortOnError = false
        self.networker.start()*/
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let range = NSMakeRange(textView.text.characters.count, 0)
        textView.scrollRangeToVisible(range)
        textView.isScrollEnabled = false
        textView.isScrollEnabled = true
    }
    
}
