//
//  Message+ExtProp.swift
//  JustAMap
//
//  Created by Artur on 30/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//
//  http://stackoverflow.com/a/35378402/2175025
//

import CoreData
import CoreLocation
import UIKit

extension Message {

    enum Direction : Int {
        case incoming, outgoing
    }
    
    var direction: Direction {
        set {
            self.setRawValue(newValue, forKey: "direction_")
        }
        get {
            return self.rawValueForKey("direction_")! as Direction
        }
    }
    
    var shortText: String {
        get {
            if (self.text.characters.count > 64) {
                return self.text[0...64] + "\u{2026}"
            } else {
                return self.text
            }
        }
    }
    
    var correspondent: Advertisement? {
        get {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Advertisement")
            fetchRequest.predicate = NSPredicate(format: "(udid = %@)", argumentArray: [self.correspondentUDID]);
            
            let fetchedObjects = try! self.managedObjectContext?.fetch(fetchRequest)
            
            return (fetchedObjects as? [Advertisement])?.first
        }
        
        set {
            if (newValue == nil) {
                if let correspondent = self.correspondent {
                    self.managedObjectContext?.delete(correspondent)
                }
            } else {
                self.correspondentUDID = newValue!.udid
            }
        }
    }

    func linkToAdvertisementUDID(
        _ advertisementUDID: String,
        scale: String,
        circleImage: @escaping (UIImage) -> UIImage,
        coreDataDispatcher: CoreDataDispatcher,
        //networker: JAMNetworking,
        baseUri: String,
        selfUDID: String,
        completionHandler: ((_ advertisement: Advertisement, _ message: Message) -> Void)?,
        errorHandler: DataCompletionHandler?
    ) -> Void
    {
        let link = { (advertisement: Advertisement) -> Void in
            self.correspondent = advertisement
        }
        
        if let advertisement = (coreDataDispatcher.fetchObjectsMeetPredicate(NSPredicate(format: "(udid = %@)", argumentArray: [advertisementUDID])) as [Advertisement]).first
        {
            link(advertisement)
            completionHandler?(advertisement, self)
        } else {
            Advertisement.download(
                advertisementUDID,
                scale: scale,
                circleImage: circleImage,
                coreDataDispatcher: coreDataDispatcher,
                baseUri: baseUri,
                selfUDID: selfUDID,
                completionHandler: { (advertisement: Advertisement) -> Void in
                    link(advertisement)
                    completionHandler?(advertisement, self)
                },
                errorHandler: errorHandler
            )
        }
    }
}
