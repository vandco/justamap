//
//  CallSelector.swift
//  JustAMap
//
//  Created by Artur on 06/08/16.
//  Copyright (c) 2016 V&Co Ltd. All rights reserved.
//
//  http://stackoverflow.com/q/24170282/2175025
//

import Foundation

extension NSObject {
    
    func callSelectorAsync(_ selector: Selector, object: AnyObject?, delay: TimeInterval) -> Timer {
        
        let timer = Timer.scheduledTimer(timeInterval: delay, target: self, selector: selector, userInfo: object, repeats: false)
        return timer
    }
    
    func callSelector(_ selector: Selector, object: AnyObject?, delay: TimeInterval) {
        
        let delay = delay * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time, execute: {
            Thread.detachNewThreadSelector(selector, toTarget:self, with: object)
        })
    }
}
