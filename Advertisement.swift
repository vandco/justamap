//
//  Advertisement.swift
//  
//
//  Created by Artur on 29/08/16.
//
//

import Foundation
import CoreData

class Advertisement: NSManagedObject {

    @NSManaged var desc: String?
    @NSManaged var descShort: String
    @NSManaged var isDraft: Bool
    @NSManaged var photoMicro: Data?
    @NSManaged var submittedAt: Date
    @NSManaged var udid: String
    @NSManaged var photoMini: Data?
    @NSManaged var photoFullSize: Data?
    @NSManaged var latitude: Double
    @NSManaged var longitude: Double

}
